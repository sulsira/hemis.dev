
var studentModal = {
		container : $('#myModal'),
	init: function(config){
		var $this = this;
		var studentId = 0;
		config.tr.on('click',$this.getdata);
	},
	openloading: function(){
		$('.progress').slideDown();
		for(var i = 0; i <= 100; i++){
			$(".bar").css("width",i+"%");
			// console.log(i);
		}
		// console.log( $(".loading") );
	},
	closeloading: function(){
		$('.loading').hide();
		   $('#myModal').modal();
		   studentModal.completed();
		   // console.log(window.location)
		   $('.progress').slideUp();
	},
	getdata: function(){
		var self = studentModal;
		if ( ($(this).data('mtype')) == 'student' ) {
			studentModal.studentId = $(this).data('rnum');
			studentModal.ajaxpull().then(function(e){
				self.addinfo.call(e);
			});
		}
	},
	ajaxpull: function(){
	var self = this;
	var dfd = new $.Deferred();
	$.ajax({
		url: 'students/'+studentModal.studentId,
		type: 'GET',
		dataType: 'json',
		success: dfd.resolve,
		beforeSend: self.openloading,
		complete: self.closeloading,
		error: function(e){
			console.log(e);
		}
	});
	return dfd.promise();
	},
	completed: function(){
		if($("#modal").is("hidden")){
		var locat = window.location.href;
// var loc = window.location.pathname;
// var dir = loc.substring(0, loc.lastIndexOf('/'));
// var url = window.location.origin +dir+"/"+"redirector.php?wget="+window.location.href;
        window.location.replace(locat);
        // console.log(window.location)
		}else{
			//console.log("not");
		}
	},
	addinfo: function(){
		var self = studentModal;
		var modalbox = self.container;
		var $this = this;
		var 
			student_status = modalbox.find('#action'),
			bDay = modalbox.find('#bday'),
			bMonth = modalbox.find('#bmonth'),
			bYear = modalbox.find('#byear'),
			student_enit = modalbox.find('#enit'),
			student_fname = modalbox.find('#firstName'),
			student_mname = modalbox.find('#middleName'),
			student_lname = modalbox.find('#lastName'),
			student_gender = modalbox.find('#gender'),
			student_nation = modalbox.find('#nation'),
			student_street = modalbox.find('#street'),
			student_town = modalbox.find('#town'),
			student_district = modalbox.find('#district'),
			student_region = modalbox.find('#region'),
			student_pobox = modalbox.find('#pobox'),
			student_email = modalbox.find('#email'),
			student_phone = modalbox.find('#phone'),
			student_delete = modalbox.find('#deleting'),
			class_details = modalbox.find('#class_details');
			var mname = ($this['Pers_MiddleName'] !== null )? $this['Pers_MiddleName'] : ' ';
			student_fname.val($this['Pers_FirstName']);
			student_mname.val(mname);
			student_lname.val($this['Pers_LastName']);


			console.log(bDay);



			student_status.find('option').each(function(e,option){
				if($(option).text() === $this['Pers_Standing'] ){
					$(option).attr('selected','selected');
					$(student_status).prepend(option);

				}
			});

			student_gender.find('option').each(function(e,option){
				if($(option).text().toLowerCase() === $this['Pers_Gender'].toLowerCase() ){
					$(option).attr('selected','selected');
					$(student_gender).prepend(option);

				}
			});
			student_nation.find('option').each(function(e,option){
				if($(option).text() === $this['Pers_Nationality'] ){
					$(option).attr('selected','selected');
					$(student_nation).prepend(option);
				}
			});

			if ($this['Pers_Ethnicity'] != null) {
				student_enit.find('option').each(function(e,option){
				if($(option).text().toLowerCase() === $this['Pers_Ethnicity'].toLowerCase() ){
					$(option).attr('selected','selected');
					$(option).addClass('selected');
					 $(student_enit).prepend(option);
				}
			});
			};

			if (student_enit.find('option').is(".selected")) {

			}else{
				 $(student_enit).prepend('<option selected="selected" class="selected">'+$this['Pers_Ethnicity']+'</option>');
				// $('<option></option>', {
				// 	text: $this['Pers_Ethnicity'],
				// 	class: 'selected'
				// }).prependTo(student_enit);
			}
			// console.log($this['contact']);
			// check if an array (contact is empty or not)
			// loop through the available details and put in the view
                        if($this['contact'] !== null){ // beging of the contact array

                        	$.each($this['contact'],function(ind, val){

                                if( val['Cont_ContactType'].toLowerCase() == 'Phone'.toLowerCase() ){
                                        student_phone.val(val['Cont_Contact']);
                                } 

                        	// work with website
                                if( val['Cont_ContactType'].toLowerCase() == 'website'.toLowerCase() ){
                                        // student_email.val(val['Cont_Contact']);
                                }

                        	// work with email

                                if( val['Cont_ContactType'].toLowerCase() == 'Email'.toLowerCase() ){
                                        student_email.val(val['Cont_Contact']);
                                }
                        	});

                        } //end of contact array

			if ($this['Pers_DOB'] != null) {
                            var studentDate = $this['Pers_DOB'].split(' ');

				if ((studentDate[0].match("/"))) {
				 
				var dobArray = studentDate[0].split("/");
				var year = dobArray[2];
				var month = dobArray[1];
				var day = dobArray[0];
				bDay.val(day);
				bMonth.val(month);
				bYear.val(year);
				console.log(day)
				console.log(dobArray)
			}else if(studentDate[0].match(":")){
				var dobArray = studentDate[0].split(":");
				var day = dobArray[2];
				var month = dobArray[1];
				var year = dobArray[0];
				bDay.val(day);
				bMonth.val(month);
				bYear.val(year);
			}else if(studentDate[0].match("-")){
                            
				var dobArray = studentDate[0].split("-");
				var day = dobArray[2];
				var month = dobArray[1];
				var year = dobArray[0];
				bDay.val(day);
				bMonth.val(month);
				bYear.val(year);
			}

			};


		modalbox.find('#persId').val($this['Pers_PersonID']);
										var delete_url_link = 'deleting.php?what=student&std_id='+$this['Pers_PersonID']+'&lcid='+$this['Pers_LearningCenterID']+'&what=student';
										profile_url_link = 'profile.php?pid='+$this['Pers_PersonID']+'&sid='+$this['Pers_LearningCenterID']+'&what=student',
										student_delete.attr('href',delete_url_link);
										// here we start to loop the student class

									if ( $this['classes'] != null) {

										var content =  $this['classes'];

										var template = $.trim( $('#classtemplate').html() );

										var spans = $(template).find('span');

										$.each( content, function( index, value ){

											var admit = value['Clas_AdmissionDate'].split(' ');
											var admdate = admit[0];
											var grat =  (value['Clas_GraduationDate'])?value['Clas_GraduationDate'].split(' ') : {};
											var gratdate = grat[0];

										var temp = template.replace( /{}number{}/ig, index+1 )
											.replace( /{}class_name{}/ig, value['Clas_LocalClassName'] )
											.replace( /{}class_date{}/ig, admdate )
											.replace( /{}class_status{}/ig, value['Clas_AttendanceStatus'] )
											.replace( /{}class_qualification{}/ig, value['Clas_EntryQualification'] )
											.replace( /{}class_award{}/ig, value['Clas_Award'] )
											.replace( /{}class_grad{}/ig, gratdate );
											$('#class_details').append($(temp));
										});


									};


	
										//				//looping student classes ends

	// GETTING STUDENTS ADDRESS

		if($this.std_address != null ){
	
				student_district.find('option').each(function(e,option){
				if($(option).text() === $this.std_address['Addr_District'] ){
					$(option).attr('selected','selected');
					$(student_district).prepend(option);

				}
			});

			student_region.find('option').each(function(e,option){
				if($(option).text() === $this.std_address['Addr_Region'] ){
					$(option).attr('selected','selected');
					$(student_region).prepend(option);

				}
			});

		$(student_street).val($this.std_address['Addr_AddressStreet']);
		$(student_town).val($this.std_address['Addr_Town']);
		$(student_pobox).val($this.std_address['Addr_POBox']);	
		$('#add_id').val($this.std_address['Addr_AddressID']);	

		}
	// GETTING STUDENTS ADDRESS STOPS
			$('#delete').attr('href',delete_url_link);	
			$('#profile').attr('href',profile_url_link);	
			// console.log(delete_url_link);

						
	}

};
studentModal.init({
	tr: $('.onmodal')
});
