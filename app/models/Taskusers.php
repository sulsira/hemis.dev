<?php

class Taskusers extends Eloquent {
	protected $guarded = array();
	protected $table = 'task_users';
protected $primaryKey = 'taskID';
	public $timestamps = false;
	public static $rules = array();
	protected $fillable = [
	'toID','fromID','taskID'
	];


	public function tasks(){
		return $this->belongsTo('Task');
	}
}
