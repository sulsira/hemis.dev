<?php

class ScholarshipApplication extends Eloquent {
	protected $guarded = array();
	protected $table = 'scholarship_Application';
	public static $rules = array();
	protected $fillable = [
'scholarshipsID',
'studentID',
'scholarshipID',
'award',
'filedofstudy',
'leveofstudy',
'status'
	];


	public function applicant()
    {
        return $this->belongsTo('Scholarship');
    }
}
