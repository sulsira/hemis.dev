<?php

class Researcher extends Eloquent {
	protected $guarded = array();
	protected $primaryKey = 'id';
	protected $table = 'researcher';
	public static $rules = array();
	protected $fillable = [
							"id",
							"userID",
							"email",
							"type",
							"fullname",
							"deleted"
						];

    public function publications()
    {
        return $this->hasMany('Publication','pub_researcherID','id');
 // select * from Publication where (foreign key) pub_researcherID = (local key) -> (publication model key that links the two tables) id
    }
}
