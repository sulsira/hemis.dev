<?php

class Programme extends Eloquent {
	protected $guarded = array();
	protected $primaryKey = 'Pers_PersonID';
	public static $rules = array();

	public function school(){
		return $this->belongsTo('School','Pers_LearningCenterID');
	}
}
