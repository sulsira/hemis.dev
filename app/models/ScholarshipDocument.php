<?php

class ScholarshipDocument extends Eloquent {
	protected $table = 'scholarship_documents';
	// protected $primaryKey = ''
	protected $fillable = [
		'foldername',
		'directory',
		'filename',
		'studentID',
		'confirmed',
		'rename',
		'type',
		'deleted'
	];
	protected $guarded = array();
	public static $rules = array();

public function applicant()
    {
        return $this->belongsTo('Scholarship');
    }
}
