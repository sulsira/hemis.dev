<?php

class Person extends Eloquent {

	protected $table = 'person';
	protected $primaryKey = 'Pers_PersonID';
	protected $guarded = array();
	public  $timestamps = false;
	public $fillable = ['Pers_UpdateDate','Pers_PersonIdentifier','Pers_LearningCenterID','Pers_GivenID','Pers_FirstName','Pers_MiddleName','Pers_LastName','Pers_Type','Pers_DOB','Pers_Gender','Pers_Nationality','Pers_Ethnicity','Pers_Standing','Pers_Disability','Pers_NIN','Pers_AddDate'];
	public static $rules = array();
	public function school(){
		return $this->belongsTo('School','Pers_LearningCenterID');
	}
	public function classes(){
		return $this->hasMany('Classes','Clas_PersonID');
	}

	public function staffs(){
		return $this->hasMany('Staff','Staff_PersonID');
	}
}
