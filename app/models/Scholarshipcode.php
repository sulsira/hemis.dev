<?php

class Scholarshipcode extends Eloquent {
	protected $table = 'scholarshipcodes';
	protected $fillable = [
	'code',
	'resellerID',
	'visible',
	'studentID',
	'deleted',
	'assigned',
	'codeencryption'
	];
	protected $guarded = array();
	public static $rules = array();
}
