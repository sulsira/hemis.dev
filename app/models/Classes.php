<?php

class Classes extends Eloquent {
	protected $guarded = array();
	protected $table = 'class'; //there should be default values in the table
	protected $primaryKey = 'Clas_ClassID';
	public static $rules = array();
	public $timestamps = false;
	public function school(){
		return $this->belongsTo('school','Clas_LearningCenterID');
	}

	public function person(){
		return $this->belongsTo('Person','Clas_PersonID');
	}

	public function courses(){
		return $this->hasMany('Course','Cour_CourseID','Clas_CourseID');
	}
}
