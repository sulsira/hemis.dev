<?php

class Course extends Eloquent {
	protected $table = 'course';
	protected $primaryKey = 'Cour_CourseID';
	protected $guarded = array();

	public static $rules = array();

	public function school(){
		return $this->belongTo('School','Cour_LearningCenterID');
	}
}
