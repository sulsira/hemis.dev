<?php

class ScholarshipAcademics extends Eloquent {

	protected $table = 'scholarship_stdacademics';
	protected $primaryKey = 'id';
	protected $guarded = array();
	public static $rules = array();
	protected $fillable = ['scholarshipID','infoID','schoolname','schoolcertification','status','visibility','step','deleted'];

public function applicant()
    {
        return $this->belongsTo('Scholarship');
    }
}
