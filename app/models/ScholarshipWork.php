<?php

class ScholarshipWork extends Eloquent {
	protected $table = 'workexperience';
	protected $primaryKey = 'work_id';
	protected $guarded = array();
	public static $rules = array();
	public $timestamps = false;
	protected $fillable = [
		'work_NameOfemployer',
		'work_lenghtOfservice',
		'work_presentPosition',
		'work_visibility',
		'work_active',
		'work_studentID'
	];

public function applicant()
    {
        return $this->belongsTo('Scholarship');
    }
}


