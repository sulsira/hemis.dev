<?php

class Scholarship extends Eloquent {
	protected $table = 'scholarship_students';
	protected $guarded = array();
	protected $primaryKey = 'id';
	protected $fillable = array('fullname','othernames','dob','gender','nationality','phone','email','address','scholarshipID','status','visibility','step','deleted');
	public static $rules = array();



	public function academics()
    {
        return $this->hasMany('ScholarshipAcademics','infoID');
    }
 	public function documents()
    {
        return $this->hasMany('ScholarshipDocument','studentID');
    }
	public function works()
    {
        return $this->hasMany('ScholarshipWork','work_studentID');
    }
	public function scholarships()
    {
        return $this->hasMany('ScholarshipApplication','studentID');
    }
}
