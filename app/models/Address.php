<?php

class Address extends Eloquent {
	protected $table = 'address';
	protected $guarded = array();
	protected $primaryKey = 'Addr_AddressID';
	public static $rules = array();
	protected $fillable = [
			'Addr_AddressID' ,
			'Addr_EntityID' ,
			'Addr_EntityType' ,
			'Addr_AddressStreet' ,
			'Addr_POBox' ,
			'Addr_Town' ,
			'Addr_Region' ,
			'Addr_District' ,
			'Addr_Country' ,
			'Addr_CareOf' ,
			'Addr_Deleted' ,
			'Addr_AddDate' ,
			'Addr_UpdatedDate'
	];
	public $timestamps = false;
}
