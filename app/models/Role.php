<?php

class Role extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

	public function ScholarshipApplications(){
		return $this->belongsToMany('ScholarshipApplication');
	}
	public function ScholarshipDocuments(){
		return $this->belongsToMany('ScholarshipDocument');
	}

	public function ScholarshipWorks(){
		return $this->belongsToMany('ScholarshipWork');
	}
	public function ScholarshipAcademics(){
		return $this->belongsToMany('ScholarshipAcademics');
	}
	public function Scholarships(){
		return $this->belongsToMany('Scholarship');
	}
}
