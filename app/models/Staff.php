<?php

class Staff extends Eloquent {
	protected $table = 'staff';
	protected $guarded = array();
	protected $primaryKey = 'Staff_StaffID';
	public static $rules = array();

	public function school(){
		return $this->belongsTo('School','Staff_LearningCenterID');
	}
	public function persons(){
		return $this->belongsTo('Person','Staff_PersonID');
	}
}
