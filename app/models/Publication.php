<?php

class Publication extends \Eloquent {
	protected $table = 'publication';
	protected $primaryKey = 'id';
	protected $fillable = [
	"id",
	"title",
	"pub_researcherID",
	"pub_type",
	"pub_abstract",
	"pub_discipline",
	"pub_view",
	"pub_docID",
	"visible",
	"deleted"
];

    public function researcher()
    {
        return $this->belongsTo('Researcher','pub_researcherID');

       // select * from researcher where (local key) pub_researcherID
    }
     public function document()
    {
        return $this->hasMany('document','entity_ID', 'id');
    }
}