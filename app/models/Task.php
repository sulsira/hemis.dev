<?php

class Task extends Eloquent {
	protected $guarded = array();
protected $primaryKey = 'fromID';
	public static $rules = array();
	protected $fillable =[
		'title',
		'details',
		'due',
		'from',
		'to',
		'fromID',
		'toID',
		'remarks',
		'reassign',
		'reassignID',
		'deleted',
		'publish'
	];

	######## ** RULES ** #######
	#
	#
	#
	#
	#
	#
	######### ** END RULES ** ###

	public function taskusers(){
		return $this->hasMany('Taskusers','taskID','id');
	}
}


