<?php

class School extends Eloquent {  

	protected $table = 'learningcenter';
	protected $primaryKey = 'LeCe_LearningCenterID';
	protected $guarded = array();
	public static $rules = array(
		'school.name' => 'required',
		'school.enrol-capacity' =>'numeric|max:4000',
		'school.ownership' =>'',
		'school.type' =>'required',
		'school.classification' =>'required',
		'cont.email' =>'email',
		'cont.tele' =>'numeric',
		'cont.mobile' =>'numeric',
		'cont.website' =>'',
		'address.street' =>'',
		'address.pobox'=>'',
		'address.town'=> ''
		);
	protected $fillable = [
	'LeCe_LearningCenterID',
	'LeCe_Name' ,
	'LeCe_LearningCenterNo',
	'LeCe_MainPersonID',
	'LeCe_LearningCenterType',
	'LeCe_Ownership',
	'LeCe_EnrolmentCapacity',
	'LeCe _FinancialSource',
	'LeCe_Standing',
	'LeCe_Deleted',
	'LeCe_AddDate',
	'LeCe_UpdateDate',
	'LeCe_Classification',
	'LeCe_AccountManager',
	'url'
	];
public $timestamps = false;
	public function students(){
		return $this->hasMany('Person','Pers_LearningCenterID','LeCe_LearningCenterID');
	}
	public function classes(){
		return $this->hasMany('Classes','Clas_LearningCenterID','LeCe_LearningCenterID');
	}
	public function staffs(){
		return $this->hasMany('staff','Staff_LearningCenterID','LeCe_LearningCenterID');
	}

	public function address(){
		return $this->hasOne();
	}
	public function contacts(){
		return $this->hasMany('Contact','Cont_EntityID','LeCe_LearningCenterID');
	}
	public function persons(){
		return $this->hasMany('Person','Pers_LearningCenterID','LeCe_LearningCenterID');
	}
	public function courses(){
		return $this->hasMany('Course','Cour_LearningCenterID','LeCe_LearningCenterID');
	}
}
