<?php

class Contact extends Eloquent {
	protected $guarded = array();
	protected $table = 'contactinfo';
	protected $primaryKey = 'Cont_ContactInfoID';
	public static $rules = array(
'email'=> 'email',
'telephone'=> 'numeric',
'mobile'=> 'numeric',
'website'=> 'url'
		);
	protected $fillable = ['Cont_ContactInfoID','Cont_EntityID','Cont_EntityType','Cont_Contact','Cont_ContactType'];
	public $timestamps = false;
	public function school(){
		return $this->belongsTo('school','Cont_EntityID');
	}
}
