<?php

class Document extends Eloquent {


	protected $guarded = array();
	protected $table = 'document';
	// protected $primaryKey = '';
	// public $timestamps = false;
	public static $rules = array();
	protected $fillable = [
	'id',
	'type',
	'title',
	'entity_type',
	'entity_ID',
	'fullpath',
	'filename',
	'foldername',
	'filetype',
	'extension',
	'userID',
	'addedDate',
	'thumnaildir'
	];
	public  function publication(){
		return $this->belongsTo('publication','');
	}
}
