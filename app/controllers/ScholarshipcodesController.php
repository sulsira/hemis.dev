<?php

class ScholarshipcodesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $layout = 'code';
	protected $menu = array();
	public function __construct(){

		$this->menu = [
			['name'=>'back','visible'=> 1 , 'url'=>'scholarship','priv'=>'veda','type'=>'single'],
			['name'=>'home','visible'=> 1 , 'url'=>'scholarship/students','priv'=>'veda','type'=>'single'],
			['name'=>'scholarships','visible'=> 0 , 'url'=>'scholarship/scholarships','priv'=>'veda','type'=>'single'],
			['name'=>'interviews','visible'=> 0 , 'url'=>'scholarship/interviews','priv'=>'veda','type'=>'single'],
			['name'=>'awards','visible'=> 0 , 'url'=>'scholarship/awards','priv'=>'veda','type'=>'single'],
			['name'=>'head count','visible'=> 0 , 'url'=>'scholarship/headcount','priv'=>'veda','type'=>'single'],
			['name'=>'scholarships','visible'=> 0 , 'url'=>'scholarship/scholarships','priv'=>'veda','type'=>'single'],
			['name'=>'codes','visible'=> 0 , 'url'=>'scholarship/codes','priv'=>'veda','type'=>'single'],
			['name'=>'scholarships','visible'=> 0 , 'url'=>'scholarship/scholarships','priv'=>'veda','type'=>'single'],
			['name'=>'username','visible'=>0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 0 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 0 , 'url'=>'scholarship/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 0 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	


	}
	public function index()
	{
        $this->layout->content = View::make('scholarshipcodes.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()

	{
		$data = Scholarshipcode::all();
       $this->layout->content = View::make('scholarshipcodes.create')->with('data',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$dbCodes = array();
		$newCodes = array();
		$year = 'A14';
		for ($i=0; $i <= 10 ; $i++) { 
			$r = str_random(4);
			$code = $year.$r;
			$respond = Scholarshipcode::whereCode($code)->first();
			if ( $respond ) {
				# code...
			}else{
				$newCodes[] = $code;
			}
		}
		
		foreach ($newCodes as $key => $value) {
				$codes = Scholarshipcode::create(array(
					'code'  => $value,
					'resellerID' => 0,
					'visible' => 1,
					'studentID' => 0,
					'deleted' => 0,
					'assigned' => 0,
					'codeencryption' =>Crypt::encrypt($value)
				));
				$dbCodes[] = $codes;
				// return $codes;
		}
		
		$this->layout->content = View::make('scholarshipcodes.create')->with('data',$dbCodes);
		

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('scholarshipcodes.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('scholarshipcodes.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
