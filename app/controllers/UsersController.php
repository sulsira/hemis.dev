<?php

class UsersController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::where('deleted','!=',1)->get()->toArray();
		$this->data['users'] = $users;
		$this->layout->content = View::make('admin.users.index')->with('data',$this->data);
       // $this->layout->content = View::make('users.index')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $this->layout->content = View::make('users.index')->with('data',$this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()

	{
	$rules = [
		'email'=>'required|email|unique:users,email',
		'userType'=>'required',
		'password'=>'required|min:6'
	];
		// return Input::get('username','email','password','mobile','accountType','userType','Previleges');
		$validation = Validator::make(Input::all(),$rules);

		if ($validation->fails()) {
		return Redirect::back()->withErrors($validation)->withInput();
		// $messages = $validation->messages();
		// $failed = $validation->failed();
		// dd( $messages );
		}else{

			if (Input::get('accountType') == 'school') {
					$userType = (is_numeric(Input::get('userType')))? Input::get('userType') : 0 ;
					$user_details = User::create(array(
						'fullname'=>Input::get('username'),
						'email'=>Input::get('email'),
						'phone'=>Input::get('mobile'),
						'userType'=>$userType,
						'password'=>Hash::make(Input::get('password')),
						'domain'=>Input::get('accountType'),
						'visible'=>1,
						'lickId'=>$userType,
						'deleted'=>0,
						'link'=>1, // 1 means yes there is a link
						'status'=>3, //3 means pending not yet used his account
						'activated'=>0,
						'userGroup'=>Input::get('accountType'),
						'privileges'=>'veda',
						'username'=>'newUser_',
						'url'=>'school||institution/'.$userType,

						 ));
				}else{
					$userID = User::create(array(
						'fullname'=>Input::get('username'),
						'email'=>Input::get('email'),
						'phone'=>Input::get('mobile'),
						'userType'=>Input::get('userType'),
						'password'=>Hash::make(Input::get('password')),
						'domain'=>Input::get('accountType'),
						'visible'=>1,
						'lickId'=>0,
						'deleted'=>0,
						'link'=>0, // 1 means yes there is a link
						'status'=>3, //3 means pending not yet used his account
						'activated'=>0,
						'userGroup'=>Input::get('accountType'),
						'privileges'=>'veda',
						'username'=>'newUser'

						 ));
				}

		}

		return Redirect::back()->withErrors($validation)->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('users.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
        $this->layout->content =  View::make('admin.users.edit')->with('user',$user);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input  = Input::all();
	$rules = [
		'email'=>'required|email',
	];
		$validation = Validator::make($input,$rules);

		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput();
		}
		$user = User::findOrFail($id);
		$user->fill($input);
		$user->save();

		return Redirect::to('admin/users')->withErrors($validation)->withInput();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::where('id','=',$id)->delete();
		// $user = User::where('id','=',$id)->update(array('deleted'=>1));
		return Redirect::to('admin/users');
	}

}
