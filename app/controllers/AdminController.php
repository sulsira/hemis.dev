<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $layout = 'admin';
	protected $menu = array();
	protected $data = array();
	public function __construct(){

			$name = Session::get('userName');
			$userDetails = Auth::user();
			$username = ($name)? $name : '';
			if (empty($username)) {
				$username = $userDetails->fullname || $userDetails->email;
			}
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/admin','priv'=>'veda','type'=>'single'],
			['name'=>'schools','visible'=> 1 , 'url'=>'admin/schools','priv'=>'veda','type'=>'single'],
			['name'=>'files','visible'=> 1 , 'url'=>'admin/files','priv'=>'veda','type'=>'single'],
			['name'=>'entry','visible'=> 0 , 'url'=>'admin/entry','priv'=>'veda','type'=>'single'],
			['name'=>'task','visible'=> 0 , 'url'=>'admin/task','priv'=>'veda','type'=>'single'],
			['name'=>'reports','visible'=> 0 , 'url'=>'admin/reports','priv'=>'veda','type'=>'single'],
			['name'=>'research','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'scholarship','visible'=> 1 , 'url'=>'apply','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 1 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'users','visible'=> 1 , 'url'=>'admin/users','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 1 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> 1 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 0 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 0 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});				
	}

	public function getIndex()
	{
		$this->layout->content = View::make('admin.index')->with('data',$this->data);
	}

	public function getSchools()
	{
		$schools = School::paginate(15);
		$this->data['schools'] = $schools;
		$this->layout->content = View::make('admin.schools')->with('data',$this->data);
	}

	public function getSchool($id)
	{	
		$this->layout->content = View::make('admin.school')->with('data',$this->data);
	}




	public function getFiles()
	{	
		// $this->data['documents']['public'] = Document::where('entity_type','=','MOHERST_template')->get();
		$this->data['documents']['public'] = Document::whereRaw('entity_type = ? ORDER BY created_at DESC',['MOHERST_template'])->get();
		$this->layout->content = View::make('admin.files')->with('data',$this->data);
	}
	public function getUploads()
	{
		$this->layout->content = View::make('admin.uploads')->with('data',$this->data);
	}

	public function getPrivate()
	{
		$this->data['documents']['private'] = Document::where('type','=','private')->get();
		$this->layout->content = View::make('admin.privatefiles')->with('data',$this->data);
	}
	public function postUploads()
	{
		if (Input::get('document') == 'public') {
			$rules = [
				'file'=> 'required|max:1000|mimes:jpeg,png,doc,docx'
					];
			$validation = Validator::make(Input::file(),$rules);
			if ($validation->fails()) {
				return Redirect::back()->withErrors($validation)->withInput();
			}

			if (Input::input('type') == 'internal') {
				$excert = '/media/public/';
				$path =public_path().'/media/public/';		
				$folder = (File::exists($path))? true : File::makeDirectory($path,  $mode = 0777, $recursive = false);					
				if ($folder) {
					$name = Input::file('file')->getClientOriginalName();
					$rename = str_random(28).'_'.$name;
					$upload = Input::file('file')->move($path,$rename);
					if (File::isFile($path.'/'.$rename)) {
						$id = Session::get('userId');
						$done = Document::create(array(
						'name' => $rename,
						'type' => 'public',
						'title' => $name,
						'uploader_id' => $id,
						'location' => $path.'/'.$rename,
						'deleted' => 0,
						'dir' => $path,
						'excert' => $excert,
						'file_owner' => 0,
						'active' => 1,
						'from_entity_id' =>	0							
						));
						if ($done) {
							return Redirect::to('admin/files');
						}
					}
				}
			}elseif(Input::input('type') == 'private'){
				$userName = Session::get('userName');
				$userId = Session::get('userId');
				$userDomain = Session::get('userDomain');
				$excert = '/media/users/'.$userName.'_'.$userId.'_'.$userDomain ;
				$path =public_path().'/media/users/'.$userName.'_'.$userId.'_'.$userDomain ;	

				$folder = (File::exists($path))? true : File::makeDirectory($path,  $mode = 0777, $recursive = false);					
				if ($folder) {
					$name = Input::file('file')->getClientOriginalName();
					$rename = Input::get('type').str_random(28).'_'.$name;
					$upload = Input::file('file')->move($path,$rename);
					if (File::isFile($path.'/'.$rename)) {
						$id = Session::get('userId');
						$done = Document::create(array(
						'name' => $rename,
						'type' => 'private',
						'title' => $name,
						'uploader_id' => $id,
						'location' => $path.'/'.$rename,
						'deleted' => 0,
						'dir' => $path,
						'excert' => $excert,
						'file_owner' => 0,
						'active' => 1,
						'from_entity_id' =>	0							
						));
						if ($done) {
							return Redirect::to('admin/private');
						}
					}
				}
			}elseif(Input::input('type') == 'schools'){
				$datetime = date('now');
				$userName = Session::get('userName');
				$userId = Session::get('userId');
				$userDomain = Session::get('userDomain');
				$excert = '/media/schools/';
				$path =public_path().'/media/schools/';	
				$folder = (File::exists($path))? true : File::makeDirectory($path,  $mode = 0777, $recursive = false);					
				if ($folder) {
					$name = Input::file('file')->getClientOriginalName();
					$rename = $datetime.'_'.$userId.'_'.$name;
					$upload = Input::file('file')->move($path,$rename);
					if (File::isFile($path.'/'.$rename)) {
						$id = Session::get('userId');
						$done = Document::create(array(
						'name' => $rename,
						'type' => 'schools',
						'title' => $name,
						'uploader_id' => $id,
						'location' => $path.'/'.$rename,
						'deleted' => 0,
						'dir' => $path,
						'excert' => $excert,
						'file_owner' => 0,
						'active' => 1,
						'from_entity_id' =>	0							
						));
						if ($done) {
							return Redirect::to('admin/files');
						}
					}
				}
			}else{
	            $sch = preg_split('/[#]+/',Input::input('type'));
	            $excert ='/media/schools/'.$sch[0].'_'.$sch[1];
				$path =public_path().'/media/schools/'.$sch[0].'_'.$sch[1];		
				$folder = (File::exists($path))? true : File::makeDirectory($path,  $mode = 0777, $recursive = false);					
				if ($folder) {
					$name = Input::file('file')->getClientOriginalName();
					$rename = str_random(28).'_'.$name;
					$upload = Input::file('file')->move($path,$rename);
					if (File::isFile($path.'/'.$rename)) {
						$id = Session::get('userId');
						$done = Document::create(array(
						'name' => $rename,
						'type' => 'school',
						'title' => $name,
						'uploader_id' => $id,
						'location' => $path.'/'.$rename,
						'deleted' => 0,
						'dir' => $path,
						'excert' => $excert,
						'file_owner' => $sch[1],
						'active' => 1,
						'from_entity_id' =>	0							
						));
						if ($done) {
							return Redirect::to('admin/files');
						}
					}
				}
			}
			

		}
	}

	public function getDownload()
	{
		$r = $_GET['sid'];
		return Response::download($r);
	}


	public function postTemplate()
	{
		$input = Input::all();
		$validation = Validator::make($input,['fileUpload'=>'required|mimes:xlsx,xls,xlt|max:51024']);
		if( $validation->fails() ){
			return Redirect::back()->withInput()->withErrors($validation);
			// return $validation->messages();
		}

		$foldername = DS.'media'.DS.'public'.DS;
		$path = base_path().$foldername;
		$filename = Input::file('fileUpload')->getClientOriginalName();
		$rename = 'MoHERST_template_'.DATDASH.'_'.$filename;
		$filetyp = Input::file('fileUpload')->getMimeType();
		$fileexten = Input::file('fileUpload')->getClientOriginalExtension();
		$uploaded = false;

		if (!File::exists($path)) {
		$status = mkdir($path);	
			if ($status) {
				Input::file('fileUpload')->move($path , $rename);
				Document::create(array(
					'type'=>$filetyp,
					'title'=>$filename,
					'entity_type'=>'MOHERST_template',
					'entity_ID'=>0,
					'fullpath'=>$path.$rename,
					'filename'=>$rename,
					'foldername'=>$foldername,
					'filetype'=>$filetyp,
					'extension'=>$fileexten,
					'userID'=>0
				));
				$uploaded = true;
			}
		}else{	
				Input::file('fileUpload')->move($path , $rename);				
				Document::create(array(
					'type'=>$filetyp,
					'title'=>$filename,
					'entity_type'=>'MOHERST_template',
					'entity_ID'=>0,
					'fullpath'=>$path.$rename,
					'filename'=>$rename,
					'foldername'=>$foldername,
					'filetype'=>$filetyp,
					'extension'=>$fileexten,
					'userID'=>0
				));
				$uploaded = true;
		}

		if($uploaded){
			Session::flash('success', $rename );
			return Redirect::back();
		}

	}

	
	public function getEntry()
	{
		$this->layout->content = View::make('admin.entry')->with('data',$this->data);
	}

	public function getTask()
	{
		// note status explained here
		##############################
		#####  1 means online    #####
		#####  0 means offline   #####
		#####  3 means deleted   #####
		#####  2 means signedout #####
		#####  4 means not-in-yet#####
		#####                    #####
		##############################		

		$usergroup = Session::get('userGroup');
		$userId = Session::get('userId');
		$userName = Session::get('userName');
		$this->data['moherstusers']  = User::whereDeletedAndUsergroupOrDomain(0,'all','admin')->get();
		$this->data['department']  = User::whereDeletedAndUsergroup(0,$usergroup)->get();
		$task = Task::whereRaw('fromID = ? Or toID = ?',array($userId,$userId))->get();
		$this->data['tasks'] = $task->toArray();

		$this->layout->content = View::make('admin.task')->with('data',$this->data);
		 // $task = Task::find($userId)->taskusers;
		 // var_dump($task);
		 // foreach ($task as $key => $value) {
			// var_dump($value);
		 // }
	}
	public function getReports()
	{
		$this->layout->content = View::make('admin.reports')->with('data',$this->data);
	}
	public function getUsers()
	{
		// $schools = User::all();
		// $this->data['schools'] = $schools;
		$users = User::where('deleted','!=',1)->get()->toArray();
		$this->data['users'] = $users;
		$this->layout->content = View::make('admin.users.index')->with('data',$this->data);
	}
	public function getNotification()
	{
		$this->data['documents']['public'] = Document::whereRaw('entity_type = ? AND deleted = ? ORDER BY created_at DESC',['template', 0 ])->get()->toArray();
		$this->layout->content = View::make('admin.notification')->with('data',$this->data);

	}
	public function createuser(){
		
	}
	public function getDeleteuser($id){
		// $this->layout->content = View::make('admin.notification')->with('data',$this->data);
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}