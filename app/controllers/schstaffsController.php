<?php

class SchstaffsController extends SchoolController {

	/**
	 * Display a listing of the resource.
	 * GET /schstaffs
	 *
	 * @return Response
	 */
public function index()
	{
		$staffs = Person::with('staffs')->whereRaw('Pers_learningCenterID = ? AND Pers_Type = ? ',array($this->id,'Staff'))->get()->toArray();	
		$this->data['staffs'] = $staffs;		
		$this->layout->content = View::make('school.staffs')->with('data',$this->data);

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /schstudents/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /schstudents
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /schstudents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $this->layout->content = View::make('school.student')->with('data',$this->data);
		if (Request::ajax())
		{
			$staff = Person::with('classes')->where('Pers_PersonID','=',$id)->first()->toArray();
			$staff['contact'] = Contact::whereRaw('Cont_EntityID = ? AND Cont_EntityType = ?',[$student['Pers_PersonID'],'Staff'])->get()->toArray();
		   return Response::json( $staff);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /schstudents/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /schstudents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$validation = Validator::make(Input::all(), []);
		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput();
		}
		// $t = new DateTime;
		// $today = date("Y-m-d H:i:s");    
		// Date::now(); 
		// dd( $today );
		$updated = Person::where('Pers_PersonID','=',Input::get('persId'))->update([
			'Pers_FirstName'=>Input::get('FirstName'),
			'Pers_MiddleName'=>Input::get('MiddleName'),
			'Pers_LastName'=>Input::get('LastName'),
			'Pers_DOB'=>Input::get('bday').'/'.Input::get('bmonth').'/'.Input::get('byear'), 
			'Pers_Gender'=>Input::get('gender'),
			'Pers_Nationality'=>Input::get('nation'),
			'Pers_UpdateDate'=> DATETIME,
			'Pers_Ethnicity'=>Input::get('enit')
			]);
		if( $updated ){
			Session::flash('success', 'updated'.Input::get('FirstName'));
			return Redirect::back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /schstudents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}