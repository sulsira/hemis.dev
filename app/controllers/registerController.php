<?php

class RegisterController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /register
	 *
	 * @return Response
	 */

	protected $layout = 'research.master';
	public $data =  array();
	protected $menu = array();

	public function __construct(){

			$name = Session::get('userName');
			$userDetails = Auth::user();
			$username = ($name)? $name : ''; 
			if ( empty($username) && !empty($userDetails) ) {
				$username = $userDetails->fullname || $userDetails->email;
			}
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'researchers','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'publications','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
			['name'=>'register','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
			['name'=>'upload','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
			['name'=>'research','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'scholarship','visible'=> 1 , 'url'=>'apply','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 1 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	
		
				
	}

	public function index()
	{
		if(Auth::check()){
			$user = Auth::user();
			if ($user->status == 3) {
				return Redirect::to($user->domain.'/'.$user->userGroup.'/'.'start');
			}
		}
		$this->layout->content = View::make('research.register')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /register/create
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /register
	 *
	 * @return Response
	 */
	public function store()
	{

		$input = Input::all();
		$validation = Validator::make($input,['fullname'=>'required', 'email'=>'required|email|unique:users,email','photo'=>'required|mimes:jpg,png,jpeg|max:1024','password'=>'required|min:6']);
		if( $validation->fails() ){
			return Redirect::back()->withInput()->withErrors($validation)->with('userdata',Auth::user());
		}
			$researcher = Researcher::create(array(
				'email'=>Input::get('email'),
				'type'=>'registered',
				'fullname'=>Input::get('fullname')
				));
			$user = User::create(array(
					'username'=>str_random(8),
					'password'=>Hash::make(Input::get('password')),
					'fullname'=>Input::get('fullname'),
					'privileges'=>'ve',
					'domain'=>'research',
					'link'=>'1',
					'userType'=>'researcher',
					'userGroup'=>'researcher',
					'lickId'=>$researcher->id,
					'email'=>Input::get('email')
				));
			Researcher::whereId($researcher->id)->update(array(
				'userID' => $user->id
				));
				$not = 'media'.DS.'research'.DS.'researchers'.DS.$user->username.'_'.$user->fullname.DS;
				$foldername = DS.$not;
				$path = base_path().$foldername ;
				$filename = Input::file('photo')->getClientOriginalName();
				$rename = $researcher->fullname.'_passportPhoto_'.$user->username.'_'.$filename;
				$filetyp = Input::file('photo')->getMimeType();
				$fileexten = Input::file('photo')->getClientOriginalExtension();
				$thumbnail = $not.'thumbnail'.DS;
				$tfile = $thumbnail.$rename;
				$uploaded = false;

				if (!File::exists($path)) {
					$status = mkdir($path);
					if ($status) {

						Input::file('photo')->move($path , $rename);

						$status = mkdir(base_path().DS.$thumbnail);
						if ($status) {
							$img = Image::make($not.$rename);
							$img->resize(128, 128);
							$img->save($thumbnail.$rename);	
						}	
						Document::create(array(
							'type'=>'photo',
							'title'=>$filename,
							'entity_type'=>'researcher',
							'entity_ID'=>$researcher->id,
							'fullpath'=>$path.'/'.$rename,
							'filename'=>$rename,
							'foldername'=>$foldername,
							'filetype'=>$filetyp,
							'extension'=>$fileexten,
							'userID'=>$researcher->id,
							'thumnaildir'=>$thumbnail
						));
						$uploaded = true;
					}
				}else{	
						Input::file('photo')->move($path , $rename);
						$status = mkdir(base_path().DS.$thumbnail);
						if ($status) {
							// dd(	$tfile );
							$img = Image::make($not.$rename);
							$img->resize(128, 128);
							$img->save($thumbnail.$rename);	
						}				

						Document::create(array(
							'type'=>'photo',
							'title'=>$filename,
							'entity_type'=>'researcher',
							'entity_ID'=>$researcher->id,
							'fullpath'=>$path.'/'.$rename,
							'filename'=>$rename,
							'foldername'=>$foldername,
							'filetype'=>$filetyp,
							'extension'=>$fileexten,
							'userID'=>$researcher->id,
							'thumnaildir'=>$thumbnail

						));
						$uploaded = true;
				}
				if($uploaded){
						$creditials = array(
										'username' => $user->username,
										'password' => Input::get('password')
								);

							 	// see if they are in the database
								if ( Auth::attempt($creditials) ) {
									if ( Auth::check() ) {
										$status = User::whereId(Auth::user()->id)->update(array('status'=>3)) ;
									 	// put userdata in session
										Session::put('userName',Auth::user()->username);
										Session::put('userType',Auth::user()->userType);
										Session::put('userId',Auth::user()->id);
										Session::put('userDomain',Auth::user()->domain);
										Session::put('userGroup',Auth::user()->userGroup);
										Session::put('link',Auth::user()->link);
										Session::put('privileges',Auth::user()->privileges);

										//  redirect to  appropriate dashboard
										if ( $status ) {
											return Redirect::intended('research/researcher/start')->with('userdata',Auth::user());
										}else{


										}

									}
								}
				}
	}

	/**
	 * Display the specified resource.
	 * GET /register/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /register/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /register/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /register/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}