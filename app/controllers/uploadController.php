<?php

class UploadController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /upload
	 *
	 * @return Response
	 */


protected $layout = 'research.master';
	/**
	 * Display a listing of the resource.
	 * GET /researcher
	 *
	 * @return Response
	 */

	public $data =  array();
	protected $menu = array();

	public function __construct(){

			$name = Session::get('userName');
			$userDetails = Auth::user();
			$username = ($name)? $name : ''; 
			if ( empty($username) && !empty($userDetails) ) {
				$username = $userDetails->fullname || $userDetails->email ;
			}
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'researchers','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'publications','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
			['name'=>'register','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
			['name'=>'upload','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
			['name'=>'research','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'scholarship','visible'=> 1 , 'url'=>'apply','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 1 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	
		
				
	}
	public function index()
	{
		$this->layout->content = View::make('research.upload')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /upload/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /upload
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input,['fullname'=>'required', 'email'=>'required|email','publtype'=>'required','userfile'=>'required|mimes:pdf,doc,docx|max:1024','abstract'=>'max:200']);
		if( $validation->fails() ){
			return Redirect::back()->withInput()->withErrors($validation);
		}
			$researcher = Researcher::create(array(
				'email'=>Input::get('email'),
				'type'=>'not-registered',
				'fullname'=>Input::get('fullname')
				));
			$pub = Publication::create(array(
				'title'=>Input::get('publtitle'),
				'pub_type'=>Input::get('publtype'),
				'pub_view'=>Input::get('view'),
				'pub_researcherID'=>$researcher->id,
				'pub_abstract'=>e(Input::get('abstract')),
				'pub_discipline'=>Input::get('discipline'),
				'visible'=>1
				));
				$foldername = DS.'media'.DS.'research'.DS.'publications'.DS.$pub->pub_view.'/'.$researcher->fullname.'_'.str_random(28).DS;
				$path = base_path().$foldername ;

				// $foldername = '/media/research/publications/'.$pub->pub_view.'/'.$researcher->fullname.'_'.str_random(28).DS ;
				// $path = public_path().$foldername.DS ;

				$filename = Input::file('userfile')->getClientOriginalName();
				$rename = $researcher->fullname.'_publication_'.str_random(28).'_'.$filename;
				$filetyp = Input::file('userfile')->getMimeType();
				$fileexten = Input::file('userfile')->getClientOriginalExtension();
				$uploaded = false;
				
				if (!File::exists($path)) {
					$status = mkdir($path);
					if ($status) {
						Input::file('userfile')->move($path , $rename);
						Document::create(array(
							'type'=>$filetyp,
							'title'=>$filename,
							'entity_type'=>'publication',
							'entity_ID'=>$pub->id,
							'fullpath'=>$path.'/'.$rename,
							'filename'=>$rename,
							'foldername'=>$foldername,
							'filetype'=>$filetyp,
							'extension'=>$fileexten,
							'userID'=>$researcher->id
						));
						$uploaded = true;
					}
				}else{					
						Input::file('userfile')->move($path , $rename);
						Document::create(array(
							'type'=>$filetyp,
							'title'=>$filename,
							'entity_type'=>'publication',
							'entity_ID'=>$pub->id,
							'fullpath'=>$path.'/'.$rename,
							'filename'=>$rename,
							'foldername'=>$foldername,
							'filetype'=>$filetyp,
							'extension'=>$fileexten,
							'userID'=>$researcher->id
						));
						$uploaded = true;
				}
				if($uploaded){
					Session::flash('success', $pub->title);
					return Redirect::back();
				}
	}

	/**
	 * Display the specified resource.
	 * GET /upload/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /upload/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /upload/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /upload/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}