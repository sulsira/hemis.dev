<?php

class schoolsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $layout = 'school';
	protected $menu = array();
	public $data = array();
	public function __construct(){
		$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'school.index','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 1 , 'url'=>'school.notifications','priv'=>'veda','type'=>'single'],
			['name'=>'moherst','visible'=> 1 , 'url'=>'school.moherst','priv'=>'veda','type'=>'single'],
			['name'=>'username','visible'=> 1 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'school.settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});			
	}
	public function index()
	{
		$this->data[]['userdata'] = Auth::user();
		$data = $this->data;
		$this->layout->content = View::make('school.index')->with('data', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}