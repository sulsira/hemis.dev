<?php

class InstituteController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	// public function __construct(){
	// 	// View::composer('_partials.scope',function($view){
	// 	// 	// return $view->with('id',$this->schoolID);
	// 	// });	
	// }
	public function index()
	{
		$this->layout->content = View::make('admin.school.add')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// $this->layout->content = View::make('admin.school.add')->with('data',$this->data);
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
			$validation = Validator::make(Input::all(),School::$rules);
			if ($validation->fails()) {
				 return Redirect::back()->withErrors($validation)->withInput();
			}

			if (!empty($input['school'])) {

				$school = School::create(array(
				'LeCe_Name' => $input['school']['name'],
				'LeCe_LearningCenterType' => $input['school']['type'],
				'LeCe_Ownership' => $input['school']['ownership'],
				'LeCe_EnrolmentCapacity' => (!empty($input['school']['enrol-capacity']))? $input['school']['enrol-capacity'] : NULL,
				'LeCe_Standing' =>'1',
				'LeCe_Deleted' =>0,
				'LeCe_Classification' => $input['school']['classification']
				));

			}


			if (!empty($input['address'])) {

				Address::create(array(
				'Addr_EntityID' => $school->LeCe_LearningCenterID,
				'Addr_EntityType' => 'Learning Center',
				'Addr_AddressStreet' => $input['address']['street'],
				'Addr_POBox' => $input['address']['pobox'],
				'Addr_Town' => $input['address']['town'],
				'Addr_District' => $input['address']['region']			
				));

			}

			if (!empty($input['cont'])) {

				Contact::create(array(
				'Cont_EntityID' => $school->LeCe_LearningCenterID,
				'Cont_EntityType' => 'Learning Center',
				'Cont_Contact' => $input['cont']['email'],
				'Cont_ContactType' => 'email'			
				));	

				Contact::create(array(
				'Cont_EntityID' => $school->LeCe_LearningCenterID,
				'Cont_EntityType' => 'Learning Center',
				'Cont_Contact' => $input['cont']['mobile'],
				'Cont_ContactType' => 'Phone'			
				));	

				Contact::create(array(
				'Cont_EntityID' => $school->LeCe_LearningCenterID,
				'Cont_EntityType' => 'Learning Center',
				'Cont_Contact' => $input['cont']['tele'],
				'Cont_ContactType' => 'Telephone'			
				));	

				Contact::create(array(
				'Cont_EntityID' => $school->LeCe_LearningCenterID,
				'Cont_EntityType' => 'Learning Center',
				'Cont_Contact' => $input['cont']['website'],
				'Cont_ContactType' => 'website'			
				));	

			}


		if ($input['SaveSchool'] == 'save') {

			Session::flash('success', $input['school']['name'] );
			return Redirect::back();

		}else{

			return Redirect::to('admin/schools');
		}

// 		if (Input::get('SaveSchool') == 'save' || Input::get('doneSchool') == 'done') {
// // dd(Input::all());
// 			$validation = Validator::make(Input::all(),School::$rules);
// 			if ($validation->fails()) {
// 				 return Redirect::back()->withErrors($validation)->withInput();
// 			}
// 			$school = School::create(array(
// 				'LeCe_Name' => Input::get('LeCe_Name'),
// 				'LeCe_LearningCenterNo' =>'Null' ,
// 				'LeCe_MainPersonID' => 0,
// 				'LeCe_LearningCenterType' =>Input::get('LeCe_LearningCenterType'),
// 				'LeCe_Ownership' =>Input::get('LeCe_Ownership'),
// 				'LeCe_EnrolmentCapacity' =>Input::get('LeCe_EnrolmentCapacity'),
// 				'LeCe _FinancialSource' =>'',
// 				'LeCe_Standing' =>'1',
// 				'LeCe_Deleted' =>0,
// 				'LeCe_Classification' =>Input::get('LeCe_Classification'),
// 				'LeCe_AccountManager' => ''
// 			));


// 		if ($school) {

// 			Address::create(array(
// 			'Addr_EntityID' => $school->LeCe_LearningCenterID,
// 			'Addr_EntityType' => 'Learning Center',
// 			'Addr_AddressStreet' => Input::get('street'),
// 			'Addr_POBox' => Input::get('pobox'),
// 			'Addr_Town' => Input::get('town'),
// 			'Addr_District' => Input::get('district')			
// 			));
// 			if(Input::get('email')){

// 				Contact::create(array(
// 				'Cont_EntityID' => $school->LeCe_LearningCenterID,
// 				'Cont_EntityType' => 'Learning Center',
// 				'Cont_Contact' => Input::get('email'),
// 				'Cont_ContactType' => 'email'			
// 				));	

// 			}
// 			if (Input::get('telephone')) {
// 				Contact::create(array(
// 				'Cont_EntityID' => $school->LeCe_LearningCenterID,
// 				'Cont_EntityType' => 'Learning Center',
// 				'Cont_Contact' => Input::get('telephone'),
// 				'Cont_ContactType' => 'telephone'			
// 				));				
// 			}
// 			if (Input::get('mobile')) {
// 				Contact::create(array(
// 				'Cont_EntityID' => $school->LeCe_LearningCenterID,
// 				'Cont_EntityType' => 'Learning Center',
// 				'Cont_Contact' => Input::get('mobile'),
// 				'Cont_ContactType' => 'mobile'			
// 				));		
// 			}

// 			if (Input::get('website')) {
// 				Contact::create(array(
// 				'Cont_EntityID' => $school->LeCe_LearningCenterID,
// 				'Cont_EntityType' => 'Learning Center',
// 				'Cont_Contact' => Input::get('website'),
// 				'Cont_ContactType' => 'website'			
// 				));				
// 			}




// 			// 'Addr_Region' => Input::get('region')
// 			// 'Addr_Country' => Input::get(''),
// 			// 'Addr_CareOf' => Input::get(''),
// 			// 'Addr_Deleted' => Input::get(''),
// 			// 'Addr_AddDate' => Input::get(''),

// 		}
// 		if (Input::get('SaveSchool')) {
// 			return Redirect::back();
// 		}else{
// 			return Redirect::to('admin');
// 		}


// 		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
		$this->data['school'] = School::with('persons')->where('LeCe_LearningCenterID','=',$id)->first()->toArray();
		$this->data['courses'] = School::find($id)->courses()->get()->toArray();
		$this->layout->content = View::make('admin.school.school')->with('data',$this->data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}
	public function getStudents($id)
	{
		//
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}