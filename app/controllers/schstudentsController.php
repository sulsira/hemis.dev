<?php

class SchstudentsController extends schoolController {

	/**
	 * Display a listing of the resource.
	 * GET /schstudents
	 *
	 * @return Response
	 */
	public $students = array();

	public function index()
	{
		$input = Input::all();
		$data = array();
		$students = Person::with('classes')->whereRaw('Pers_learningCenterID = ? AND Pers_Type = ? ',array($this->id,'Student'))->get()->toArray();	
		foreach ($students as $key => $value) {
			foreach ($value['classes'] as $key1 => $value1) {
				$data[pull_year($value1['Clas_AdmissionDate'])][] = $value;
			}
		}
		$this->data['students'] = $data;
		if (empty($data)) {
			$this->data['students'] = $students;
		}
		
		if (Request::ajax()) {
			return Response::json( $data[$input['y']]);
		}
		$this->layout->content = View::make('school.students')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /schstudents/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('school.add_student')->with('data',$this->data);
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /schstudents
	 *
	 * @return Response
	 */
	public function store()
	{	
		
		// $person = new Person;
		$person = array();
		$address = array();
		$class = array();
		$input = Input::all();


		// a loop that would go through inputs and then send all values to respective arrays
		$input['class']['Clas_AdmissionDate'] = date_standard($input['class']['Clas_AdmissionDate']);
		foreach ($input as $key => $value) {
			$$key = $value;
		}
		$person = array_add($person, 'Pers_LearningCenterID', $this->id);
		$person = array_add($person, 'Pers_Type', 'Student');
		$person = array_add($person, 'Pers_AddDate', DATETIME);
		$person = array_add($person, 'Pers_Deleted', 0);



		$uploaded = false;	
		$rules = [
			'person.Pers_FirstName' => 'required|max:200',
			'person.Pers_LastName' => 'required|max:200',
			'class.Clas_LocalClassName' => 'required|max:200',
			'class.Clas_AdmissionDate' => 'required|max:200'
		];
		$validation = Validator::make($input , $rules);
		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput();
		}

		$student = Person::create($person);

		if ( $student ) {

			if(!empty($address)){

				$address = array_add($address, 'Addr_EntityID', $student->Pers_PersonID);
				$address = array_add($address, 'Addr_EntityType', 'Student');
				$address = array_add($address, 'Addr_AddDate', DATETIME);
				$address = array_add($address, 'Addr_Deleted', 0);
				$address = Address::create($address);
			}
			if(!empty($class)){

				$class = array_add($class,'Clas_PersonID', $student->Pers_PersonID);
				$class = array_add($class,'Clas_LearningCenterID', $this->id);
				$class = array_add($class,'Clas_LearningCenterID', $this->id);

				$class = Classes::create($class);
			}
			$uploaded = true;

	}

	// dd($person);
	if($uploaded){

		if ( $input['save'] == 'save' ) {
			
			Session::flash('success', $student->Pers_FirstName);
			return Redirect::back();

		}else if( $input['save'] == 'done' ){

			return Redirect::intended('school/students');

		}

	}

}

	/**
	 * Display the specified resource.
	 * GET /schstudents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $this->layout->content = View::make('school.student')->with('data',$this->data);
		if (Request::ajax())
		{
			$student = Person::with('classes')->where('Pers_PersonID','=',$id)->first()->toArray();
			$student['contact'] = Contact::whereRaw('Cont_EntityID = ? AND Cont_EntityType = ?',[$student['Pers_PersonID'],'Student'])->get()->toArray();
		   return Response::json( $student);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /schstudents/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /schstudents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$validation = Validator::make(Input::all(), []);
		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput();
		}

		$updated = Person::where('Pers_PersonID','=',Input::get('persId'))->update([
			'Pers_FirstName'=>Input::get('FirstName'),
			'Pers_MiddleName'=>Input::get('MiddleName'),
			'Pers_LastName'=>Input::get('LastName'),
			'Pers_DOB'=>Input::get('bday').'/'.Input::get('bmonth').'/'.Input::get('byear'), 
			'Pers_Gender'=>Input::get('gender'),
			'Pers_Nationality'=>Input::get('nation'),
			'Pers_UpdateDate'=> DATETIME,
			'Pers_Ethnicity'=>Input::get('enit')
			]);
		if( $updated ){
			Session::flash('success', 'updated'.Input::get('FirstName'));
			return Redirect::back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /schstudents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}