<?php

class TasksController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user =  ['id' => 13,
				'username' => 'developer',
				'password' => '$2y$10$7XdQnnqkrTp9N.AHOykwBuyX1M./N8SMuc/oss0TYpvFS7ukkAnMy',
				'userType' => 'internal',
				'fullname' => 'developer',
				'ip' => 0,
				'privileges' => 'veda',
				'domain' => 'admin',
				'userGroup' => 'admin'];
	$data = ['user'=>$user];

return View::make('tasks.index');
   
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('tasks.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Request::ajax()) {
			if (Input::get('pid')) {
				$User = User::where('id','=',Input::get('pid'))->first();
				return Response::json($User);
			}else{
				switch (Input::get('type')) {
					case 'depart':
						$depart = Session::get('userGroup');
						$User = User::where('userGroup','=',$depart)->get();
						return Response::json($User);
						break;
					case 'all':
						$User = User::all();
						return Response::json($User);
						break;
					case 'smt':
						$User = User::where('id','=',Input::get('pid'))->first();
						return Response::json($User);
						break;
					case 'directors':
						$User = User::where('id','=',Input::get('pid'))->first();
						return Response::json($User);
						break;

					case 'sti':
						$User = User::where('id','=',Input::get('pid'))->first();
						return Response::json($User);
						break;
					case 'he':
						$User = User::where('id','=',Input::get('pid'))->first();
						return Response::json($User);
						break;

					case 'admin':
						$User = User::where('id','=',Input::get('pid'))->first();
						return Response::json($User);
						break;
					case 'research':
						$User = User::where('id','=',Input::get('pid'))->first();
						return Response::json($User);
						break;
					case 'messengers':
						$User = User::where('id','=',Input::get('pid'))->first();
						return Response::json($User);
						break;

				}
			}
			
			if (Input::get('type') == 'task') {
				$validation = Validator::make(Input::all(),['title' => 'required','details'=> 'required', 'publish'=>'date']);
			
			if ($validation->fails()) {
				$error = ['error' => 'messages'];
				$message = $validation->messages();
				return Response::json( $message );	

			}
				$users = array();

				foreach (Input::all() as $key => $value) {
					if (ends_with($key, 'userId')) {
						$users[] = $value;
					}					
				}
				$data = array();
				if (!empty($users)) {
					$userId = Session::get('userId');
					$userName = Session::get('userName');
					$date = (Input::get('publish') == '0000-00-00' )? Input::get('publish') : '0000-00-00';
					$tasking = false;
					$task = Task::create(array(
						'title' =>Input::get('title'),
						'details' =>Input::get('details'),
						'due' =>$date,
						'from' =>$userName,
						'fromID' => $userId,
						'remarks' =>null,
						'reassign' =>null,
						'reassignID' =>0,
						'deleted' =>0
						));					
					foreach ($users as $key1 => $value1) {
						if ( Input::get('email') != 'on' ) {	
									Taskusers::create(array(
										'toID' => $value1,
										'fromID' => $userId,
										'taskID' => $task->id,
										'statusID' => '9',
										'opened' => '0', 
										'emailed' => '0',
										'deleted' => '0'
									));								
									$tasking = true;							
						}else{
							#PLEASE 
							// Task::create(array(
							// 	'title' =>Input::get('title'),
							// 	'details' =>Input::get('details'),
							// 	'due' =>$date,
							// 	'from' =>$userName,
							// 	'to' =>0,
							// 	'fromID' => $userId,
							// 	'toID' =>$value1,
							// 	'remarks' =>null,
							// 	'reassign' =>null,
							// 	'reassignID' =>0,
							// 	'deleted' =>0,
							// 	'emailed' => 1
							// 	));	
						}
					$tasking = true;
					}
				return Response::json(  $task  );
				}
					
				return Response::json( $task );
			} //end of task


		} //end of ajax request

					// $userId = Session::get('userId');
					// $userName = Session::get('userName');
					// $date = (Input::get('publish') == '0000-00-00' )? Input::get('publish') : '0000-00-00';
					// $tasking = false;
					// $task = Task::create(array(
					// 	'title' =>Input::get('title'),
					// 	'details' =>Input::get('details'),
					// 	'due' =>$date,
					// 	'from' =>$userName,
					// 	'fromID' => $userId,
					// 	'remarks' =>null,
					// 	'reassign' =>null,
					// 	'reassignID' =>0,
					// 	'deleted' =>0
					// 	));
					// 			if ($task) {
					// 				$tu = Taskusers::create(array(
					// 					'toID' => $value1,
					// 					'fromID' => $userId,
					// 					'taskID' => $task->id,
					// 				));
					// 			}
								
					// 				$tasking = true;
		// // get all the users in scope
		// 	$users  = User::whereDeletedAndUsergroupOrDomain(0,'all','admin')->get();
		// 	$this->data['moherstUsers'] = $users;
  //       // return View::make('tasks.index');
		// // return Input::all();
		// // check the rules
		// // is email checked then load the email function
		// // check and see if the date is set if yes dont send. 
		// 	#status set to pending 
		// // check the user to task or uses and check if its pending or check and see if the date and email is set then email if not due date
		// 	#when login in a User check and see if they have a task 

		// $this->layout->content = View::make('admin.index')->with('data',$this->data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('tasks.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('tasks.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public static function is_connected()
	{
	   $connected = @fsockopen("www.google.com", 80, $errno, $errstr, 5); //website and port
	    if ($connected){
	        $is_conn = true; //action when connected
	        fclose($connected);
	    }else{
	        $is_conn = false; //action in connection failure
	    }
	    return $is_conn;

	}
	public function getUserask(){
		if (Request::ajax()) {
			dd('tesing');
		}
	}

}
