<?php

class ResearchersController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $layout = 'research.master';
	public $data =  array();
	protected $menu = array();

	public function __construct(){

			$name = Session::get('userName');
			$userDetails = Auth::user();
			$username = ($name)? $name : ''; 

			if ( empty($username) && !empty($userDetails) ) {
				$username = $userDetails->fullname || $userDetails->email;
			}
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'researchers','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'publications','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
			['name'=>'register','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
			['name'=>'upload','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
			['name'=>'research','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'scholarship','visible'=> 1 , 'url'=>'apply','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 1 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	
		
				
	}
	public function index()
	{
		// $this->data['publications'] = Publication::with('researcher')->get();
		$results = array();
		$grand = array();
		$publication = Researcher::with('publications')->get()->toArray();
		foreach ($publication as $key => $value) {
			$pub = array();
			$doc = Document::whereRaw("entity_id = ?",array($value['id']))->get();
			$pub['researcher'] = $value;
			$pub['document'] = $doc;
			$results[] = $pub;
		}
		//GROUPING STARTS BELOW
			foreach ($results as $key0 => $value0) {
				if ($value0['researcher']['type'] == 'registered') {
					$grand[$value0['researcher']['type']][] = $value0;
				}else{
					$grand[$value0['researcher']['type']][] = $value0;
				}
			}

		// $this->data['publications'] = Publication::with('document')->get();

        $this->layout->content = View::make('research.index')->with('data',$grand);
	}
	public function showUpload()
	{
        $this->layout->content = View::make('research.index')->with('data',$this->data);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('researchers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id = 0)
	{

		// take the id of the user check if its number /
		// 	redirect if its not a number
		// check if the number exist in the database /
		// 	redirect if it does not exist
		// else if exist
		// then collect all the researcher details
		// collect all the researcher pulications and link each publication to a document


		$data['id'] = $id;
		$validation = Validator::make($data,['id'=>'integer']);
		if ( $validation->fails()) {
			var_dump('this pages failed');
		}
		$results = array();
		$researcher = Researcher::with('publications')->where('id','=',$id)->get()->toArray();

		if(empty($researcher)){
			var_dump('expression');
		}

		foreach ($researcher as $key => $value) {

			$pub = array();
			$doc = Document::whereRaw("entity_ID = ? and entity_type != ?",array($value['id'],'publication'))->get();
			$pub['researcher'] = $value;
			$pub['document'] = $doc;
			
			
			foreach ($value['publications'] as $key1 => $value1) {

				$doc = Document::whereRaw("entity_type = ? and entity_ID = ?",array('publication',$value1['id']))->get();
				$pub['publication'][]['detail'] = $value1;
				$pub['publication'][]['detail']['document'] = $doc;

				
			}

			$results[] = $pub;
		}
			$this->layout->content = View::make('research.researchers.index')->with('data',$results);
		// dd($results);

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('researchers.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}
	public function getPublications(){
		 return View::make('research.publications');
	}
	public function getDownload(){
		var_dump('something');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
