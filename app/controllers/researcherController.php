<?php

class ResearcherController extends \BaseController {

protected $layout = 'research.master';
	/**
	 * Display a listing of the resource.
	 * GET /researcher
	 *
	 * @return Response
	 */

	public $data =  array();
	protected $menu = array();

	public function __construct(){

			$name = Session::get('userName');
			$userDetails = Auth::user();
			$username = ($name)? $name : ''; 

			if ( empty($username) && !empty($userDetails) ) {
				$username = $userDetails->fullname || $userDetails->email;
			}
			$this->menu = [
			['name'=>'researchers','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'publications','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
			['name'=>'register','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
			['name'=>'upload','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
			['name'=>'task','visible'=> 0 , 'url'=>'admin/task','priv'=>'veda','type'=>'single'],
			['name'=>'reports','visible'=> 0 , 'url'=>'admin/reports','priv'=>'veda','type'=>'single'],
			['name'=>'users','visible'=> 0 , 'url'=>'admin/users','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	
		
				
	}
	public function index()
	{
		$id = Session::get('lickId');

		if(!$id) Redirect::to('logout');
		$data['id'] = $id; // this id should come from a session

		$validation = Validator::make($data,['id'=>'integer']);
		if ( $validation->fails()) {
			var_dump('this pages failed');
		}
		$results = array();
		$researcher = Researcher::with('publications')->where('id','=',$id)->get()->toArray();

		if(empty($researcher)){
			var_dump('expression');
		}

		foreach ($researcher as $key => $value) {

			$pub = array();
			$doc = Document::whereRaw("entity_ID = ? and entity_type != ?",array($value['id'],'publication'))->get();
			$pub['researcher'] = $value;
			$pub['document'] = $doc;
			
			
			foreach ($value['publications'] as $key1 => $value1) {

				$doc = Document::whereRaw("entity_type = ? and entity_ID = ?",array('publication',$value1['id']))->get();
				$pub['publication'][]['detail'] = $value1;
				$pub['publication'][]['detail']['document'] = $doc;

				
			}

			$results[] = $pub;
		}
			// $this->layout->content = View::make('research.researchers.index')->with('data',$results);








		$this->layout->content = View::make('research.researcher.index')->with('data',$results);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /researcher/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('research.researcher.create')->with('data',$this->data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /researcher
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = Auth::user();
		$input = Input::all();
		switch (Input::get('formType')) {
			case 'start':
						$validation = Validator::make($input,['resume'=>'required|mimes:doc,docx,pdf|max:3024']);
						if( $validation->fails() ){
							return Redirect::back()->withInput()->withErrors($validation)->with('userdata',Auth::user());
						}
							$researcher = Researcher::whereUserid($user->id)->update(array(
								'type'=>'registered',
								'phone'=>Input::get('phone'),
								'address'=>Input::get('address'),
								'qualification'=>Input::get('quali'),
								));
								$foldername = DS.'media'.DS.'research'.DS.'researchers'.DS.$user->username.'_'.$user->fullname.DS ;
								$path = base_path().$foldername ;
								
								$filename = Input::file('resume')->getClientOriginalName();
								$rename = '_cv_'.$user->username.'_'.$filename;
								$filetyp = Input::file('resume')->getMimeType();
								$fileexten = Input::file('resume')->getClientOriginalExtension();
								$uploaded = false;
								
								if (!File::exists($path)) {
									$status = mkdir($path);
									if ($status) {
										Input::file('photo')->move($path , $rename);
										Document::create(array(
											'type'=>'cv',
											'title'=>$filename,
											'entity_type'=>'researcher',
											'entity_ID'=>$user->lickId,
											'fullpath'=>$path.$rename,
											'filename'=>$rename,
											'foldername'=>$foldername,
											'filetype'=>$filetyp,
											'extension'=>$fileexten,
											'userID'=>$user->id
										));
										$uploaded = true;
									}
								}else{					
										Input::file('resume')->move($path , $rename);
										Document::create(array(
											'type'=>'cv',
											'title'=>$filename,
											'entity_type'=>'researcher',
											'entity_ID'=>$user->lickId,
											'fullpath'=>$path.'/'.$rename,
											'filename'=>$rename,
											'foldername'=>$foldername,
											'filetype'=>$filetyp,
											'extension'=>$fileexten,
											'userID'=>$user->id
										));
										$uploaded = true;
								}
								if($uploaded){

									$status = User::whereId($user->id)->update(array('status'=>1));
									//  redirect to  appropriate dashboard
									if ( $status ) {
										return Redirect::intended('research/researcher/add_publication')->with('userdata',Auth::user());
									}

								}
				break;
			case 'addpublication':

						$validation = Validator::make($input,['publtype'=>'required','userfile'=>'required|mimes:pdf,doc,docx|max:1024','abstract'=>'max:200']);
						if( $validation->fails() ){
							return Redirect::back()->withInput()->withErrors($validation);
						}
						$pub = Publication::create(array(
							'title'=>Input::get('publtitle'),
							'pub_type'=>Input::get('publtype'),
							'pub_view'=>Input::get('view'),
							'pub_researcherID'=>$user->lickId,
							'pub_abstract'=>e(Input::get('abstract')),
							'pub_discipline'=>Input::get('discipline'),
							'visible'=>1
							));
							$foldername = DS.'media'.DS.'research'.DS.'researchers'.DS.$user->username.'_'.$user->fullname.DS.'publications'.DS ;
							$path = base_path().$foldername;
							$filename = Input::file('userfile')->getClientOriginalName();
							$rename = $user->fullname.'_publication_'.str_random(28).'_'.$filename;
							$filetyp = Input::file('userfile')->getMimeType();
							$fileexten = Input::file('userfile')->getClientOriginalExtension();
							$uploaded = false;
							if (!File::exists($path)) {
								$status = mkdir($path);
								if ($status) {
									Input::file('userfile')->move($path , $rename);
									Document::create(array(
										'type'=>$filetyp,
										'title'=>$filename,
										'entity_type'=>'publication',
										'entity_ID'=>$pub->id,
										'fullpath'=>$path.'/'.$rename,
										'filename'=>$rename,
										'foldername'=>$foldername,
										'filetype'=>$filetyp,
										'extension'=>$fileexten,
										'userID'=>$user->id
									));
									$uploaded = true;
								}
							}else{					
									Input::file('userfile')->move($path , $rename);
									Document::create(array(
										'type'=>$filetyp,
										'title'=>$filename,
										'entity_type'=>'publication',
										'entity_ID'=>$pub->id,
										'fullpath'=>$path.'/'.$rename,
										'filename'=>$rename,
										'foldername'=>$foldername,
										'filetype'=>$filetyp,
										'extension'=>$fileexten,
										'userID'=>$user->id
									));
									$uploaded = true;
							}

							if($uploaded){
								Session::flash('success', $pub->title);
								$this->layout->content = View::make('research.researcher.index')->with('data',$this->data);
							}
				break;
			default:
			$this->layout->content = View::make('research.researcher.index')->with('data',$this->data);
				break;
		}

	}

	/**
	 * Display the specified resource.
	 * GET /researcher/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($page)
	{
		switch ($page) {
			case  'start':
			 $this->layout->content = View::make('research.researcher.start')->with('data',$this->data);
				break;
			case  'add_publication':
			 $this->layout->content = View::make('research.researcher.addpublication')->with('data',$this->data);
				break;	
			case  'done':
			 $this->layout->content = View::make('research.researcher.done')->with('data',$this->data);
				break;
			case  'settings':
			 $this->layout->content = View::make('research.researcher.settings')->with('data',$this->data);
				break;	
			case  'researchs':

							$id = Session::get('lickId');

							if(!$id) Redirect::to('logout');
							$data['id'] = $id; // this id should come from a session

							$validation = Validator::make($data,['id'=>'integer']);
							if ( $validation->fails()) {
								var_dump('this pages failed');
							}
							$results = array();
							$researcher = Researcher::with('publications')->where('id','=',$id)->get()->toArray();

							if(empty($researcher)){
								var_dump('expression');
							}

							foreach ($researcher as $key => $value) {

								$pub = array();
								$doc = Document::whereRaw("entity_ID = ? and entity_type != ?",array($value['id'],'publication'))->get();
								$pub['researcher'] = $value;
								$pub['document'] = $doc;
								
								
								foreach ($value['publications'] as $key1 => $value1) {

									$doc = Document::whereRaw("entity_type = ? and entity_ID = ?",array('publication',$value1['id']))->get();
									$pub['publication'][]['detail'] = $value1;
									$pub['publication'][]['detail']['document'] = $doc;

									
								}

								$results[] = $pub;
							}

			 			$this->layout->content = View::make('research.researcher.publications')->with('data',$results);
				break;		
			default:
				$this->layout->content = View::make('research.researcher.index')->with('data',$this->data);
				break;
		}
		
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /researcher/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /researcher/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /researcher/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}