<?php

class applicantController extends ScholarshipsController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// $this->layout->content = View::make('scholarship.applicant.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	// // $this->data['documents'] = Scholarship::find($id)->documents;
	// 	$details  = Scholarship::with('documents')->where('id','=',$id)->get();
	// 	if ($details) {
	// 		$this->data['details']  = Scholarship::with('documents')->where('id','=',$id)->get(); 
	// 	}
	// 	$academics  = Scholarship::with('documents')->where('id','=',$id)->get();= Scholarship::find($id)->academics;
	// 	if ($academics) {
	// 		$this->data['academics'] = Scholarship::with('documents')->where('id','=',$id)->get();= Scholarship::find($id)->academics;
	// 	}
	// 	$works  = Scholarship::with('documents')->where('id','=',$id)->get(); = Scholarship::find($id)->works;
	// 	if ($works) {
	// 		$this->data['works']  = Scholarship::with('documents')->where('id','=',$id)->get(); = Scholarship::find($id)->works;
	// 	}
	// 	$scholarship  = Scholarship::with('documents')->where('id','=',$id)->get(); = Scholarship::find($id)->scholarship;
	// 	if ($scholarship) {
	// 		$this->data['scholarship']  = Scholarship::with('documents')->where('id','=',$id)->get(); = Scholarship::find($id)->scholarship;
	// 	}

		$this->data['details']  = Scholarship::with('documents')->where('id','=',$id)->get();
		$this->data['scholarships'] = Scholarship::find($id)->scholarships;
		$this->data['work'] = Scholarship::find($id)->works;
		$this->data['academics'] = Scholarship::find($id)->academics;
 		$this->layout->content = View::make('scholarship.applicant.index')->with('data',$this->data);
		
		 
		
		
 		$this->layout->content = View::make('scholarship.applicant.index')->with('data',$this->data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return $id;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}