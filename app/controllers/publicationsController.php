<?php

class PublicationsController extends \BaseController{

	/**
	 * Display a listing of the resource.
	 * GET /publications
	 *
	 * @return Response
	 */
	protected $layout = 'research.master';
	public $data =  array();
	protected $menu = array();

	public function __construct(){

			$name = Session::get('userName');
			$userDetails = Auth::user();
			$username = ($name)? $name : ''; 
			if ( empty($username) && !empty($userDetails) ) {
				$username = $userDetails->fullname || $userDetails->email  ;
			}


			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'researchers','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'publications','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
			['name'=>'register','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
			['name'=>'upload','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
			['name'=>'research','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'scholarship','visible'=> 1 , 'url'=>'apply','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 1 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];		
		// 	$this->menu = [
		// 	['name'=>'researchers','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
		// 	['name'=>'publications','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
		// 	['name'=>'register','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
		// 	['name'=>'upload','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
		// 	['name'=>'task','visible'=> 0 , 'url'=>'admin/task','priv'=>'veda','type'=>'single'],
		// 	['name'=>'reports','visible'=> 0 , 'url'=>'admin/reports','priv'=>'veda','type'=>'single'],
		// 	['name'=>'users','visible'=> 0 , 'url'=>'admin/users','priv'=>'veda','type'=>'single'],
		// 	['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
		// 	['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
		// 		['name'=>'help','visible'=> 0 , 'url'=>'help','priv'=>'veda','type'=>'single'],
		// 		['name'=>'settings','visible'=> 0 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
		// 		['name'=>'logout','visible'=> 0 , 'url'=>'logout','priv'=>'veda','type'=>'single']
		// 	]]
		// ];
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	
		
				
	}
	public function index()
	{
		#GROUPING PULICATIONS 
		// REGISTERED
		// NOT REGISTERED
		// INCOMPLETE
		$results = array();
		$grand = array();
		$publication = Publication::with('researcher')->get()->toArray();
		foreach ($publication as $key => $value) {
			$pub = array();
			$doc = Document::whereRaw("entity_type = ? and entity_id = ?",array('publication',$value['id']))->get();
			$pub['publication'] = $value;
			$pub['document'] = $doc;
			$results[] = $pub;
		}
		//GROUPING STARTS BELOW
		foreach ($results as $key0 => $value0) {
			if ($value0['publication']['researcher']['type'] == 'registered') {
				$grand[$value0['publication']['researcher']['type']][] = $value0;
			}else{
				$grand[$value0['publication']['researcher']['type']][] = $value0;
			}
		}
		// $r = Researcher::with('publications'Researcher)->get();
		$this->layout->content = View::make('research.publications')->with('data',$grand);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /publications/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /publications
	 *
	 * @return Response
	 */
	public function store()
	{
		
	}

	/**
	 * Display the specified resource.
	 * GET /publications/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		var_dump('expression');
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /publications/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /publications/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /publications/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}