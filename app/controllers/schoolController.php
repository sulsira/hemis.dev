<?php

class SchoolController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /school
	 *
	 * @return Response
	 */
	protected $layout = 'school';
	protected $menu = array();
	public $data = array();
	public  $id = 0;
	public $persons = array();

	public function __construct(){

			$this->beforeFilter(function(){
				$userId = Session::get('userId');
				$data['id'] = Session::get('lickId');
				$validation = Validator::make($data,['id'=>'integer']);
				if ($validation->fails()) {

					return Redirect::intended('logout');
				}
				$user =  User::find($userId);
				
				if ( $user->lickId != $data['id'] ) {

					Auth::logout();					
					Session::flush();
					return Redirect::intended('logout');
				}

				if(!$data['id']){
					return Redirect::intended('logout');

				}
				
			});
			$this->id = Session::get('lickId');	
				#COLLECTING SCHOOL LOGED DATA
					
					//if not redirect to error page
					//else collect all data related to school
				// dd($this->beforeFilter('schoolAuth',[]));
				$this->data['school'] = School::with('courses')->where('LeCe_LearningCenterID','=',$this->id)->first()->toArray();
		
				$name = Session::get('userName');
				$userDetails = Auth::user();

				$username = ($name)?  $userDetails->email : $name; 

			if ( empty($username) && !empty($userDetails) ) {
				$username = $userDetails->fullname || $userDetails->email;
			}
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'learning centers','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
			['name'=>'research','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
			['name'=>'scholarship','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 0 , 'url'=>'admin/task','priv'=>'veda','type'=>'single'],
			['name'=>'hemis','visible'=> 0 , 'url'=>'admin/reports','priv'=>'veda','type'=>'single'],
			['name'=>'users','visible'=> 0 , 'url'=>'admin/users','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		$this->data['userData'] = Auth::user();
		array_push($this->menu, $this->data['school']['LeCe_Name']); //this is not safe this would break the code (end(menu))
		View::composer($this->layout,function($view){

			return $view->with('menu',$this->menu);
		});	
				
	}
	public function index()
	{
		$this->data['students'] = Person::whereRaw('Pers_learningCenterID = ? AND Pers_Type = ? ',array($this->id,'Student'))->get()->toArray();
		$this->layout->content = View::make('school.index')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /school/create
	 * adding 
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /school
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /school/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
		$this->layout->content = View::make('school.students')->with('data',$this->data);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /school/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /school/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /school/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}