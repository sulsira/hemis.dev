<?php
use Symfony\Component\HttpFoundation\Response;

/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 6/1/2015
 * Time: 7:45 PM
 */

class ApiController extends \BaseController{
    protected  $statusCode = 200;
    public function  respondNotFound($message = 'Not Found'){
    return $this->setStatusCode(404)->respondWithError($message);
    }

    public function setStatusCode($statusCode){
       $this->statusCode = $statusCode;
        return $this;
    }
    public function getStatusCode(){
        return $this->statusCode;
    }
    public function respond($data , $headers = []){
        return \Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message){
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }

    public function respondCreated($message){
        return $this->setStatusCode(201)->respond([
            'message' => $message
        ]);
    }
} 