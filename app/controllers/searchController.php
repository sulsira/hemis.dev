<?php

class SearchController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /search
	 *
	 * @return Response
	 */
	private $keywords = '';
	public function index()
	{
		$q = Input::all();

		$this->keywords = $q['q'];

		if (empty($q)) {
			var_dump('nothing to search for');
		}
		if (!empty($q['q'])) {

			if (!empty($q['type'])) {

				if ($q['type'] == 'natural') {

					$this->naturalSearch(e($q['q']),$q['x']);

				}else if($q['type'] == 'index'){

						$this->indexSearch(e($q['q']));
				}
			}else{
				
				$this->naturalSearch(e($q['q']));
			}
		}
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /search/create
	 *
	 * @return Response
	 */
	public function stringCreator()
	{
		return 'hello';
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /search
	 *
	 * @return Response
	 */
	public function indexSearch($string)
	{
		// 
	}

	/**
	 * Display the specified resource.
	 * GET /search/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function naturalSearch($string, $x = 0)
	{
		// $string = "jolloftutors@hotmail.com";
		// $string = "momodou jallow";
		$limit = 0 .','. 28;	
		if(is_numeric($x)){

			if ($x != 1) {

				$count = 10;
				$limit = ( 28 + (  $count * $x  ) ) .','. ( $count * $x );

			}
			
		}
		
		// $fist = DB::table('learningcenter')->whereRaw('
		// 	MATCH(LeCe_Name,LeCe_LearningCenterNo,LeCe_LearningCenterType,LeCe_Ownership,LeCe_EnrolmentCapacity) 
		// 	AGAINST(? IN BOOLEAN MODE) ',[$string])->get();

		// $con = DB::table('contactinfo')
		// 	->select(DB::raw('Cont_ContactInfoID AS primaryKey,Cont_EntityID  AS entity ,Cont_EntityType  AS type, @table := "contact" AS tabal'))
		// 	->whereRaw('
		// 	MATCH(Cont_Contact) 
		// 	AGAINST(? IN BOOLEAN MODE) ',[$string]);	



		// $pr = DB::table('person')
		// 	->select(DB::raw('Pers_PersonID AS primaryKey,Pers_LearningCenterID AS entity,Pers_Type AS type, @table := "person" AS tabal'))
		// 	->whereRaw('
		// 	MATCH(Pers_FirstName,Pers_MiddleName,Pers_LastName,Pers_DOB) 
		// 	AGAINST(? IN BOOLEAN MODE) ',[$string])->union($con);	


		// $lc = DB::table('learningcenter')
		// 	->select(DB::raw(' LeCe_LearningCenterID AS primaryKey ,LeCe_Name AS entity,LeCe_Classification AS type, @table := "lc" AS tabal'))
		// 	->whereRaw('
		// 	MATCH(LeCe_Name,LeCe_LearningCenterNo,LeCe_LearningCenterType,LeCe_Ownership,LeCe_EnrolmentCapacity) 
		// 	AGAINST(? IN BOOLEAN MODE) ',[$string])->unionAll($pr)->get();
	

	$r = DB::select(DB::raw("
				(SELECT

					LeCe_LearningCenterID AS primaryKey , LeCe_Name AS entity,LeCe_Classification AS type, @table := 'lc' AS tabal
					FROM learningcenter
					WHERE MATCH(LeCe_Name,LeCe_LearningCenterNo,LeCe_LearningCenterType,LeCe_Ownership,LeCe_EnrolmentCapacity)
					AGAINST('$string' IN BOOLEAN MODE)
				)
				UNION
				(SELECT

					Pers_PersonID AS primaryKey,Pers_LearningCenterID AS entity,Pers_Type AS type, @table := 'person' AS tabal
					FROM person
					WHERE MATCH(Pers_FirstName,Pers_MiddleName,Pers_LastName,Pers_DOB)
					AGAINST('$string' IN BOOLEAN MODE)
				)UNION
				(SELECT

					Cont_ContactInfoID AS primaryKey,Cont_EntityID  AS entity ,Cont_EntityType  AS type, @table := 'contact' AS tabal
					FROM contactinfo
					WHERE MATCH(Cont_Contact)
					AGAINST('$string' IN BOOLEAN MODE)
				)
				LIMIT $limit

		 "));


		// unionAll
		// $lc = DB::table('learningcenter')->whereRaw('
		// 	MATCH(LeCe_Name,LeCe_LearningCenterNo,LeCe_LearningCenterType,LeCe_Ownership,LeCe_EnrolmentCapacity) 
		// 	AGAINST(? IN BOOLEAN MODE) ',[$string]);

		// $pr = DB::table('person')->whereRaw('
		// 	MATCH(Pers_FirstName,Pers_MiddleName,Pers_LastName,Pers_DOB) 
		// 	AGAINST(? IN BOOLEAN MODE) ',[$string])->union($lc);		


		// $con = DB::table('contactinfo')->whereRaw('
		// 	MATCH(Cont_Contact) 
		// 	AGAINST(? IN BOOLEAN MODE) ',[$string])->union($pr)->get();	

		// $pr = DB::table('person')->whereRaw('MATCH(Pers_Type,Pers_GivenID,Pers_FirstName,Pers_LastName,Pers_MiddleName,Pers_DOB,Pers_Gender,Pers_Nationality,Pers_Ethnicity,Pers_Disability,Pers_NIN) 
		// AGAINST(`jollof tutor` IN BOOLEAN MODE)')->get();

		// dd($r);
		// $this->limit($r,$postion)
			$this->fetch($r); //should be used after limit when ajax is implemented

		// look in tables: learning center:name, person:name!age , contact:email!phone, 
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /search/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function limit($array,$postion)
	{
		// reads an array from a position
		// return all values up to the limit stop
		// calls fetch fn to to fect the details of the array from database
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /search/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function fetch($array)
	{
		// checks for the values
		$found = array();
		$results =  array();

			if (!empty($array)) {
				foreach ($array as $key => $value) {

						if ($value->tabal == 'lc') {
							if (strtolower($value->entity) == strtolower($this->keywords)) {
								$sch = School::find($value->primaryKey)->toArray();
								$found[] = $sch;

							}
							$sch = School::find($value->primaryKey)->toArray();
							$results['school'][] = $sch;
						}else if($value->tabal == 'person'){
							if (strtolower($value->entity) == strtolower($this->keywords)) {
								$person = Person::find($value->primaryKey)->toArray();
								$found[] = $person;
							}
							$person = Person::find($value->primaryKey)->toArray();
							$results['person'][] = $person;
						}else if($value->tabal == 'contact'){
							if (strtolower($value->entity) == strtolower($this->keywords)) {
								$contact = Contact::find($value->primaryKey)->toArray();
								$found[] = $contact;
							}
							$contact = Contact::find($value->primaryKey)->toArray();
							$results['contact'][] = $contact;
						}else{
							$table = ($value['tabal'] == 'lc') ? 'learningcenter' : $value['tabal'];
							$unknown = DB::table($table)->get();
							if (!empty($unknown)) {
								$results['unknown'][] = $unknown;
							}
						}						

				}
				//fn replace array 
				if (!empty($found)) {
					$placeholder = array();
					$plugedvalye = array();
					$done = array();
					// read through the array
					foreach ($results as $key => $value) {
						if ($key == 'school') {
							foreach ($value as $key1 => $value1) {
								if ($key1 == 0) {
									$plugedvalye = $value1;
								}
								$placeholder[$key][] = $value1;
							}						
						}else{
							$placeholder[$key][] = $value1;
						}


					}
					$done['school'][] = $found[0];
					foreach ($placeholder as $key => $value) {
						foreach ($value as $key => $val) {
							if ($key == 'school') {
								$done['school'][] = $val;
							}else if($key == 'contact'){
								$done['contact'][] = $val;
							}
							
						}
						
					}
					$results = $done;
				}

				// find a key 
				// remove the key and value
				// give the give to a new value
				// append the remove key and value to the end of a new array
					
			$this->results($results);
				// var_dump($found);
			}else{


				$this->results([]);
			}
		// confirm which table to collect details
		// returns details and relevant relationships
		// calls results

		// let the results array full
			// the read through it and search for 

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /search/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function results($array)
	{
		//organises the the array if there is any disorder
		// returns the array
			$this->data['search'] = $array;
			$this->layout->content = View::make('admin.search')->with('data',$this->data);

	}

}