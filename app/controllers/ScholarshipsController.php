<?php

class ScholarshipsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $layout = 'scholarship';
	public $data =  array();
	protected $menu = array();
	public function __construct(){
			$name = Session::get('userName');
			$userDetails = Auth::user();
			$username = ($name)? $name : '';
			if (empty($username)) {
				$username = $userDetails->fullname || $userDetails->email;
			}
				$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'scholarship','priv'=>'veda','type'=>'single'],
			['name'=>'students','visible'=> 1 , 'url'=>'scholarship/students','priv'=>'veda','type'=>'single'],
			['name'=>'scholarships','visible'=> 1 , 'url'=>'scholarship/scholarships','priv'=>'veda','type'=>'single'],
			['name'=>'interviews','visible'=> 1 , 'url'=>'scholarship/interviews','priv'=>'veda','type'=>'single'],
			['name'=>'awards','visible'=> 1 , 'url'=>'scholarship/awards','priv'=>'veda','type'=>'single'],
			['name'=>'head count','visible'=> 1 , 'url'=>'scholarship/headcount','priv'=>'veda','type'=>'single'],
			['name'=>'scholarships','visible'=> 0 , 'url'=>'scholarship/scholarships','priv'=>'veda','type'=>'single'],
			['name'=>'codes','visible'=> 1 , 'url'=>'scholarship/codes','priv'=>'veda','type'=>'single'],
			['name'=>'scholarships','visible'=> 0 , 'url'=>'scholarship/scholarships','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> 1 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'scholarship/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];
		
		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	
	}
	public function getIndex()
	{
		$this->data['scholarshipsstudents'] = Scholarship::all();
		$this->data['applications']  = ScholarshipApplication::all();
       $this->layout->content = View::make('scholarship.index')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getStudents()
	{
		 $this->layout->content = View::make('scholarship.students');
        
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function getScholarships()
	{
       $this->layout->content = View::make('scholarship.scholarships');
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getInterviews()
	{
       $this->layout->content = View::make('scholarship.interviews');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getAwards()
	{
       $this->layout->content = View::make('scholarship.awards');
        
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getHeadcount()
	{
       $this->layout->content = View::make('scholarship.headcount');
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getSector()
	{
       $this->layout->content = View::make('scholarship.sector');
		
	}
	public function getCodes()
	{
      return View::make('scholarshipcodes.index');
		
	}
}
