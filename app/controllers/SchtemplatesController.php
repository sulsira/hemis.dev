<?php

class SchtemplatesController extends SchoolController {

	/**
	 * Display a listing of the resource.
	 * GET /schtemplates
	 *
	 * @return Response
	 */
	public function index()
	{
		// $docs = Document::whereRaw('entity_type = ? AND entity_ID = ?',array('template',1))->get()->toArray();
		$this->data['docs'] = Document::whereRaw('entity_type = ? AND entity_ID = ?',array('template',$this->id))->get()->toArray();

		$this->layout->content = View::make('school.template')->with('data',$this->data);
		// dd($docs);

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /schtemplates/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /schtemplates
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /schtemplates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /schtemplates/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /schtemplates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$validation = Validator::make(Input::all(),['fileUpload'=>'required|mimes:xlsx,xls,xlt|max:51024']);
		if( $validation->fails() ){
			return Redirect::back()->withInput()->withErrors($validation);
		}

		$foldername = DS.'media'.DS.'schools'.DS.$this->data['school']['LeCe_Name'].'_'.$this->data['school']['LeCe_LearningCenterID'].DS;
		$path = base_path().$foldername;

		$filename = Input::file('fileUpload')->getClientOriginalName();
		$rename = '_template_'.DATDASH.'_'.$filename;
		$filetyp = Input::file('fileUpload')->getMimeType();
		$fileexten = Input::file('fileUpload')->getClientOriginalExtension();
		$uploaded = false;

		if (!File::exists($path)) {
		$status = mkdir($path);	
			if ($status) {
				Input::file('fileUpload')->move($path , $rename);
				Document::create(array(
					'type'=>$filetyp,
					'title'=>$filename,
					'entity_type'=>'template',
					'entity_ID'=>$this->data['school']['LeCe_LearningCenterID'],
					'fullpath'=>$path.$rename,
					'filename'=>$rename,
					'foldername'=>$foldername,
					'filetype'=>$filetyp,
					'extension'=>$fileexten,
					'userID'=>0
				));
				$uploaded = true;
			}
		}else{	
				Input::file('fileUpload')->move($path , $rename);				
				Document::create(array(
					'type'=>$filetyp,
					'title'=>$filename,
					'entity_type'=>'template',
					'entity_ID'=>$this->data['school']['LeCe_LearningCenterID'],
					'fullpath'=>$path.$rename,
					'filename'=>$rename,
					'foldername'=>$foldername,
					'filetype'=>$filetyp,
					'extension'=>$fileexten,
					'userID'=>0
				));
				$uploaded = true;
		}

		if($uploaded){
			Session::flash('success', $rename );
			return Redirect::back();
		}


	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /schtemplates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}