<?php

class scholarapplyController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $layout = 'apply';
	protected $menu = array();
	protected $data = array();
	public function __construct(){

			$name = Session::get('userName');
			$userDetails = Auth::user();

			$username = ($name)?  $userDetails->email : $name; 

			if ( empty($username) && !empty($userDetails) ) {
				$username = $userDetails->fullname || $userDetails->email;
			}
					
			$this->menu = [
			['name'=>'home','visible'=> 1 , 'url'=>'/','priv'=>'veda','type'=>'single'],
			['name'=>'researchers','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'publications','visible'=> 1 , 'url'=>'research/publications','priv'=>'veda','type'=>'single'],
			['name'=>'register','visible'=> 1 , 'url'=>'research/register','priv'=>'veda','type'=>'single'],
			['name'=>'upload','visible'=> 1 , 'url'=>'research/upload','priv'=>'veda','type'=>'single'],
			['name'=>'research','visible'=> 1 , 'url'=>'research','priv'=>'veda','type'=>'single'],
			['name'=>'scholarship','visible'=> 1 , 'url'=>'apply','priv'=>'veda','type'=>'single'],
			['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
			['name'=>'login','visible'=> 1 , 'url'=>'login','priv'=>'veda','type'=>'single'],
			['name'=>'notifications','visible'=> 0 , 'url'=>'admin/notification','priv'=>'veda','type'=>'single'],
			['name'=>$username,'visible'=> (!empty($username))? 1 : 0 , 'url'=>'#','priv'=>'veda','type'=>'drop', 'menu'=>[
				['name'=>'help','visible'=> 1 , 'url'=>'help','priv'=>'veda','type'=>'single'],
				['name'=>'settings','visible'=> 1 , 'url'=>'admin/settings','priv'=>'veda','type'=>'single'],
				['name'=>'logout','visible'=> 1 , 'url'=>'logout','priv'=>'veda','type'=>'single']
			]]
		];

		$this->data['userData'] = Auth::user();
		View::composer($this->layout,function($view){
			return $view->with('menu',$this->menu);
		});	
		


	}

	public function index()
	{
		$this->layout->content = View::make('scholarship_application.index')->with('data',$this->data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{	

	
		// apply and check for code validity
		$decrypted = '';
		$code  = Input::get('code');
		if ($code) {
			if (Session::has('ecode')) {
				$ecode = Session::get('ecode');
				// dd($ecode);
				$decrypted .= Crypt::decrypt($ecode);
				if ( $code == $decrypted ) {
					return Redirect::to('apply/step1');
				}else{
					Session::forget('ecode');
					Session::forget('name');
					Session::forget('id');
					Session::forget('step');

						$rules = [
							'code' => 'required|exists:scholarshipcodes,code,assigned,1'
						];
						$validation = Validator::make(Input::all(),$rules);
						if ($validation->fails()) {
							return Redirect::back()->withErrors($validation)->withInput();
						}else{
							$respond = Scholarshipcode::whereCode(Input::get('code'))->first();	
							Session::put('ecode',$respond->codeencryption);
							return Redirect::to('apply/step1');
							
						}	
				}
			}else{
			
			$rules = [
				'code' => 'required|exists:scholarshipcodes,code,assigned,1'
			];
			$validation = Validator::make(Input::all(),$rules);
			if ($validation->fails()) {
				return Redirect::back()->withErrors($validation)->withInput();
			}else{
				$respond = Scholarshipcode::whereCode(Input::get('code'))->first();	
				Session::put('ecode',$respond->codeencryption);
				return Redirect::to('apply/step1');
				// dd($respond);
			}
			}
		}

		$decrypted = '';
		$ecode = Session::get('ecode');
		if ($ecode) {
			$resp = Scholarshipcode::whereCodeencryptionAndVisible($ecode,1);	
			if($resp){
				$decrypted .= Crypt::decrypt($ecode);
			}else{
				Session::forget('ecode');
				return Redirect::to('code')->withErrors('sorry your code is disabled');
			}			
		}


			switch (Input::get('here')) {
				case '1':

						$rules = [
								'name'=>'required',
								'dob'=>'required',
								'gender'=>'required',
								'nationality'=>'required',
								'telephone'=>'required|numeric',
								'email'=>'required|email',
								'address'=>'required'
								];
						$validation = Validator::make(Input::all(),$rules);
						if ($validation->fails()){
							return Redirect::back()->withErrors($validation)->withInput();
							// 	$messages = $validation->messages();
						 	// $fail = $validation->failed();

						}else{
		

							// Scholarship::create(array(
							// 		'fullname'=> 'FULLNAME',
							// 		'othernames'=> 'OTHER',
							// 		'dob'=> 'DOB'
									
							// 	));
							$step1 = Scholarship::create(array(
									'fullname'=> Input::get('name'),
									'othernames'=> Input::get('othername'),
									'dob'=> Input::get('dob'),
									'gender'=> Input::get('gender'),
									'nationality'=> Input::get('nationality'),
									'phone'=> Input::get('telephone'),
									'email'=> Input::get('email'),
									'address'=> Input::get('address'),
									'scholarshipID'=> $decrypted,
									'status'=> 3, // 0 = means does not apply yet * 1 = means done * 2 = see by ministry * 3 = means still applying * 4 = means just started applying
									'visibility'=> 1,
									'step'=>Input::get('here'),
									'deleted'=> 0
								));
								Session::put('step',1);
								Session::put('id',$step1->id);
								Session::put('name',$step1->fullname);
								$up = Scholarship::where('id', '=',$step1->id)->update(array('step' => '2'));
								if ($up) {
									Scholarshipcode::whereCode($decrypted)->update(array('studentID'=>$step1->id));
									return Redirect::to('apply/step2');
								}
								
						}
					break;
				case '2':
					$id = Session::get('id');

					$rules = [
						'sch1' => '',
						'qua1' => '',
						'sch2' => '',
						'qua2' => '',
						'sch3' => '',
						'qua3' => ''
					];
					$validation = Validator::make(Input::all(),$rules);
					if ($validation->fails()){
						return Redirect::back()->withErrors($validation)->withInput();
					}else{

						
							 ScholarshipAcademics::create(array(
							 'scholarshipID' =>	$decrypted,
							 'infoID' => $id,	
							 'schoolname' => Input::get('sch1'),	
							 'schoolcertification' => Input::get('qua1'),		
							 'status' => 1,	
							 'visibility' => 1,	
							 'step' =>	2,
							 'deleted' => 0	
							));
						
							

				
							 ScholarshipAcademics::create(array(
							 'scholarshipID' =>	$decrypted,
							 'infoID' => $id,	
							 'schoolname' => Input::get('sch2'),	
							 'schoolcertification' => Input::get('qua2'),		
							 'status' => 1,	
							 'visibility' => 1,	
							 'step' =>	2,
							 'deleted' => 0
							));
				


						ScholarshipAcademics::create(array(
							 'scholarshipID' =>	$decrypted,
							 'infoID' => $id,	
							 'schoolname' => Input::get('sch3'),	
							 'schoolcertification' => Input::get('qua3'),		
							 'status' => 1,	
							 'visibility' => 1,	
							 'step' =>	2,
							 'deleted' => 0
							));


								Session::forget('step');
								Session::put('step',2);
								Session::put('id',$id);
								$name = Session::get('name');
								Session::put('name',$name);
								$up = Scholarship::where('id', '=',$id)->update(array('step' => '3'));
								if ($up) {
									return Redirect::to('apply/step3');
								}
					}
								
					break;
				case '3':
				$id = Session::get('id');
					$rules = [
						'name' => '',
						'lenght' => '',
						'position' => ''
						];
					$validation = Validator::make(Input::all(),$rules);
					if ($validation->fails()){
						return Redirect::back()->withErrors($validation)->withInput();
					}else{
						// return Input::all();
						$step3 = ScholarshipWork::create(array(
							 'work_studentID' => $id,
							 'work_NameOfemployer' => Input::get('name'),	
							 'work_presentPosition' => Input::get('position'),
							 'work_lenghtOfservice' => Input::get('lenght')
						));
								Session::forget('step');
								Session::put('step',4);
								Session::put('id',$id);
								$name = Session::get('name');
								Session::put('name',$name);
								$up = Scholarship::where('id', '=',$id)->update(array('step' => '4'));

								if ($up) {
									return Redirect::to('apply/step4');
								}
					}
					break;

				case '4':
					$id = Session::get('id');
					$name = Session::get('name');

				$path = '';
					$public = public_path();
					$name = $name.'_'.$decrypted.'_'.$id;
					$path .= $public.'/media/scholarship/documents/'.$name;
					//
					$rules = [
						'birth'=> 'max:1000|mimes:jpeg,png',
						'Photo'=> 'max:1000|mimes:jpeg,png',	
						'cert1'=> 'max:1000|mimes:jpeg,pdf,doc',	
						'cert2'=> 'max:1000|mimes:jpeg,pdf,doc',	
						'cert3'=> 'max:1000|mimes:jpeg,pdf,doc',	
						'cert4'=> 'max:1000|mimes:jpeg,pdf,doc'	
					];

						$validation = Validator::make(Input::file(),$rules);
						if ($validation->fails()) {
							return Redirect::back()->withErrors($validation)->withInput();
						}else{

							if (File::exists($path)) {
								//files
									if (Input::file('birth')) {
											$birth = Input::file('birth')->getClientOriginalName();
											$rename = 'birthcertificate_'.str_random(28).'_'.$birth;
											$uploadbirth = Input::file('birth')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $birth,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'birthcertificate'
												));
										}
									if (Input::file('Photo')) {
										$Photo = Input::file('Photo')->getClientOriginalName();
										$rename = 'photo_'.str_random(28).'_'.$Photo;
										$uploadPhoto = Input::file('Photo')->move($path,$rename);

											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $Photo,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'photo'
												));
									}
									
									if (Input::file('cert2')) {
										$cert2 = Input::file('cert2')->getClientOriginalName();
										$rename ='certificate_'.str_random(28).'_'.$cert2;
										$uploadcert2 = Input::file('cert2')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert2,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'cert'
												));
									}
									if (Input::file('cert1')) {
										$cert1 = Input::file('cert1')->getClientOriginalName();
										$rename ='certificate_'.str_random(28).'_'.$cert1;
										$uploadcert1 = Input::file('cert1')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert1,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
											'rename'=> $rename,
											'type'=>'certificate'
												));

									}

									if (Input::file('cert3')) {
										$cert3 = Input::file('cert3')->getClientOriginalName();
										$rename = 'certificate_'.str_random(28).'_'.$cert3;
										$uploadcert3 = Input::file('cert3')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert3,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>'certificate_'.str_random(28).'_'.$cert3,
												'type'=>'certificate'
												));
									}


									if (Input::file('cert4')) {
										$cert4 = Input::file('cert4')->getClientOriginalName();
										$rename = 'certificate_'.str_random(28).'_'.$cert4;
										$uploadcert4 = Input::file('cert4')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert4,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'certificate'
												));
									}

								}else{
								// make directory 
									mkdir($path);

								//files
									if (Input::file('birth')) {
											$birth = Input::file('birth')->getClientOriginalName();
											$rename = 'birthcertificate_'.str_random(28).'_'.$birth;
											$uploadbirth = Input::file('birth')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $birth,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'birthcertificate'
												));
										}
									if (Input::file('Photo')) {
										$Photo = Input::file('Photo')->getClientOriginalName();
										$rename = 'photo_'.str_random(28).'_'.$Photo;
										$uploadPhoto = Input::file('Photo')->move($path,$rename);

											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $Photo,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'photo'
												));
									}
									
									if (Input::file('cert2')) {
										$cert2 = Input::file('cert2')->getClientOriginalName();
										$rename ='certificate_'.str_random(28).'_'.$cert2;
										$uploadcert2 = Input::file('cert2')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert2,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'cert'
												));
									}
									if (Input::file('cert1')) {
										$cert1 = Input::file('cert1')->getClientOriginalName();
										$rename ='certificate_'.str_random(28).'_'.$cert1;
										$uploadcert1 = Input::file('cert1')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert1,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
											'rename'=> $rename,
											'type'=>'certificate'
												));

									}

									if (Input::file('cert3')) {
										$cert3 = Input::file('cert3')->getClientOriginalName();
										$rename = 'certificate_'.str_random(28).'_'.$cert3;
										$uploadcert3 = Input::file('cert3')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert3,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'certificate'
												));
									}


									if (Input::file('cert4')) {
										$cert4 = Input::file('cert4')->getClientOriginalName();
										$rename = 'certificate_'.str_random(28).'_'.$cert4;
										$uploadcert4 = Input::file('cert4')->move($path,$rename);
											ScholarshipDocument::create(array(
												'foldername' => $name,
												'directory' => $path,
												'filename' => $cert4,
												'studentID' => $id,
												'confirmed' => 0,
												'deleted' => 0,
												'rename'=>$rename,
												'type'=>'certificate'
												));
									}
								
								}
	

								Session::forget('step');
								Session::put('step',4);
								Session::put('id',$id);
								$name = Session::get('name');
								Session::put('name',$name);
								$up = Scholarship::where('id', '=',$id)->update(array('step' => '5'));
								if ($up) {
									return Redirect::to('apply/step5');
								}

						}

					break;

				case '5':
				$id = Session::get('id');
				$rules = [
					'award' => 'required',
					'fieldofstudy' => 'required',
					'level' => 'required',
				];
				$validation = Validator::make(Input::all(),$rules);
				if ($validation->fails()) {
					return Redirect::back()->withErrors($validation)->withInput();
				}else{
						ScholarshipApplication::create(array(
								'scholarshipsID'=> 0,
								'studentID'=> $id,
								'scholarshipID'=> $decrypted,
								'award'=> Input::get('award'),
								'filedofstudy'=>Input::get('fieldofstudy'),
								'leveofstudy'=>Input::get('fieldofstudy'),
								'status'=>3

							));
								Session::forget('step');
								Session::put('step',5);
								Session::put('id',$id);
								$name = Session::get('name');
								Session::put('name',$name);
								Session::put('code',$decrypted);
								$up = Scholarship::where('id', '=',$id)->update(array('step' => '6'));

								if ($up) {
									return Redirect::to('apply/step6');
								}
					}
					break;
				case '6':
								$id = Session::get('id');
								$up = Scholarship::where('id', '=',$id)->update(array('step' => '0','status'=>1));
								if ($up) {
									// go to all tables and change there status to complete ?:
								ScholarshipApplication::where('studentID', '=',$id)->update(array('status' => '1'));
								Session::forget('ecode');
								Session::forget('name');
								Session::forget('id');
								Session::forget('step');
									return Redirect::to('apply');
								}
					break;

				default:
					# code...
					break;
			}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{

		// controlling what is display and what steps are displaced
		$id = Request::segment(2);
		if (Session::has('ecode')) {
			$ecode = Session::get('ecode');
			
			$decrypted = Crypt::decrypt($ecode);
			$resp = Scholarshipcode::whereStudenidAndDeleted($decrypted,0);	
			if($resp){
				$up = Scholarship::where('scholarshipID', '=',$decrypted)->first();
				if ($up) {
					if($id == 'step0') return Redirect::to('apply') ;
					if ($id !== 'step'.$up->step) {
						Session::put('id',$up->id);
						Session::put('name',$up->fullname);
						return Redirect::to('apply/step'.$up->step);					
					}

				}

			}else{
				Session::forget('ecode');
				return Redirect::to('code')->withErrors('sorry your code is disabled');
			}
			$this->layout->content = View::make('scholarship_application.form')->with('data',$this->data);
		}else{
			return Redirect::to('apply');
		}


		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return '$id';
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return $id;
	}
	public function postUp()
	{
		return 'postting';
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return $id;
	}

}