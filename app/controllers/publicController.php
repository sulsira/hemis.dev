<?php 

class publicController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $layout = 'public';
	public function index()
	{
		      $domain = Session::get('userDomain');
		      $seg1 =Request::segment(1);
		      $seg2 =Request::segment(2);
		      $seg3 =Request::segment(3);
		if (!Auth::guest()) return Redirect::intended($domain);;
		$this->layout->content = View::make('home.index');
	}
	public function postLogin()
	{
		$creditials = array(
				'email' => Input::get('email'),
				'password' => Input::get('password')
		);
	 	// see if they are in the database

		if ( Auth::attempt($creditials) ) {
			if ( Auth::check() ) {
				// dd(Auth::user()->id);
				$status = User::whereId(Auth::user()->id)->update(array('status'=>1)) ;
				// 	// turn on status to active

				// create a log file and show that time, pc , ip 

				//  redirect to  appropriate dashboard
				if ( $status ) {
				 	// put userdata in session
					Session::put('userName',Auth::user()->username);
					Session::put('userType',Auth::user()->userType);
					Session::put('userId',Auth::user()->id);
					Session::put('userDomain',Auth::user()->domain);
					Session::put('userGroup',Auth::user()->userGroup);
					Session::put('link',Auth::user()->link);
					Session::put('privileges',Auth::user()->privileges);


					switch ($status) {
						case Auth::user()->userGroup == 'researcher' :
							Session::put('lickId',Auth::user()->lickId);						
							Session::put('fullname',Auth::user()->fullname);						
							return Redirect::intended('research/researcher')->with('userdata',Auth::user());
							break;
						case Auth::user()->userGroup == 'school' :

							Session::put('lickId',Auth::user()->lickId);	
							Session::put('fullname',Auth::user()->fullname);											
							return Redirect::intended('school')->with('userdata',Auth::user());
							break;
						case Auth::user()->userGroup == 'scholarship' :
							return Redirect::intended('scholarship')->with('userdata',Auth::user());
							break;
						case Auth::user()->userGroup == 'hemis' :
							// dd(Auth::user()->userType);
							break;				
						default:
							// check if the user have a url in the user table if yes redirect to that 
							return Redirect::intended(Auth::user()->domain)->with('userdata',Auth::user());
							break;
					}
					
				}
			}
		}
		else{ #credentials wrong for the user

			// else if not logged in redirect back with errors
			return Redirect::to('login')->with('flash_error','access denied')->withInput();

		}
		
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getApply()
	{
		$this->layout->content = View::make('home.scholarshipform');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function getHelp()
	{
		$this->layout->content = View::make('home.help');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getSchools()
	{
		// get total number of schools
		// get ownership grouping
		// get region grouping
		// get mostly classification
		$data = array();
		$lc = array();
		$stats = array();
		$inst = array();
		$schools = School::all()->toArray();
		foreach ($schools as $key => $value) {
			$inst[$value['LeCe_Classification']][] = $value;
			$lc[$value['LeCe_Classification']][$value['LeCe_Ownership']][] = $value;
		}
		foreach ($lc as $key => $value) {
			foreach ($value as $key1 => $value1) {
				$stats[$key][$key1] = count($value1);
				
			}

			
		}
		$data['lc'] = $lc;
		$data['stats'] = $stats;

		$this->layout->content = View::make('home.schools')->with('data',$data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}