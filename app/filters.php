<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});


/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');

});

Route::filter('schoolAuth', function()
{
	$data['id'] = Session::get('lickId');
	$validation = Validator::make($data,['id'=>'integer']);
	if ($validation->fails()) {
		return Redirect::intended('logout');
	}else{
		if(!$data['id']){
			return Redirect::intended('logout');
		}
	}

});

Route::filter('radmin', function()
{
	// check if the person is loged in
	// check if the person is researcher
	// 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');

	if ( Auth::check() ) {
      $domain = Session::get('userDomain');
      $seg1 =Request::segment(1);
      $seg2 =Request::segment(2);
      $seg3 =Request::segment(3);
        if ( ($seg1.DS.$seg2) != ($domain.DS.$userGroup) ) {

          if ($seg2 != $domain)
            {
              return Redirect::intended($domain);
          }
        }
			 
		}
if (Auth::guest()) return Redirect::guest('login');

});

Route::filter('school', function()
{
	// check if the person is loged in
	// check if the person is researcher
	// 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');

	if ( Auth::check() ) {
      $domain = Session::get('userDomain');
      $seg1 =Request::segment(1);
      $seg2 =Request::segment(2);
      $seg3 =Request::segment(3);
        if ( ($seg1) != ($userGroup) ) {

          if ($seg1 != $domain)
            {
              return Redirect::intended($domain);
          	}
        }
			 
		}
if (Auth::guest()) return Redirect::guest('login');

});

Route::filter('place', function()
{

	if ( Auth::check() ) {
			$domain = Session::get('userDomain');
			$seg1 =Request::segment(1);

			if ($seg1 !== $domain)
				{
				  return Redirect::intended($domain);
				}
			 
		}
if (Auth::guest()) return Redirect::guest('login');

});

Route::filter('admin',function(){
		if ( Auth::check() ) {
				$domain = Session::get('userDomain');
				if ($domain != 'admin' || $domain != 'scholarship') {
					// return Redirect::to($domain);
				}
			
		}else{
			return Redirect::to('login');
		}
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});