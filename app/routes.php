<?php
Route::resource('/','publicController');
Route::get('login',['as'=>'login', 'uses'=>'publicController@index']);
Route::post('login','publicController@postLogin');

	
	Route::get('search',['as'=>'search', 'uses'=>'SearchController@index']);
	Route::get('help',['as'=>'help', 'uses'=>'publicController@getHelp']);
	Route::get('schools',array('as'=>'schools','uses'=>'publicController@getSchools'));
	Route::get('scholarship','ScholarshipsController@getScholarships');
	Route::get('school/download',function(){
		$file = $_GET['file'];
		return Response::download($file);
	});
	Route::resource('apply', 'ScholarapplyController');
	Route::get('logout',function(){ #LOGING OUT
		if ( Auth::check() ) {
				$status = User::whereId(Auth::user()->id)->update(array('status'=>0));
				Auth::logout();
				Session::flush();
				return Redirect::to('/');
			}else{
				return Redirect::to('login');
		}
	});

// RESEARCH ROUTES
Route::group(array('prefix'=>'research'),function(){

	Route::resource('researchers','ResearchersController');
	Route::resource('/','ResearchersController');
	Route::resource('publications','publicationsController');
	Route::resource('register','registerController');
	Route::resource('upload','uploadController');

	Route::group(array('before'=>'radmin'), function(){
			Route::resource('researcher','researcherController');
	});
	Route::get('download',function(){
		$file = $_GET['file'];
		return Response::download($file);
	});
	Route::get('view', function()
	{
		$file = $_GET['file'];
		return Redirect::to($file);
	});

});// end of research

# LOGING REQUIRED ZONE

	// ADMIN ZONE (REQUIRE USER LOGING AND ACCESS PRIVILGES)
	Route::group(array('before' => 'auth','before'=>'place'), function()
	{
		Route::resource('scholarship/applicant', 'applicantController');
		Route::resource('scholarship/codes', 'ScholarshipcodesController');
		Route::resource('admin/tasks', 'TasksController');
		Route::resource('admin/user', 'UsersController');
	 	Route::resource('admin/programme','ProgrammeController');
	 	Route::resource('admin/institution','InstituteController');
	 	Route::resource('admin/institution.students','studentsController');
	 	Route::resource('admin/institution.staffs','staffsController');
	 	Route::resource('admin/institution.classes','ClassesController');
	 	Route::resource('admin/institution.courses','coursesController');
	 	Route::resource('admin/institution.contacts','contactsController');
	 	Route::resource('admin/institution.statistics','StatisticsController');
		Route::controller('admin','AdminController');
		Route::controller('scholarship','ScholarshipsController'); 	
	}); #END OF ADMING

	Route::group(array('prefix'=>'admin','before' => 'auth','before'=>'place'),function(){	

		Route::controller('/admin','AdminController');
		Route::get('/',array('as'=>'admin','uses'=>'AdminController@getIndex'));

		Route::get('/programme',array('as'=>'programme','uses'=>'ProgrammeController@index'));
		Route::get('/files',array('as'=>'files','uses'=>'ProgrammeController@index'));

		Route::get('/institution/{id?}',array('as'=>'institution','uses'=>'InstituteController@index'));

		Route::get('/institution/{id?}/students',array('as'=>'students','uses'=>'studentsController'));

		Route::get('/institution/{id?}/staffs',array('as'=>'staffs','uses'=>'staffsController'));

		Route::get('/institution/{id?}/courses',array('as'=>'courses','uses'=>'CoursesController'));

		Route::get('/institution/{id?}/contacts',array('as'=>'contacts','uses'=>'ContactsController'));

		Route::get('/institution/{id?}/classes',array('as'=>'classes','uses'=>'ClassesController'));

		Route::get('/institution/{id?}/statistics',array('as'=>'statistics','uses'=>'StatisticsController'));	
	});	
	Route::group(array('prefix'=>'scholarship','before' => 'auth','before'=>'place'),function(){	
		Route::get('/',array('as'=>'scholarship','uses'=>'ScholarshipsController@getIndex'));	
	});	
	Route::group(array('before'=>'admin'), function(){

		Route::resource('code', 'ScholarshipcodesController');

	});

	Route::group(array('prefix'=>'school','before'=>'school'),function(){

		Route::resource('/','schoolController');
			Route::resource('students','SchstudentsController');
			Route::resource('staffs','schstaffsController');
			Route::resource('courses','schcoursesController');
			Route::resource('templates','SchtemplatesController');
	});

	Route::group(array('prefix'=>'scholarship'),function(){
		#CODE HERE
	});





// ROUTES THAT NEED AUTHENTICATION BELOW




#ERROR AND DEBUGING ZONE
App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});
// Event::listen('illuminate.query',function($query){
// 	// dd($query);
// 	echo '<pre style="margin:200px 80px">';
// 	print_r($query);
// 	echo '</pre>';
// });
// Route::get('/company-profile', function() {
//     return View::make('companyprofile');
// });


//api processing
//
Route::group(array('prefix' => 'api/v1'), function()
{

Route::resource('trainees', 'TraineController');

    // Route::get('post', function()
    // {
    //    var_dump("lets test");
    // });
    // Route::resource('dashboard','DashboardController');

});