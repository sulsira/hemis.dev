@include('_partials/doc')
	@section('style')
	{{ HTML::style('assets/dist/css/bootstrap.min.css') }}		
	{{ HTML::style('css/.css') }}		 
	{{ HTML::style('css/style.css') }}	 
	@show
       
    </head>
    <body>
	    <div class="top">
	    	@include('_partials/menu')  		
	  		@yield('scope')		
	    </div>
	    <div class="wrapper">
		    <div class="row content">
		    	@yield('container')
		    </div>	    	
	    </div>

	    <footer>
	    	<div class="hedacont">MOHERST &copy; Copyright 2013</div>
	    	@yield('footer')
	    </footer>
	    	@yield('script')
@include('_partials/footer')