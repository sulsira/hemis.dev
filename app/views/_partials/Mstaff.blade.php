<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	
<!-- 			<div class="btn-group modal_options">
			  <a class="btn btn-info  dropdown-toggle" data-toggle="dropdown" href="#">
				options
				<span class="caret"></span>
			  </a>
			  <ul class="dropdown-menu">
				<li><a href="" id="profile">view profile</a></li>
				<li><a href="" id="delete">delete student</a></li>
			  </ul>
			</div>	 -->
	    	<h3 id="myModalLabel">( Staff )</h3>				
	</div>

  <div class="modal-body">
		{{Form::open(['route'=>['school.staffs.update','alter'],'method'=>'PUT'])}}
			<div id="nameForm">
					<fieldset>
						<legend>
							<strong>
								Fullname
							</strong>														
						</legend>
						<div class="input-feild">
							<input type="text" name="FirstName" id="firstName" placeholder="First Name" required="">
							<input type="text" name="MiddleName" id="middleName" placeholder="Middle Name" class="span2">
							<input type="text" name="LastName" id="lastName" placeholder="Last Name" required="">
						</div>
					</fieldset>
			</div>
			<div id="detailsForm">
					<fieldset>
						<legend>
							<strong>
								details
							</strong>	
						</legend>
						<div class="input-heading">
							<span class="bday">birthday</span>
							<span class="enthnic">ethniticity</span>
							<span class="nation">nationality</span>
						</div>
						<div class="input-feild">
							<input type="text" name="bday" id="bday" placeholder="day" class="span1">
							<input type="text" name="bmonth" id="bmonth" placeholder="month" class="span1">
							<input type="text" name="byear" id="byear" placeholder="year" class="span1">
							<select name="enit" id="enit">
								<?php $countries = Variable::domain('Pers_Ethnicity')->toArray();  ?>
								@foreach ($countries as $key => $country)
								<option>{{$country['Vari_VariableName']}}</option>
								@endforeach
							</select>
							<select name="nation" id="nation">
								<?php $countries = Variable::domain('Country')->toArray();  ?>
								@foreach ($countries as $key => $country)
								<option>{{$country['Vari_VariableName']}}</option>
								@endforeach
							</select>

						</div>
						<div class="input-heading">
							<span class="gender">gender</span>
						</div>
						<div class="input-feild">
							<select name="gender" id="gender">
						 		<option>male</option>
						 		<option>female</option>
						 	</select>
						</div>
					</fieldset>
			</div>	
		<!--	
			<div id="address">
					<fieldset>
						<legend>
							<strong>
								address
						</strong>
					</legend>
						<div class="input-heading">
							<span class="town">town</span>
							<span class="district">district</span>
							<span class="box">p.o.box</span>
						</div>
						<div class="input-feild">
							<input type="text" name="addr0town0string" placeholder="town" id="town">
							<select name="addr0district0string" id="district">
								
							</select>
							<input type="text" name="addr0pobox0string" placeholder="P.O box" id="pobox">
						</div>
						<div class="input-heading">
							<span class="street">street</span>
							<span class="region">region</span>
						</div>
						<div class="input-feild">
							<input type="text" name="addr0street0string" placeholder="street" id="street">
							<select name="addr0region0string" id="region">
								<option>lower river region</option>
								<option>central river region</option>
								<option>northbank region</option>
							</select>	
						</div>
						<input type="hidden" data-schoolId="" id="lece_id">
						<input type="hidden" value="" id="persId" name="person0persId0number">
						<input type="hidden" value="" id="add_id" name="addr0Id0number">
					</fieldset><input type="hidden" value="" id="contactID" name="contactID">
			</div> 
			-->
						<input type="hidden" data-schoolId="" id="lece_id">
						<input type="hidden" value="" id="persId" name="persId">
						<input type="hidden" value="" id="add_id" name="add_id">
						
  </div>
  <div class="modal-footer">
		<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" name="edit_student">Save changes</button>
		</div><!-- end of modal footer -->	
	{{Form::close()}}	
  </div>
</div>