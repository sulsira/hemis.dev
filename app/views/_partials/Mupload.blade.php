<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h3 id="myModalLabel">Upload your filled document to MoHERST</h3>				
	</div>
	{{Form::open(['route'=>['school.templates.update',$school['LeCe_LearningCenterID']],'method'=>'PUT','files' => true])}}
	  <div class="modal-body">
		{{Form::file('fileUpload', array('class' => 'input-large','placeholder'=>".input-large"))}}			
	  </div>
	  <div class="modal-footer">
			<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button class="btn btn-primary" name="upload">Upload File</button>
			</div><!-- end of modal footer -->	
	  </div>
	{{Form::close()}}	

</div>