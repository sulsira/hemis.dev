
<hr>
<h3>Add new school (s)</h3>
<hr>
 @include('_partials.errors')  
 @if(Session::has('success')) 
  <h3 class="text-success">
    You have successfully added: {{Session::get('success')}}
  </h3>
 @endif
<hr>
{{Form::open(array('route'=>'admin.institution.store', 'method'=>'post'))}}
	<div class="content-area">
		<div class="school-details">
			<strong>School details</strong>
			<hr> 
			<div class="input-heading">
				<span class="school-name">school name</span>
				<span class="entrollment">entrollment capacity</span>
				<span class="ownership">ownership</span>
				<span class="lcenter-type">Learning center type</span>
				<span class="lcenter-type">Learning center classification</span>
			</div>
			<div class="input-feild">
				{{Form::text('school[name]',null,array('placeholder'=>"add school name",'required'=>1))}}
				<input	type="number" name="school[enrol-capacity]" placeholder="entrollment capacity"/>
				{{Form::hidden('school[lece_id]',null)}}
				<select name="school[ownership]">
									<?php $countries = Variable::domain('LeCe_Ownership')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
				</select>
				<select name="school[type]">
					<?php $countries = Variable::domain('LeCe_Classification')->toArray();  ?>
					@foreach ($countries as $key => $country)
					<option>{{$country['Vari_VariableName']}}</option>
					@endforeach
				</select>
				<select name="school[classification]">
					<option>lower tertiary</option>
					<option>higher tertiary</option>
					<option>higher education</option>
				</select>
			</div>
		</div>
		<div class="error-controlling control-group error" style="display:none">
				<span class="school-name help-inline">school name</span>
				<span  class="entrollment">entrollment capacity</span>
				<span  class="ownership">ownership</span>
				<span   class="lcenter-type">Learning center type</span>
		</div>
		<hr>
		<div class="school-contacts">
			<strong>School contact information</strong>
			

			<div class="input-heading">
				<span class="cont-email">email</span>
				<span class="cont-tele">telephone</span>
				<span class="cont-mobile">mobile</span>
				<span class="cont-website">website</span>
			</div>
			<div class="input-feild">
				<input type="email" name="cont[email]" placeholder="add email">
				<input type="number" name="cont[tele]" placeholder="add telephone" value="">
				<input type="number" name="cont[mobile]" placeholder="add mobile" value="">
				<input type="text" name="cont[website]" placeholder="add website" value="">
			</div>
			<div class="hiddens">
				<input type="hidden" name="address[add_id]"  value="">

				<input type="hidden" name="cont[coct_id]"  value="">

				<input type="hidden" name="cont[coct_id_tele]"  value="">

				<input type="hidden" name="cont[mobile]" placeholder="add mobile" value="">

				<input type="hidden" name="cont[coct_id_website]"  value="">
			</div>
		</div>
		<hr>
		<div class="school-contacts">
			<strong>School address</strong>
			<hr>
			<div class="input-heading">
				<span class="addr-street">street</span>
				<span class="addr-town" >town</span>
				<span class="addr-town" >p.o.box</span>
				<span class="addr-region">region</span>
			</div>
			<div class="input-feild">
				<input type="text" name="address[street]" placeholder="add street"  value="">
				<input type="text" name="address[town]" placeholder="add town" value="">
				<input type="text" name="address[pobox]" placeholder="add p.o.box" value="">
				<select name="address[region]" data-input="" id="">
					<option></option>
					<?php $countries = Variable::domain('Addr_District')->toArray();  ?>
					@foreach ($countries as $key => $country)
					<option>{{$country['Vari_VariableName']}}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	<hr>
	<div class="action-area">
	  <button type="submit" class="btn btn-primary" name="SaveSchool" value="save">Save</button>
	  <button type="submit" class="btn btn btn-success" name="SaveSchool" value="done">Done</button>
	</div>
{{Form::close()}}
<!--
			</div>
			<hr>
			<div class="row-fluid">
				<form method="post">
					<div class="content-area">
						<table class="table table-bordered">
						  <caption>
						  	<span>information entered</span> |
						  	<a href="add_school.php?section=history#history" id="history">history</a>
						  </caption>
						  <thead>
						    <tr>
						      <th>#</th>
						      <th>school name</th>
						      <th>actions</th>
						    </tr>
						  </thead>
						  <tbody class="entered-data-school">					  	
									<tr><td colspan="3" class="maked">today</td></tr>

											<tr>
										      	<td  class="maked"></td>
										      	<td><a href=""></a></td>
										      	<td class="action-td">
											      	<a href="" title="edit school entry"><img src="../../__public/imgs/edit-entry.png"></a>
											      	<a href="" title="remove entry"><img src="../../__public/imgs/delete-entry.png"></a>
										     	</td>
									    	</tr>
									<tr><td colspan="3" class="maked">something</td></tr>
											<tr>
										      	<td  class="maked"></td>
										      	<td><a href=""></a></td>
										      	<td class="action-td">
											      	<a href="" title="edit school entry"><img src="../../__public/imgs/edit-entry.png"></a>
											      	<a href="" title="remove entry"><img src="../../__public/imgs/delete-entry.png"></a>
										     	</td>
									    	</tr>		  	
						  </tbody>
						</table>
					</div>
					<div class="action-area">
						<button class="btn-large btn-primary" name="">finish all</button>
					</div>
			</form>
-->