              <div class="panel-info" id="tab4">
                <!-- Default panel contents -->
                <hr>
                <div class="panel-heading">Information4</div>
                <hr>
                <div> 
                  <?php $name = Session::get('name'); ?>
                   <em>INSTRUCTIONS : </em> <strong>{{$name }} ,</strong> consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
                </div>
                <hr>
                @if($errors->any())
                  <hr>
                  <ul class="list-group">
                    {{implode('',$errors->all('<li class="list-group-item list-group-item-danger">:message</li>'))}}
                  </ul>
                @endif
                <div class="panel-body">

                    {{Form::open(['route'=>'apply.store', 'files' => true])}}
                              <fieldset>
                                <legend>Documents</legend>
                                <p>
                                  Note: All relevant documnets (including birth certificates) must be attached
                                </p>
                                <div class="form-group">
                                  <label for="birth">Birth Certificate</label>
                                  <input type="file" id="birth" name="birth">
                                  <p class="help-block">Please upload your birth certificate</p>
                                </div>
                                <div class="form-group">
                                  <label for="cert1">Photo</label>
                                  <input type="file" id="Photo" name="Photo">
                                  <p class="help-block">Please upload your picture(passport size)</p>
                                </div>
                                <div class="form-group">
                                  <label for="cert1">Certificate 1</label>
                                  <input type="file" id="cert1" name="cert1">

                                </div>
                                <div class="form-group">
                                  <label for="cert1">Certificate 2</label>
                                  <input type="file" id="cert1" name="cert2">
                                </div>
                                <div class="form-group">
                                  <label for="cert1">Certificate 3</label>
                                  <input type="file" id="cert1" name="cert3">
                                </div>
                                <div class="form-group">
                                  <label for="cert1">Certificate 4</label>
                                  <input type="file" id="cert1" name="cert4">
                                </div>
                                 <div class="checkbox">
                                <label>
                                  <input type="checkbox"> I have acknowledge that tyhe info is correct
                                </label>
                              </div>
                              </fieldset>
                            <fieldset>
                             <button type="reset" class="btn btn-lg btn-default">Cancel</button>
                              <button type="submit" class="btn btn-lg btn-primary">Next</button>
                             <input  type="hidden" name="here" value="4"/>
                             <input  type="hidden" name="next" value="5"/>
                            </fieldset>
                  {{Form::close()}}

                </div>

                <!-- Table -->
                <table class="table">
                  ...
                </table>
              </div>