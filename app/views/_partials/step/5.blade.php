              <div class="panel-info" id="tab5">
                <!-- Default panel contents -->
                <hr>
                <div class="panel-heading">Information5</div>
                <hr>
                <div> 
                  <?php $name = Session::get('name'); ?>
                   <em>INSTRUCTIONS : </em> <strong>{{$name }} ,</strong> consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
                </div>
                <hr>
                 @if($errors->any())
                  <hr>
                  <ul class="list-group">
                    {{implode('',$errors->all('<li class="list-group-item list-group-item-danger">:message</li>'))}}
                  </ul>
                @endif
                <div class="panel-body">
                     {{Form::open(['route'=>'apply.store'])}}
                             <fieldset>
                                <legend>Scholarship Details</legend>
                                <div class="form-group">
                                  <label for="award">Name of Award</label>
                                  <input type="text" class="form-control" id="award" placeholder="Enter Award" name="award" required>
                                </div>
                                <div class="form-group">
                                  <label for="study">Field of Study</label>
                                  <input type="text" class="form-control" id="study" placeholder="Enter Field of Study" name="fieldofstudy" required>
                                </div>
                                 <div class="form-group">
                                  <label for="level">Level of Study</label>
                                  <select class="form-control" name="level">
                                    <option>-- Level of study --</option>
                                    <option>Dip</option>
                                    <option>Bachelors</option>
                                    <option>Mastera</option>
                                    <option>PhD</option>
                                  </select>
                                </div>
                              </fieldset>
                    <fieldset>
                     <button type="reset" class="btn btn-lg btn-default">Cancel</button>
                      <button type="submit" class="btn btn-lg btn-primary">Next</button>
                     <input  type="hidden" name="here" value="5"/>
                     <input  type="hidden" name="next" value="6"/>
                    </fieldset>
                  {{Form::close()}}
                </div>

                <!-- Table -->
                <table class="table">
                  ...
                </table>
              </div>