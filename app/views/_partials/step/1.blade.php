              <div class="panel-info" id="tab1">
                <!-- Default panel contents -->
                <hr>
                <div class="panel-heading">

                <strong> Personal Information </strong>

                </div>
                <hr>
                <div> 
                   <em>INSTRUCTIONS : </em>dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
                </div>
                <hr>
                @if($errors->any())
                  <hr>
                  <ul class="list-group">
                    {{implode('',$errors->all('<li class="list-group-item list-group-item-danger">:message</li>'))}}
                  </ul>
                @endif
                <div class="panel-body">
                     {{Form::open(['route'=>'apply.store'])}}
                     

                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" required>
                        <p class="help-block">Please the name that is on your documents</p>
                      </div> 

                      <div class="form-group">
                        <label for="othername">Other name</label>
                        <input type="text" class="form-control" id="othername" placeholder="Enter Other name" name="othername" >
                      </div> 

                       <div class="form-group">
                        <label for="dob">Date of Birth</label>
                        <input type="date" class="form-control" id="dob" placeholder="Enter Other name" name="dob" >
                      </div> 

                      <div class="form-group">
                        <label for="dob">Gender</label>
                          <select name="gender"  class="form-control" required>
                            <option>male</option>
                            <option>female</option>
                          </select> 
                        </div>
                      </div> 

                      <div class="form-group">
                        <label for="nationality">Nationality</label>
                        <input type="text" class="form-control" name="nationality" placeholder="Enter Nationality" >
                      </div>

                      <div class="form-group">
                        <label for="tel">Telephone/Mobile</label>
                        <input type="telephone" class="form-control" id="tel" placeholder="Enter Telephoe" name="telephone" required>
                      </div>

                      <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
                      </div>
                      
                      <div class="form-group">
                        <label for="address">address</label>
                        <textarea class="form-control" rows="3" name="address" value=""></textarea>
                        <p class="help-block">For Correspondence</p>
                      </div>
                    <fieldset>
                     <button type="reset" class="btn btn-lg btn-default">Cancel</button>
                      <button type="submit" class="btn btn-lg btn-primary">Next</button>
                     <input  type="hidden" name="here" value="1"/>
                     <input  type="hidden" name="next" value="2"/>
                    </fieldset>
                  {{Form::close()}}
                </div>

              </div>