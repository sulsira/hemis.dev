              <div class="panel-info" id="tab3">
                <!-- Default panel contents -->
                <hr>
                <div class="panel-heading">Information3</div>
                <hr>
                <div> 
                  <?php $name = Session::get('name'); ?>
                   <em>INSTRUCTIONS : </em> <strong>{{$name }} ,</strong> consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
                </div>
                <hr>
                @if($errors->any())
                  <hr>
                  <ul class="list-group">
                    {{implode('',$errors->all('<li class="list-group-item list-group-item-danger">:message</li>'))}}
                  </ul>
                @endif
                <div class="panel-body">
{{Form::open(['route'=>'apply.store'])}}

                          <fieldset>
                          <legend>Work Experience</legend>
                          <div class="form-group">
                            <label for="emp">Name of Employer</label>
                            <input type="text" class="form-control" id="emp" placeholder="Enter Employer name" name="name">
                          </div>
                           <div class="form-group">
                            <label for="len">Length of Services</label>
                            <input type="text" class="form-control" id="emp" placeholder="Enter Length of Services" name="lenght">
                          </div>
                           
                           <div class="form-group">
                            <label for="pre-pos">Present Position</label>
                            <input type="text" class="form-control" id="emp" placeholder="Enter Present Position" name="position">
                          </div>
                        </fieldset>
                                            <fieldset>
                     <button type="reset" class="btn btn-lg btn-default">Cancel</button>
                      <button type="submit" class="btn btn-lg btn-primary">Next</button>
                     <input  type="hidden" name="here" value="3"/>
                     <input  type="hidden" name="next" value="4"/>
                    </fieldset>
{{Form::close()}}

                </div>

                <!-- Table -->
                <table class="table">
                  ...
                </table>
              </div>