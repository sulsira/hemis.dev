<div class="panel-info" id="tab2">
  <!-- Default panel contents -->
  <hr>
  <div class="panel-heading">Educational Background</div>
                  <hr>
                <div> 
                  <?php $name = Session::get('name'); ?>
                   <em>INSTRUCTIONS : </em> <strong>{{$name }} ,</strong> consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
                </div>
                   <hr>
                @if($errors->any())
                  <hr>
                  <ul class="list-group">
                    {{implode('',$errors->all('<li class="list-group-item list-group-item-danger">:message</li>'))}}
                  </ul>
                @endif
  <div class="panel-body">
    {{Form::open(['route'=>'apply.store', 'files' => true])}}
      
          <legend>Educational Background</legend>
          <hr>
          <fieldset>
          <div class="form-group">
          <h4>School/Institute 1</h4>
            <label for="sch1">Name</label>
            <input type="text" class="form-control" id="sch1" placeholder="Enter School/Institute name" name="sch1">
            <label for="qua1">Qualification/Certificate</label>
            <input type="text" class="form-control" id="qua1" placeholder="Enter Qualification" name="qua1">
 
          </div>            
          </fieldset>
          <hr>
          <hr>
          <fieldset>
          <div class="form-group">
          <h4>School/Institute 2</h4>
            <label for="sch2">Name</label>
            <input type="text" class="form-control" id="sch2" placeholder="Enter School/Institute name" name="sch2">
            <label for="qua2">Qualification/Certificate</label>
            <input type="text" class="form-control" id="qua2" placeholder="Enter Qualification" name="qua2"> 
          </div>            
          </fieldset>
          <hr>
          <hr>
          <fieldset>
          <div class="form-group">
          <h4>School/Institute 3</h4>
            <label for="sch3">Name</label>
            <input type="text" class="form-control" id="sch3" placeholder="Enter School/Institute name" name="sch3">
            <label for="qua1">Qualification/Certificate</label>
            <input type="text" class="form-control" id="qua3" placeholder="Enter Qualification" name="qua3">
          </div>            
          </fieldset>
          <hr>
          <hr>
<!--           

          <div class="form-group">
            <label for="sch1">School/Institute 2</label>
            <input type="text" class="form-control" id="sch1" placeholder="Enter School/Institute name">
            <label for="qua1">Qualification/Certificate</label>
            <input type="text" class="form-control" id="qua1" placeholder="Enter Qualification">
          </div>

          <div class="form-group">
            <label for="sch1">School/Institute 3</label>
            <input type="text" class="form-control" id="sch1" placeholder="Enter School/Institute name">
            <label for="qua1">Qualification/Certificate</label>
            <input type="text" class="form-control" id="qua1" placeholder="Enter Qualification">
          </div>

          <div class="form-group">
            <label for="sch1">School/Institute 4</label>
            <input type="text" class="form-control" id="sch1" placeholder="Enter School/Institute name">
            <label for="qua1">Qualification/Certificate</label>
            <input type="text" class="form-control" id="qua1" placeholder="Enter Qualification">
          </div>
        </fieldset>-->
        <fieldset>
         <button type="reset" class="btn btn-lg btn-default">Cancel</button>
          <button type="submit" class="btn btn-lg btn-primary">Next</button>
         <input  type="hidden" name="here" value="2"/>
         <input  type="hidden" name="next" value="3"/>
        </fieldset> 
    {{Form::close()}}
  </div>

  <!-- Table -->
  <table class="table">
    ...
  </table>
</div>