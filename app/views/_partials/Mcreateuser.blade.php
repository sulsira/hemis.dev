<div class="modal hide fade" id="createUser">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel">Create User</h3>	
  </div>
  {{Form::open(array('route'=>'admin.user.store'))}}
		  <div class="modal-body">				
					<div id="nameForm">
								<strong>
										Fullname
								</strong>														
								<div class="input-feild">
									<input id="fullname" type="text" placeholder="Username" name="username" class="span5" required>
									<span class="help-block">Enter your fullname </span>
								</div>
								<strong>
										Email
								</strong>														
								<div class="input-feild">
									<input id="email" type="email" placeholder="email address" name="email" class="span5"  data-validation="email" required>
								</div>
								<!-- <hr> -->
								<strong>
										Password
								</strong>														
								<div class="input-feild">
									<input id="password" type="password" placeholder="enter new password" name="password" class="span5" data-validation="strength" 
		 							data-validation-strength="2" required>
									<span class="help-block">minimum is 6 characters</span>									
								</div>
								<!-- <hr> -->
								<strong>
										Mobile Phone
								</strong>														
								<div class="input-feild">
									<input type="text" name="mobile" id="mobile" placeholder="fullname"  class="span5" data-validation="number">
									<span class="help-block">example 7052217</span>

								</div>
								<hr>
								<strong>
										Account Type
								</strong>														
								<div class="input-feild">
									<select name="accountType" id="accountType" class="span5" >
									<option>admin</option>
									<option>record</option>
									<option>scholarship</option>
									<option value="data">data</option>
									<option>school</option>
									<option>research</option>
								</select>
									<span class="help-block">The information system that the user is assign to</span>

								</div>
								<strong>
										User Type
								</strong>														
								<div class="input-feild">
								<select name="userType" id="userType" class="span5" >
									<option value="internal">Internal (MOHERST)</option>
									<optgroup label="external">	
									<?php $schools = School::all(); ?>
									@if($schools)
											@foreach($schools as $key => $value)
												<option value="<?php echo $value->LeCe_LearningCenterID ?>">{{$value->LeCe_Name}}</option>												
											@endforeach
										@else 
									@endif						
									</optgroup>
								</select>
									<span class="help-block">The section that the user is allowed to access</span>
								</div>
								<strong>
										Previleges
								</strong>														
								<div class="input-feild">
								<select name="Previleges" id="accountPriv" class="span5">
									<option>veda</option>
									<option value="ve">view and edit</option>
									<option value="e">edit only</option>
									<option value="ea">edit and add</option>
									<option value="v">view only</option>
									<option value="super">super account</option>
								</select>	
									<span class="help-block">Access on how the user manipulate assignment data</span>
								</div>				
				</div>																																				
		  </div>

		<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" name="createUser">Save changes</button>
		</div><!-- end of modal footer -->			  
 {{Form::close()}}

</div>
