<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h3 id="myModalLabel">( Staff )</h3>				
	</div>

  <div class="modal-body">
		{{Form::open(['route'=>['school.staffs.update','alter'],'method'=>'PUT'])}}
			<div id="nameForm">
					<fieldset>
						<legend>
							<strong>
								Fullname
							</strong>														
						</legend>
						<div class="input-feild">
							<input type="text" name="FirstName" id="firstName" placeholder="First Name" required="">
							<input type="text" name="MiddleName" id="middleName" placeholder="Middle Name" class="span2">
							<input type="text" name="LastName" id="lastName" placeholder="Last Name" required="">
						</div>
					</fieldset>
			</div>
			<div id="detailsForm">
					<fieldset>
						<legend>
							<strong>
								details
							</strong>	
						</legend>
						<div class="input-heading">
							<span class="bday">birthday</span>
							<span class="enthnic">ethniticity</span>
							<span class="nation">nationality</span>
						</div>
						<div class="input-feild">
							<input type="text" name="bday" id="bday" placeholder="day" class="span1">
							<input type="text" name="bmonth" id="bmonth" placeholder="month" class="span1">
							<input type="text" name="byear" id="byear" placeholder="year" class="span1">
							<select name="enit" id="enit">
								<?php $countries = Variable::domain('Pers_Ethnicity')->toArray();  ?>
								@foreach ($countries as $key => $country)
								<option>{{$country['Vari_VariableName']}}</option>
								@endforeach
							</select>
							<select name="nation" id="nation">
								<?php $countries = Variable::domain('Country')->toArray();  ?>
								@foreach ($countries as $key => $country)
								<option>{{$country['Vari_VariableName']}}</option>
								@endforeach
							</select>

						</div>
						<div class="input-heading">
							<span class="gender">gender</span>
						</div>
						<div class="input-feild">
							<select name="gender" id="gender">
						 		<option>male</option>
						 		<option>female</option>
						 	</select>
						</div>
					</fieldset>
			</div>	
						<input type="hidden" data-schoolId="" id="lece_id">
						<input type="hidden" value="" id="persId" name="persId">
						<input type="hidden" value="" id="add_id" name="add_id">
						
  </div>
  <div class="modal-footer">
		<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				<button class="btn btn-primary" name="edit_student">Save changes</button>
		</div><!-- end of modal footer -->	
	{{Form::close()}}	
  </div>
</div>