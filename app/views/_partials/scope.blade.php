
@if(Request::is('admin/school/*'))
        
  @elseif(Request::is('admin/institution/*'))

    @if(is_numeric(Request::segment(3)))
      <?php
       $id = Request::segment(3); 
       $schoolID = $data['school']['LeCe_LearningCenterID'];
       ?>
        <div class="scope">
          <div class="hedacont">
            <div class="navbar">
              <div class="navbar-inner" id="scopebar">
                  <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="{{route('institution',$id)}}">{{e($data['school']['LeCe_Name'])}}</a>
                    <div class="nav-collapse collapse navbar-responsive-collapse">
                      <ul class="nav">
                        <li><a href="{{route('students', $schoolID)}}">students</a></li> 
                                                    
                          <li><a href="{{route('staffs', $schoolID)}}">staff</a></li>
                          <li><a href="{{route('courses', $schoolID)}}">courses</a></li>
                          <li><a href="{{route('contacts', $schoolID)}}">contact</a></li>                              

                        </ul>
                    </div><!-- /.nav-collapse -->
                  </div>
              </div><!-- /navbar-inner -->
            </div>              
          </div>  
      </div>  
    @endif
@endif <!-- end of first if 'admin/institution/*'-->
<?php 
      $domain = Session::get('userDomain');
      $fullname = Session::get('fullname');
      $seg1 =Request::segment(1);
      $seg2 =Request::segment(2);
      $seg3 =Request::segment(3);
      $dir = $seg1.DS.$seg2;
 ?>
<?php switch ($dir) :
case 'research\researcher' : ?>
        <div class="scope">
          <div class="hedacont">
            <div class="navbar">
              <div class="navbar-inner" id="scopebar">
                  <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </a>
                    {{HTML::link('research/researcher',e(ucwords($fullname)),['class'=>'brand'])}}
                    <div class="nav-collapse navbar-responsive-collapse">
                      <ul class="nav">   
                        <li> {{HTML::link('research/researcher/researchs','Publications')}} </li>                                                
                          <li>{{HTML::link('research/researcher/create','Upload research')}} </li>
                          <li>{{HTML::link('research/researcher/settings','Edit details')}} </li>                              
                        </ul>
                      <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                          <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Options<b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">moherst portal</a></li>
                            <li><a href="#">Other Researchers</a></li>
                            <li><a href="#">Other Publications</a></li>
                          </ul>
                        </li> 
                      </ul>
                    </div><!-- /.nav-collapse -->
                  </div>
              </div><!-- /navbar-inner -->
            </div>              
          </div>  
      </div> 
<?php break; ?>
<?php endswitch;?>

    </div><!-- end of main-header -->
  </div>  <!-- end of fixheda  -->
