<?php 
  $session = Session::get('userDomain');
  $fullname = Session::get('fullname');
  $lcn = array_pop($menu);
  // dd(Route::getCurrentRoute());
?>
@include('_partials/doc')
	@section('style')
	{{ HTML::style('css/main.css') }}
  {{ HTML::style('css/style.css') }}   
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	@show      
    </head>
    <body>
     <div class="progress progress-striped active" style="margin:200px; z-index:10000; display:none;">
       <div class="bar"></div>
     </div>
	    <div class="top">
      	    	@include('_partials/menu')
      	  		<div class="acctheda  fixheda_style">
      				<div class="navbar">
      				 	<div class="navbar-inner">
      						<div class="hedacont">								 
      						    <a class="active brand" href="{{ route('admin') }}">{{ strtoupper($session . ' account ')}}</a>
      						    <ul class="nav">
      						      <li><a href="{{ route('school.students.create') }} ">Add Student</a></li>
                        <!--
      						      <li><a href="{{ route('school.staffs.create') }} ">add Staff</a></li>
      						      <li><a href="{{ route('school.courses.create') }} ">add course</a></li>
                        -->
      						    </ul>
      						</div>
      					</div>
      				</div>						  			
      	  		</div>	  		
              <div class="scope">
                <div class="hedacont">
                  <div class="navbar">
                    <div class="navbar-inner" id="scopebar">
                        <div class="container">
                          <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </a>
                          {{HTML::link('school',e(ucwords($lcn)),['class'=>'brand'])}}
                          <div class="nav-collapse navbar-responsive-collapse">
                            <ul class="nav">   
                              <li <?php echo (Request::segment(2) == 'students')? "class='active'" : '';?>> {{HTML::link('school/students','students',['id'=>'students'])}} </li>                                                
                                <li <?php echo (Request::segment(2) == 'staffs')? "class='active'" : '';?>>{{HTML::link('school/staffs','staffs')}} </li>
                                <li <?php echo (Request::segment(2) == 'courses')? "class='active'" : '';?>>{{HTML::link('school/courses','courses')}} </li>                                                     
                                <li <?php echo (Request::segment(2) == 'templates')? "class='active'" : '';?>>{{HTML::link('school/templates','templates')}} </li>                            
                              </ul>
                            <ul class="nav pull-right">
                              <li class="divider-vertical"></li>
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Options<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                  <li>{{HTML::link('school/templates','Moherst Templates')}}</li>
                                </ul>
                              </li> 
                            </ul> 
                          </div><!-- /.nav-collapse -->
                        </div>
                    </div><!-- /navbar-inner -->
                  </div>              
                </div>  
            </div> 	
	    </div>  <!-- end of top --> 
	    <div class="wrapper"><!-- wrapper starts -->
		    <div class="row content">
		    	@yield('container')
		    </div>	    	
	    <footer>
	    	<div class="hedacont">MOHERST &copy; Copyright 2013</div>
	    	@yield('footer')
	    </footer>
@include('_partials/footer')
