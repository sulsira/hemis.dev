<?php
#View specific processing
$school = $data['school'];
$contacts = $data['school']['contacts'];
// end of specific processing

?>	
@include('templates/school')
	@if(!empty($contacts))
		<table class="table">
			<thead>
				<tr>
					<th>contact type</th>
					<th>contact</th>
					<th>added data</th>
				</tr>
				
			</thead>
			<tbody>
				<?php foreach ($contacts as $key => $value): ?>
				<tr>
					<td>{{e($value['Cont_ContactType'])}}</td>
					<td>{{e($value['Cont_Contact'])}}</td>
					<td>{{e($value['Cont_AddDate'])}}</td>
				</tr>					
				<?php endforeach ?>

			</tbody>
		</table>
	@else
		<p>
			no contact detail in database
		</p>
	@endif

</div>

<div class="cft">
	
</div>

@stop