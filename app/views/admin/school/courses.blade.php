<?php
#View specific processing
$school = $data['school'];
$courses = $data['school']['courses'];
// end of specific processing
?>
@include('templates/school')
						<table class="table table-bordered">
							  <thead>
							    <tr>
							      <th>#</th>
							      <th>course name</th>
							      <th>status</th>
							      <th>tuition fee</th>
							    </tr>
							  </thead>
							  <tbody data-tbtype="courses">
							  	<!-- <tr><td colspan="7" class="maked">lets see</td></tr> -->
							      
									<?php  foreach( $courses as $acourse) : ?>
										<tr data-rnum="<?php echo $acourse['Cour_CourseID']; ?>" class="onmodal" role="button" data-toggle="modal" data-mtype="course">
											<td class="maked">...</td>
											<td><?php echo $acourse['Cour_CourseLocalName']; ?></td>
											<td><?php echo $acourse['Cour_Status']; ?> </td>
											<td><?php echo $acourse['Cour_Tuition']; ?></td>
										</tr>
									<?php endforeach; ?>
							  </tbody>
						</table>
</div>

<div class="cft">
	
</div>

@stop