<?php
#View specific processing
$school = $data['school'];
$courses = $data['school']['courses'];
// end of specific processing
?>	
@include('templates/school')
	<?php $classes = array(); ?>
	@foreach($courses as $key => $value )

			<?php $classes[$value['Cour_Year']][] = $value; ?>
	@endforeach
<?php if (!empty($classes)):  ?>
	<table class="table">
		<thead>
			<tr>
				<th>Year</th>
				<th>Programme</th>
				<th>Tution</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($classes as $key => $value): ?>
				<tr>
					<td rowspan="<?php echo count($value) + 1 ?>"><?php echo $key; ?></td>
				</tr>
				<?php foreach ($value as $key1 => $value1): ?>
					<tr>
						<td><?php echo $value1['Cour_CourseLocalName'] ?></td>
						<td><?php echo $value1['Cour_Tuition'] ?></td>
					</tr>
				<?php endforeach ?>
			<?php endforeach ?>
		</tbody>
	</table>
<?php else: ?>
	<p><h4>There is no data in the database</h4></p>
<?php endif ?>
</div>

<div class="cft">
	
</div>

@stop