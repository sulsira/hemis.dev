<?php
#View specific processing
$school = $data['school'];
$classes = $data['courses'];
$persons = $data['school']['persons'];
$student = array(); 
// end of specific processing
?>
@include('templates/school')
				<?php foreach ($persons as $key1 => $value1): ?>
					<?php if (strtolower($value1['Pers_Type']) == 'student'): ?>
						@if(strtolower($value1['Pers_Gender']) == 'female')
							<?php $student['female'][] = $value1; ?>
						@elseif(strtolower($value1['Pers_Gender']) == 'male')
							<?php  $student['male'][] = $value1; ?>
						@else
							<?php $student['others'][] = $value1; ?>
						@endif
					<?php endif ?>
		<?php endforeach ?>
					<div class="statistics-area">
					<?php if (!empty($student)): ?>
						<div class="table-age-range">
							<h3>student</h3>
							<table class="table table-bordered student-gender-general">
							  <caption>students by gender</caption>
							  <thead>
							    <tr>
							      <th>gender</th>
							      <th>total</th>
							      <th>percentage</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <td>male</td>
							      <td>{{count($student['female'])}}</td>
							      <td>-</td>
							    </tr>
							    <tr>
							      <td>female</td>
							      <td>{{count($student['male'])}}</td>
							      <td>-</td>
							    </tr>
							  </tbody>
							</table>
						</div>
						<hr>
						<?php else: ?>
						 <h3>There is no data for this school</h3>						
					<?php endif ?>

		</div>
		</div><!-- content conent -->
		<div class="cft">
			
		</div><!-- content footer -->
@stop