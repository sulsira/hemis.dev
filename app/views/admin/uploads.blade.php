@include('templates/admin')
  		@if($errors->any())
			<hr>
			<ul>
				{{implode('',$errors->all('<li>:message</li>'))}}
			</ul>
		@endif
					    <div class="tab-pane" id="tab2">
					      <div class="todb">
					    	{{Form::open(array('action' => 'AdminController@postUploads', 'files' => true));}}

					      		<legend>upload file </legend>
					      		<em>only files with extensions: csv, docx, doc etc</em>
					      		<hr>
								<select name="type">
									<option>Database</option>
									<option value="internal">Internal (MOHERST)</option>
									<option value="private">personal file</option>
									<option value="schools">All schools</option>
									<optgroup label="Learning Centers">	
									<?php $schools = School::all(); ?>
									@if($schools)
											@foreach($schools as $key => $value)
												<option value="<?php echo $value->LeCe_Name.'#'.$value->LeCe_LearningCenterID ?>">{{$value->LeCe_Name}}</option>												
											@endforeach
										@else 
									@endif						
									</optgroup>
								</select>	
					      			<input type="file" name="file" id="filetemplate">
					      		<hr>
					      		<button type="submit" class="btn" name="document" value="public">upload now</button>
					      	{{Form::close()}}

					      </div>
					    </div>


</div>
<div class="cft">
</div><!-- content footer -->
@stop

