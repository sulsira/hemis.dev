@section('scope')
	@include('_partials/scope')	  
@stop
@section('container')
<div class="chd">
	<?php $contacts= array(); ?>
	@foreach($data['school']->toArray() as $key => $value )
		@foreach ($value['contacts'] as $key1 => $value1) 
			<?php $contacts[] = $value1 ?>
		@endforeach
	@endforeach
</div>
<div class="cc">
	@if(!empty($contacts))
		<table class="table">
			<thead>
				<tr>
					<th>contact type</th>
					<th>contact</th>
					<th>added data</th>
				</tr>
				
			</thead>
			<tbody>
				<?php foreach ($contacts as $key => $value): ?>
				<tr>
					<td>{{e($value['Cont_ContactType'])}}</td>
					<td>{{e($value['Cont_Contact'])}}</td>
					<td>{{e($value['Cont_AddDate'])}}</td>
				</tr>					
				<?php endforeach ?>

			</tbody>
		</table>
	@else
		<p>
			no contact detail in database
		</p>
	@endif

</div>

<div class="cft">
	
</div>

@stop