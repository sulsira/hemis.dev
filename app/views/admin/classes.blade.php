@section('scope')
	@include('_partials/scope')	  
@stop
@section('container')
<div class="chd">
	
</div>
<div class="cc">
	<?php $classes = array(); ?>
	@foreach($data['school']->toArray() as $key => $value )
		@foreach ($value['courses'] as $key1 => $value1) 
			<?php $classes[$value1['Cour_Year']][] = $value1; ?>
		@endforeach
	@endforeach
<?php if (!empty($classes)):  ?>
	<table class="table">
		<thead>
			<tr>
				<th>Year</th>
				<th>Programme</th>
				<th>Tution</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($classes as $key => $value): ?>
				<tr>
					<td rowspan="<?php echo count($value) + 1 ?>"><?php echo $key; ?></td>
				</tr>
				<?php foreach ($value as $key1 => $value1): ?>
					<tr>
						<td><?php echo $value1['Cour_CourseLocalName'] ?></td>
						<td><?php echo $value1['Cour_Tuition'] ?></td>
					</tr>
				<?php endforeach ?>
			<?php endforeach ?>
		</tbody>
	</table>
<?php else: ?>
	<p><h4>There is no data in the database</h4></p>
<?php endif ?>
</div>

<div class="cft">
	
</div>

@stop