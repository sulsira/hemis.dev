<?php 
	$seg = Request::segments();
	$name = (!is_numeric(last($seg)) && $seg[0] == 'admin')? last($seg) : $data['userData']->fullname;
 ?>
@section('container')
@include('_partials/Mcreateuser')
<div class="chd">
		<h3>{{$name}}</h3>
	<hr>
	<div class="cht">
		<p class="school_details_sumarry">
			<strong><a  href="#createUser" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i>Add a user</a></strong> 
			<strong><a href="">DELETE</a></strong>
			<strong><a href="">Log out</a></strong>
			<strong><a href="">DISABLE</a></strong>
		</p>
		@if($errors->any())
			<hr>
			<ul>
				{{implode('',$errors->all('<li>:message</li>'))}}
			</ul>
		@endif
	</div>
	<hr>
</div>
<div class="cc">
 <section>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Email</th>
				<th>Fullname</th>
				<th>Status</th>
				<!-- <th>Log</th> -->
				<th>Edit</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>	
		@if(count($data['schools']) > 0)
			@foreach($data['schools'] as $key=>$value)
				<tr>
					<td>{{e($value->email)}}</td>
					<td>{{e($value->fullname)}}</td>
					<td>{{e($value->status)}}</td>
					<!-- <td>{{e($value->email)}}</td> -->
					<td>{{HTML::link('admin/user/'.$value->id.'/edit','user')}}</td>
					<!-- <td>{{HTML::link('admin/user/'.$value->id.'/destroy','delete')}}</td> -->
					<td>{{Form::delete('admin/user/'. $value->id, 'Delete')}}</td>
				</tr>				
			@endforeach
		@else 
			<tr>
				<td colspan="3">We could not find any users.</td>	
			</tr>
		@endif
		</tbody>
	</table>
</section>
</div><!-- content conent -->
<div class="cft">
</div><!-- content footer -->
@stop

