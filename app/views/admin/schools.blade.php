@section('container')
		<div class="chd">
				<h3>list of all schools</h3>
			<hr>
			<div class="cht">

			</div>
			<hr>
		</div>
		<div class="cc">
			<ul class="allschool">
		 	@foreach ($data['schools'] as $value) 
		 	<?php $id = $value->LeCe_LearningCenterID ?>
				<li>
					<strong><a href="{{route('institution', $id )}}"  data-toggle="tooltip" title="" data-original-title="Ctrl + Click to open link in a new tab" class="tipify" >{{$value->LeCe_Name}}</a></strong>
					<p>
					<strong>Ownership : </strong> {{$value->LeCe_Ownership}} | <strong>Capacity : </strong>  {{$value->LeCe_EnrolmentCapacity}} 
					| <strong>Classification : </strong>   {{$value->LeCe_Classification}} 
					<a href="{{route('institution', $id )}}" target="_blank" class="pop-over" data-toggle="popover" data-placement="right" data-content="content coming soon." title="" data-original-title="">MORE...</a>
					</p>
				</li>
				<hr>
		 	@endforeach				

				
			</ul>	
		</div><!-- content conent -->
		<div class="cft pagination pagination-large">
			<p>
				<?php 
					// echo $data['schools']->getTotal();
				 ?>
			</p>
			<?php echo $data['schools']->links() ?>
		</div><!-- content footer -->
@stop