<?php 	
	$moherstusers = $data['moherstusers'];
	$department = $data['department'];
 ?>

@include('templates/admin')
 <script id="tabletrusers" type="hems/template">
 	<tr>
 		<td>{name}</td>
 		<td>{directrate}</td>
 		<td>{status}</td>
 		<td>
 			<input type="checkbox" name="action" class="checkbox" data-input-type="user" data-input-value="{id}" checked="checked"/>
 			<input	type="hidden" name="{userId}" data-input="" value="{id}" />
 		</td>
 	</tr>
 </script>
	<button href="#myModal" role="button" class="btn" data-toggle="modal">Create a new task</button>
	<hr>
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h3 id="myModalLabel">Creating a new task</h3>
	  </div>
	  {{Form::open(['route'=>'admin.tasks.store','id'=>'task'])}}
	  <div class="modal-body">
			<label>task title :</label>
			<input type="text" name="title" />
			<hr>
			<label>details :</label>
			<textarea name="details"></textarea>
			<hr>
		  <!-- <input type="checkbox" name="email" /><span>email as wel</span> -->
		  <span>schedule task : </span> <input type="date" name="publish"/>	
		  <hr>
		  <label>select user to task :</label>
			<select name="userSection" data-input="" id="userselection">
				<option value="depart">YOUR DEPARTMENT</option>
				<option value="all">all users</option>
				<option value="smt">senior management</option>
				<option value="directors">directors</option>
				<optgroup label = "Users">
					<?php foreach ($moherstusers as $key => $value): ?>
						<option value="{{$value['id']}}">{{$value['fullname']}}</option>
					<?php endforeach ?>
				</optgroup>
				<optgroup label = "directorates">
					<option value="sti">Sti</option>
					<option value="he">higher education</option>
					<option value="admin">admin</option>
					<option value="research">research and planing</option>
					<option value="messengers">messengers</option>
				</optgroup>
			</select>
			<hr>
			<?php if ($department): ?>
		  <table class="table" id="displayusers">	
		  		<thead>	
	  				<th>name</th>
	  				<th>directrate</th>
	  				<th>status</th>
	  				<th>action</th>
		  		</thead>
		  		<tbody>	
		  		<?php foreach ($department as $key => $value): ?>
	  				<tr>
	  					<td>{{$value['fullname']}}</td>
	  					<td>{{$value['userGroup']}}</td>
	  					<td>{{$value['status']}}</td>
	  					<td id="action">
	  						<input	type="checkbox" name="action" checked="checked" class="checkbox"/>
	  						<input	type="hidden" name="{{$key}}_userId" data-input="" value="{{$value['id']}}"/>
	  					</td>
	  				</tr>		  			
		  		<?php endforeach ?>
		  		</tbody>
		  </table>					
			<?php endif ?>

	  </div>
	  <div class="modal-footer">
	  <input type="hidden" name="type" value="task"/>	
	  <input type="hidden" name="userId" value="{{Session::get('userId')}}"/>	
	  <input type="hidden" name="userName" value="{{Session::get('userName')}}"/>	
	    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	    <button class="btn btn-primary" type="submit" name="sendTask">SEND !</button>

	  </div>
	    {{Form::close()}}
	</div>
<hr>
	<ul class="nav nav-pills">
	  <li class="active">
	    <a href="#">Tasks</a>
	  </li>
	  <li><a href="#">failed</a></li>
	  <li><a href="#">approved</a></li>
	  <li><a href="#">out going</a></li>
	  <li><a href="#">in comming</a></li>
	</ul>
	<table class="table table-bordered">
	<thead>
		<th>title</th>
		<th>task</th>
		<th>due</th>
		<th>send</th>
		<th>from</th>
		<th>to</th>
		<th>action</th>
	</thead>
	<tbody>
	<?php foreach ($data['tasks'] as $key => $value): ?>
		<?php
			$fromID = $value['fromID'];
			$toID = $value['toID'];
			$from = User::find($fromID);
			$to = User::find($toID);
		 ?>
		<tr>
			<td>{{$value['title']}}</td>
			<td>{{$value['details']}}</td>
			<td>{{$value['due']}}</td>
			<td>{{$value['created_at']}}</td>
			<td>{{$from['fullname']}}</td>
			<td>{{$to['fullname']}}</td>
			<td class="actions">
				<a href="#">make remarks</a>|
					<div class="func"></div>
				<a href="#">problem</a>|
					<div class="func"></div>
				<a href="#">re-assign</a>|
					<div class="func"></div>
				<a href="#">delete</a>
					<div class="func"></div>
			</td>
		</tr>
	<?php endforeach ?>
	</tbody>
	</table>
</div><!-- content conent -->
<div class="cft">
</div><!-- content footer -->
@stop
