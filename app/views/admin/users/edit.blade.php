<?php 
	$seg = Request::segments();
	$name = (!is_numeric(last($seg)) && $seg[0] == 'admin')? last($seg) : $data['userData']->fullname;
 ?>
@section('container')
<div class="chd">
		<h3>{{$name}}</h3>
	<hr>
	<div class="cht">
		<p class="school_details_sumarry">
			<strong><a  href="#createUser" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i>Add a user</a></strong> 
			<strong><a href="">DELETE</a></strong>
			<strong><a href="">Log out</a></strong>
			<strong><a href="">DISABLE</a></strong>
		</p>
         @include('_partials.errors')  
         @if(Session::has('success')) 
          <h3 class="text-success">
            You have successfully uploaded: {{Session::get('success')}}
          </h3>
         @endif
	</div>
	<hr>
</div>
<div class="cc">
  {{Form::model($user,array('method'=>'PATCH','route'=>['admin.user.update',$user->id]))}}			
								<strong>
										Fullname
								</strong>														
								<div class="input-feild">
									{{Form::text('fullname',null,['class'=>"span5"])}}
									<span class="help-block">Enter your fullname </span>
								</div>
								<strong>
										Email
								</strong>														
								<div class="input-feild">
									{{Form::text('email',null,['class'=>"span5"])}}

								</div>
								<!-- <hr> -->
								<strong>
										Mobile Phone
								</strong>														
								<div class="input-feild">
									{{Form::text('phone',null,['class'=>"span5"])}}
									<span class="help-block">example 7052217</span>

								</div>
								<hr>
								<strong>
										Account Type
								</strong>
								<?php 
									$domain = ['admin','record','scholarship','data','school','research'];
								 ?>														
								<div class="input-feild">
									<select name="domain" id="accountType" class="span5" >
										<?php foreach ($domain as $key => $value): ?>
											<?php if ($value == $user->domain): ?>
												<option selected="selected">{{$user->domain}}</option>
											<?php else: ?>
												<option>{{$value}}</option>
											<?php endif ?>
										<?php endforeach ?>
								</select>
									<span class="help-block">The information system that the user is assign to</span>

								</div>
								<strong>
										User Type
								</strong>														
								<div class="input-feild">
								<select name="lickId" id="userType" class="span5" >

									<option value="internal">Internal (MOHERST)</option>
									<optgroup label="external">	
									<?php $schools = School::all(); ?>
									@if($schools)
											@foreach($schools as $key => $value)
											<?php if ($value->LeCe_LearningCenterID == $user->userType): ?>
												<option selected="selected" value="{{$user->userType}}">{{$value->LeCe_Name}}</option>
											<?php else: ?>
												<option value="<?php echo $value->LeCe_LearningCenterID ?>">{{$value->LeCe_Name}}</option>	
											<?php endif ?>
																							
											@endforeach
										@else 
									@endif						
									</optgroup>
								</select>
									<span class="help-block">The section that the user is allowed to access</span>
								</div>
								<strong>
										Previleges
								</strong>														
								<div class="input-feild">
								<select name="Previleges" id="accountPriv" class="span5">
									<option>veda</option>
									<option value="ve">view and edit</option>
									<option value="e">edit only</option>
									<option value="ea">edit and add</option>
									<option value="v">view only</option>
									<option value="super">super account</option>
								</select>	
									<span class="help-block">Access on how the user manipulate assignment data</span>
								</div>				
		<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true" type="reset">Reset Defaults</button>
				<button class="btn btn-primary" name="createUser">Save changes</button>
		</div><!-- end of modal footer -->			  
 {{Form::close()}}
</section>
</div><!-- content conent -->
<div class="cft">
</div><!-- content footer -->
@stop

