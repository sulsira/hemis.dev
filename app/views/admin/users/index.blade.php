<?php 
	$seg = Request::segments();
	$name = (!is_numeric(last($seg)) && $seg[0] == 'admin')? last($seg) : $data['userData']->fullname;
 ?>
 <!-- 
	STATUS EXPLAINED
	1 MEANS online
	2 means disabled users
	3 means never logged in (in-active)
	4 means System admin logged out
	0 means user logged out
  -->
@section('container')
@include('_partials/Mcreateuser')
<div class="chd">
		<h3>{{$name}}</h3>
	<hr>
	<div class="cht">
		<p class="school_details_sumarry">
			<strong><a  href="#createUser" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i>Add a user</a></strong> 
			<strong><a href="">DELETE</a></strong>
			<strong><a href="">Log out</a></strong>
			<strong><a href="">DISABLE</a></strong>
		</p>
		@if($errors->any())
			<hr>
			<ul>
				{{implode('',$errors->all('<li>:message</li>'))}}
			</ul>
		@endif
	</div>
	<hr>
</div>
<div class="cc">
 <section>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Email</th>
				<th>Domain</th>
				<th>Status</th>
				<!-- <th>Log</th> -->
				<th>Edit</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>	
		@if(count($data['users']) > 0)
			@foreach($data['users'] as $key=>$value)
				<?php 
				$status = '-';
				if ($value['status'] == 0)
					$status = 'offline';
				elseif($value['status'] == 1)
					$status = 'online';
				elseif($value['status'] == 2)
					$status = 'blocked';
				elseif($value['status'] == 3)
					$status = 'in-active';
				elseif($value['status'] == 4)
					$status = 'ejected';
				 ?>
				<tr>
					<td>{{e($value['email'])}}</td>
					<td>{{e($value['domain'])}}</td>
					<td>{{e($status)}}</td>
					<td>{{HTML::link('admin/user/'.$value['id'].'/edit',$value['fullname'])}}</td>
					<td>
						<div>
							{{Form::delete('admin/user/'. $value['id'], 'Delete')}}
							<!-- <input	type="checkbox" name="user[]"  data-input=""/>						 -->
						</div>
					</td>
				</tr>				
			@endforeach
		@else 
			<tr>
				<td colspan="3">We could not find any users.</td>	
			</tr>
		@endif
		</tbody>
	</table>
</section>
</div><!-- content conent -->
<div class="cft">
</div><!-- content footer -->
@stop

