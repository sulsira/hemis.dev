@include('templates/admin')
<table class="table">
<caption class="caption-right">
	<a href="">public files</a>
	<a href="private">private files</a>	
	<a href="uploads">upload files</a>
<hr>
</caption>

	<thead>
      <th>#</th>
      <th>file excert</th>
      <th>file name</th>
      <th>Added Date</th>
      <th>actions</th>
	</thead>
	<tbody>
	<?php if ($data['documents']['private']): ?>
		<?php foreach ($data['documents']['private'] as $key => $value): ?>
		<tr>
			<td>{{$key+1}}</td>
			<td>{{$value->excert}}</td>
			<td>{{$value->title}}</td>
			<td>{{$value->addedDate}}</td>
			<td>
				{{HTML::link($value->excert.'/'.$value->name,'view',['target'=>'_blank','class'=>'btn'])}}
				{{HTML::link('admin/download?sid='.urlencode($value->location),'Download file',['target'=>'_blank','class'=>'btn'])}}
			</td>
		</tr>
		<?php endforeach ?>
	<?php endif ?>
	</tbody>
</table>
</div>
<div class="cft">

</div><!-- content footer -->
@stop

