@include('templates/admin')
</div><!-- content conent -->	
<div class="cft">
<?php
 $ltype  = Variable::domain('LeCe_InstitutionType');
 $lowner  = Variable::domain('LeCe_Ownership');
 $district  = Variable::domain('Addr_District');
  ?>
	<div class="entry-typing">
		<div class="tabbable"> <!-- Only required for left/right tabs -->
		  <ul class="nav nav-tabs">
		    <li class="active"><a href="#tab1" data-toggle="tab">Adding a new school</a></li>
		    <!-- <li><a href="#tab2" data-toggle="tab">Section 2</a></li> -->
		  </ul>
		  <div class="tab-content">
		    <div class="tab-pane active" id="tab1">
		<h3>add new school (s)</h3>	
			{{Form::open(array('route' => array('admin.institution.store')));}}

					<div class="content-area">
				<div class="school-details">
<!-- 					<div class="alert alert-success">
						 <button type="button" class="close" data-dismiss="alert">&times;</button>
					 	school have been added
					</div>	 -->					
		@if($errors->any())
			<hr>

			<ul>
					<div class="alert alert-error">
						 <button type="button" class="close" data-dismiss="alert">&times;</button>
					 	{{implode('',$errors->all('<li>:message</li>'))}}
					</div>				
			</ul>
		@endif						

					
						
					<strong>School details</strong>
					<hr>
					<div class="input-heading">
						<span class="school-name">school name</span>
						<span  class="entrollment">entrollment capacity</span>
						<span  class="entrollment">Classification</span>
						<span  class="ownership">ownership</span>
						<span   class="lcenter-type">Learning center type</span>
					</div>
					<div class="input-feild">
						{{Form::text('LeCe_Name')}}
						{{Form::text('LeCe_EnrolmentCapacity')}}
						{{Form::text('LeCe_Classification')}}
						<select name="LeCe_Ownership" data-input="" id="">
							@if($lowner)
								<?php foreach ($lowner as $key => $value): ?>
									<option value="{{$value->Vari_VariableName}}">{{$value->Vari_VariableName}}</option>
								<?php endforeach ?>
							@endif							
						</select>
						<select name="LeCe_LearningCenterType" data-input="" id="">
							@if($ltype)
								<?php foreach ($ltype as $key => $value): ?>
									<option value="{{$value->Vari_VariableName}}">{{$value->Vari_VariableName}}</option>
								<?php endforeach ?>
							@endif							
						</select>
					</div>
				</div>
					<div class="error-controlling control-group error" style="display:none">
						<span class="school-name help-inline">school name</span>
						<span  class="entrollment">entrollment capacity</span>
						<span  class="ownership">ownership</span>
						<span   class="lcenter-type">Learning center type</span>
					</div>
					<hr>
					<div class="school-contacts">
						<strong>School contact information</strong>
						<hr>
						<div class="input-heading">
							<strong>address:</strong><br>
							<span class="addr-street">street</span>
							<span class="addr-town" >town</span>
							<span class="addr-town" >p.o.box</span>
							<span class="addr-region">District</span>
						</div>
						<div class="input-feild">
							{{Form::text('street')}}
							{{Form::text('town')}}
							{{Form::text('pobox')}}
						<select name="region" data-input="" id="">
							@if($district)
								<?php foreach ($district as $key => $value): ?>
									<option value="{{$value->Vari_VariableName}}">{{$value->Vari_VariableName}}</option>
								<?php endforeach ?>
							@endif							
						</select>
						</div>
						<hr>
						<div class="input-heading">
							<strong>contact information:</strong><br>

							<span class="cont-email">email</span>
							<span class="cont-tele">telephone</span>
							<span class="cont-mobile">mobile</span>
							<span class="cont-website">website</span>
						</div>
						<div class="input-feild">
							{{Form::email('email')}}
							{{Form::text('telephone')}}
							{{Form::text('mobile')}}
							{{Form::text('website')}}

						</div>
						<div class="hiddens">
							{{Form::hidden('')}}
							{{Form::hidden('')}}
							{{Form::hidden('')}}
							{{Form::hidden('')}}
						</div>
					</div>
				</div>
				<hr>
				<div class="action-area">
					{{Form::submit('save',array('class'=>'btn btn-primary','name'=>'SaveSchool'))}}
					{{Form::submit('done',array('class'=>'btn btn btn-success','name'=>'doneSchool'))}}
				</div>
			</form>		      
		    </div>
		    <div class="tab-pane" id="tab2">
		      <p>Howdy, I'm in Section 2.</p>
		    </div>
		  </div>
		</div>
	</div>
</div><!-- content footer -->
@stop

