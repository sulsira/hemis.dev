@section('container')
<div class="chd">
	<h3>{{strtoupper(e($data['userData']->fullname))}}</h3>
	<hr>
	<div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
		<p class="school_details_sumarry">
			<small>privileges : </small>
			<strong>{{$privileges}}</strong> 
			<small>user type: </small>
			<strong>{{$userType}}</strong>
		</p>
	</div>
	<hr>
</div>
<div class="cc">

	<div class="icons_zone">
	
		<p>
			<a href="admin/schools" class="imgthumb">
				<img src="imgs/user_manage.png" >
			</a>
			<a href="admin/schools">view all schools</a>
		</p>
		<p>
			<a href="admin/files" class="imgthumb">
				<img src="imgs/folder.png">
			</a>
			<a href="admin/files">file manager</a>
		</p>
		<p>
			<a href="admin/entry" class="imgthumb">
				<img src="imgs/application.png">
			</a>
			<a href="admin/entry">data entry</a>
		</p>
	</div><!-- content conent -->	
</div>
<div class="cft">

</div><!-- content footer -->
@stop

