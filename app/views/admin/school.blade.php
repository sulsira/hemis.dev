<?php
	$classes = array();
	$school = array();
	 $clas = $data['courses']->toArray();
	 $lece = $data['school']->toArray();
	 foreach ($clas as $key => $value) {
	 	$classes[$value['Cour_Year']][] = $value;
	 }

	 foreach ($lece as $key => $value) {
	 	$school = $value;
	 }
  $student = array();  
?>
@include('templates/school')
		<?php foreach ($data['school'] as $key => $value): ?>
			<?php $value = $value->persons->toArray();?>
				<?php foreach ($value as $key1 => $value1): ?>
					<?php if (strtolower($value1['Pers_Type']) == 'student'): ?>
						@if(strtolower($value1['Pers_Gender']) == 'female')
							<?php $student['female'][] = $value1; ?>
						@elseif(strtolower($value1['Pers_Gender']) == 'male')
							<?php  $student['male'][] = $value1; ?>
						@else
							<?php $student['others'][] = $value1; ?>
						@endif
					<?php endif ?>
				<?php endforeach ?>
		<?php endforeach ?>
					<div class="statistics-area">
			<div class="table-age-range">
				<h3>student</h3>
				<table class="table table-bordered student-gender-general">
				  <caption>students by gender</caption>
				  <thead>
				    <tr>
				      <th>gender</th>
				      <th>total</th>
				      <th>percentage</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>male</td>
				      <td>{{count($student['female'])}}</td>
				      <td>-</td>
				    </tr>
				    <tr>
				      <td>female</td>
				      <td>{{count($student['male'])}}</td>
				      <td>-</td>
				    </tr>
				  </tbody>
				</table>
			</div>
<hr>
			<div>
				<h3> Programme Statistics </h3>
				<hr>
				<?php if (!empty($classes)):  ?>
					<table class="table">
						<thead>
							<tr>
								<th>Year</th>
								<th>Programme</th>
								<th>Tution</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($classes as $key => $value): ?>
								<tr>
									<td rowspan="<?php echo count($value) + 1 ?>"><?php echo $key; ?></td>
								</tr>
								<?php foreach ($value as $key1 => $value1): ?>
									<tr>
										<td><?php echo $value1['Cour_CourseLocalName'] ?></td>
										<td><?php echo $value1['Cour_Tuition'] ?></td>
									</tr>
								<?php endforeach ?>
							<?php endforeach ?>
						</tbody>
					</table>
				<?php else: ?>
					<p><h4>There is no data in the database</h4></p>
				<?php endif ?>

			</div>

		</div>
		</div><!-- content conent -->
		<div class="cft">
			
		</div><!-- content footer -->
@stop