@section('container')
@include('_partials/MFupload')

<div class="chd">
	<h3>{{strtoupper(e($data['userData']->fullname))}}</h3>
	<hr>
	<div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
		<p class="school_details_sumarry">
			<small>privileges : </small>
			<strong>{{$privileges}}</strong> 
			<small>user type: </small>
			<strong>{{$userType}}</strong>
		</p>
	</div>
	<hr>
</div>
<div class="cc">
<hr>
 @include('_partials.errors')  
 @if(Session::has('success')) 
  <h3 class="text-success">
    You have successfully uploaded: {{Session::get('success')}}
  </h3>
 @endif
<hr>
<table class="table">
<caption class="caption-right">
  	<a href="#myModal" role="button" data-toggle="modal">Upload files</a>
<hr>
</caption>

	<thead>
      <th>#</th>
      <th>file name</th>
      <th>Added Date</th>
      <th>actions</th>
	</thead>
	<tbody>
	<?php if ($data['documents']['public']): ?>
		<?php foreach ($data['documents']['public'] as $key => $value): ?>
		<tr>
			<td>{{$key+1}}</td>
			<td>{{$value->title}}</td>
			<td>{{$value->created_at}}</td>
			<td>
	      {{HTML::link('school/download?file='.urlencode($value['fullpath']),'Download',['target'=>'_blank','class'=>'btn'])}}
			</td>
		</tr>
		<?php endforeach ?>
	<?php endif ?>
	</tbody>
</table>
</div>
<div class="cft">
</div><!-- content footer -->
@stop