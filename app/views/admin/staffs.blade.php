@section('scope')
	@include('_partials/scope')	  
@stop
@section('container')
		<?php  $staffs = array(); ?>
		<?php foreach ($data['school'] as  $key => $value): ?>
			<?php foreach ($value['persons'] as $key1 => $value1): ?>
				<?php if (strtolower($value1['Pers_Type']) == 'staff'): ?>
					<?php $staffs[] = $value1->toArray(); ?>
				<?php endif ?>
			<?php endforeach ?>
		<?php endforeach ?>
			<div class="chd">
					<h3>{{e($data['school']->toArray()[0]['LeCe_Name'])}}</h3>
				<hr>
				<div class="cht">
	
				</div>
				<hr>
			</div>
			<div class="cc">

							<table class="table table-bordered">
							  <thead>
							    <tr>
							      <th>#</th>
							      <th>staff name</th>
							      <th>birthday</th>
							      <th>gender</th>
							      <th>nationality</th>
							      <!-- <th>staff type</th> -->
							      <!-- <th>qualification</th> -->
							      <th>last update</th>
							      
							    </tr>
							  </thead>
							  <tbody class="table table-bordered pupinfo">
							  	<!-- <tr><td colspan="8" class="maked">lets see</td></tr> -->
							  	<?php  foreach( $staffs as $alecturer) : ?>
							      	<?php
										$sdob = explode(' ',$alecturer['Pers_DOB']);
										$lupd = explode(' ',$alecturer['Pers_UpdateDate']);
										array_pop($sdob);
										array_pop($lupd);
										$timestamp = (!empty($sdob))? strtotime($sdob[0]) : '';
										$timestamp2 = (!empty($lupd))? strtotime($lupd[0]) : '' ;
										$dob = (!empty($timestamp))? strftime("%Y-%b-%d",$timestamp) : '';
										$lastupd = (!empty($timestamp2))? strftime("%Y-%b-%d",$timestamp2) : '';
									?>
									<tr data-rnum="<?php echo $alecturer['Pers_PersonID']; ?>" class="onmodal" role="button" data-toggle="modal" data-mtype="staff">
										<td class="mark"></td>
										<td><?php echo $alecturer['Pers_FirstName'].' '.$alecturer['Pers_MiddleName'].' '.$alecturer['Pers_LastName'] ; ?></td>
										<td> <?php
												 $sdob = explode(' ',$alecturer['Pers_DOB']);
													if ( count($sdob)  > 1) {
														
														echo $sdob[0];
													}else{
														echo $alecturer['Pers_UpdateDate'];
													}
												?></td>
										<td><?php echo $alecturer['Pers_Gender']; ?> </td>

										<td><?php echo $alecturer['Pers_Nationality']; ?></td>
										<!-- <td>..</td> -->
										<td> <?php
												 $sdob = explode(' ',$alecturer['Pers_UpdateDate']);
													if ( count($sdob)  > 1) {
														
														echo $sdob[0];
													}else{
														echo $alecturer['Pers_UpdateDate'];
													}
												?></td>
									</tr>
							    <?php endforeach; ?>
							  </tbody>
						</table>
	
			</div>
			</div><!-- content conent -->
			<div class="cft">
				
			</div><!-- content footer -->
		
@stop		