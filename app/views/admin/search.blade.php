@section('container')
<div class="chd">
	<h3>Search results</h3>
	<hr>
	<div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
	</div>
</div>
<div class="cc">

	<section>
		<?php if (!empty($data['search'])): ?>
			<div class="stats">	
					<a href="#">fetch results</a>
			</div>	
			<hr>
			<ul class="search-results">
				<?php foreach ($data['search'] as $key => $value): ?>
					@if($key == 'school')
						<?php foreach ($value as $key1 => $value1): ?>
							<li class="result">
								<div class="search-detail">

									<strong><a href="{{route('institution',$value1['LeCe_LearningCenterID'])}}">{{ucwords($value1['LeCe_Name'])}}</a></strong>
									<p>
									<span>Result Type : </span> <strong>Learning Center</strong> |
									<span>Classification : </span> <strong>{{ucwords($value1['LeCe_Classification'])}}</strong> |
									@if(!empty($value1['LeCe_EnrolmentCapacity']))
									<span>Entrollment Capacity : </span> <strong>{{ucwords(e($value1['LeCe_EnrolmentCapacity']))}}</strong> |
									@endif									
									<span>Ownership: </span> <strong>{{ucwords(e($value1['LeCe_Ownership']))}}</strong> 
									</p>
								</div>
								<hr>
							</li>							
						<?php endforeach ?>
					
						@elseif($key == 'person')
						<?php foreach ($value as $key1 => $value1): ?>
						<li class="result">
							<div class="search-detail">
								<?php 
									$url = $value1['Pers_LearningCenterID'].DS.strtolower(str_plural($value1['Pers_Type'])).DS.$value1['Pers_PersonID'];
									// $url = $value1['Pers_LearningCenterID'].DS.'students'.DS.$value1['Pers_PersonID'];
									$m = (!empty($value1['Pers_MiddleName']))? $value1['Pers_MiddleName'] : ' ' ;
									$name = ucwords($value1['Pers_FirstName'].' ' .$m .' '. $value1['Pers_LastName']);
								 ?>
								<strong>
									{{HTML::link('admin/institution/'.$url,$name)}}
								</strong>
								<p>
								<span>Person Type : </span> <strong>{{ucwords(e($value1['Pers_Type']))}}</strong> |
								<span>Person Domain : </span><a href="{{route('institution',$value1['Pers_LearningCenterID'])}}">{{ucwords('school')}}</a></strong> |
								<span>Nationality : </span> <strong>{{ucwords(e($value1['Pers_Nationality']))}}</strong> |
								<span>Gender : </span> <strong>{{ucwords(e($value1['Pers_Gender']))}}</strong> |
								<span>Unique identifier : </span> <strong>{{ucwords(e($value1['Pers_PersonIdentifier']))}}</strong> 
								</p>
							</div>
							<hr>
						</li>							
						<?php endforeach ?>

						@elseif($key == 'contact')
						<?php foreach ($value as $key1 => $value1): ?>
							<li class="result">
								<div class="search-detail">
									<strong>

									{{ucwords($value1['Cont_Contact'])}}

									</strong>
									<p>
									<span>Result Type : </span> <strong>{{ucwords('Contact')}}</strong> |
									<span>Contact For : </span> <strong>{{ucwords($value1['Cont_EntityType'])}}</strong> |
									<span>Contact Type : </span> <strong>{{ucwords($value1['Cont_ContactType'])}}</strong> |
									@if($value1['Cont_EntityType'] == 'Learning Center')

										<span>Contact Domain : </span> <strong>{{HTML::link('admin/institution/'.$value1['Cont_EntityID'].'/'.'contacts',ucwords($value1['Cont_EntityType']))}}</strong> 

										@elseif($value1['Cont_EntityType'] == 'Staff')

										<span>Contact Domain : </span> <strong></strong> 
										<!-- HTML::link('admin/institution/'.$value1['Cont_EntityID'].'/'.'contacts',ucwords($value1['Cont_EntityType'])) -->

										@elseif($value1['Cont_EntityType'] == 'Student')

										<span>Contact Domain : </span> <strong>
										<!-- HTML::link('admin/institution/'.$value1['Cont_EntityID'].'/'.'contacts',ucwords($value1['Cont_EntityType'])) -->
										
										</strong> 

										@else

										<span>Contact Domain : </span> <strong>{{HTML::link('admin/institution/'.$value1['Cont_EntityID'].'/'.'contacts',ucwords($value1['Cont_EntityType']))}}</strong> 
									@endif
									
									</p>
								</div>
								<hr>
							</li>							
						<?php endforeach ?>
	
						@else
						<strong>unidentified search results</strong>

					@endif
				<?php endforeach ?>


			</ul>
			<div class="stats">	
					<a href="#">fetch results</a>
			</div>	
			<hr>
			<?php else: ?>
				<h3>Sorry there were no results</h3>
		<?php endif ?>

	</section>
</div>
<div class="cft">
</div><!-- content footer -->
@stop
