@section('scope')
	@include('_partials/scope')	  
@stop
@section('container')
<div class="chd">
	<h3>{{strtoupper(e($data['userData']->fullname))}}</h3>
	<hr>
	<div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
		<p class="school_details_sumarry">
			<small>privileges : </small>
			<strong>{{$privileges}}</strong> 
			<small>user type: </small>
			<strong>{{$userType}}</strong>
		</p>
	</div>
	<hr>
</div>
<div class="cc">

 	<div>
  		<p class="alert alert-success">SCHOLARSHIP APPLICATIONS</p>
  	<div>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane fade active in" id="ug">
        <!-- <h3>Undergraduate</h3> -->
        <p>
           Raw denim you probably haven't heard of them jean shorts Austin. 
        </p>
        <table class="table table-bordered"> 
          <thead>
            <tr>
              <th>
                 ID
              </th>
              <th>
                 ScholarshipID
              </th>
              <th>
                 Award
              </th>
              <th>
                Field of study
              </th> 
              <th>
                 Level of study
              </th>
              <th>
                 Status
              </th>
            </tr>
          </thead>
          <tbody>
            @if($data['applications'])
               @foreach($data['applications'] as $key => $value)
                  <tr>
                    <td>
                       {{$value->id}}
                    </td>
                    <td>
                      {{HTML::link('scholarship/applicant/'.$value->studentID,$value->scholarshipID)}}
                    </td>
                    <td>
                      <!-- {{HTML::link('theurl',$value->award)}} -->
                      {{$value->award}}
                    </td>
                    <td>
                      {{$value->filedofstudy}}
                    </td>
                    <td>
                       {{$value->leveofstudy}}
                    </td>
                    <td>
                    @if($value->status == 0) 
                    Not applied 
                    @elseif($value->status == 1)
                    Done applying
                    @elseif($value->status == 2)
                    Application confirmed
                    @elseif($value->status == 3)
                    Still applying
                    @elseif($value->status == 4)
                    Terminated
                    @endif
                    </td>
                  </tr>                
               @endforeach
            @else 


            @endif

          </tbody>
        </table>
      </div>

      <div class="tab-pane fade" id="ms">
        <h3>Masters</h3>
        <p>
           Raw denim you probably haven't heard of them jean shorts Austin. 
        </p>
        <table class="table table-bordered"> 
          <thead>
            <tr>
              <th>
                 #
              </th>
              <th>
                 Scholarship
              </th>
              <th>
                 Details
              </th>
              <th>
                 Country
              </th> 
              <th>
                 Dead-Line
              </th>
              <th>
                 Status
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                 1
              </td>
              <td>
                Gambia Government UTG
              </td>
              <td>
                 Raw denim you probably haven't heard of them jean shorts Austin. 
              </td>
              <td>
                The Gambia
              </td>
              <td>
                 28th November 2013
              </td>
              <td>
                Open
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="tab-pane fade" id="phd">
        <h3>Phd</h3>
        <p>
           Raw denim you probably haven't heard of them jean shorts Austin. 
        </p>
        <table class="table table-bordered"> 
          <thead>
            <tr>
              <th>
                 #
              </th>
              <th>
                 Scholarship
              </th>
              <th>
                 Details
              </th>
              <th>
                 Country
              </th> 
              <th>
                 Dead-Line
              </th>
              <th>
                 Status
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                 1
              </td>
              <td>
                Gambia Government UTG
              </td>
              <td>
                 Raw denim you probably haven't heard of them jean shorts Austin. 
              </td>
              <td>
                The Gambia
              </td>
              <td>
                 28th November 2013
              </td>
              <td>
                Open
              </td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
    <!-- END Tabs -->
              </div>
            </div>
            <div>
              <p class="alert alert-info">INTERVIEWS</p>
              <div>
        <table class="table table-bordered"> 
          <thead>
            <tr>
              <th>
                 #
              </th>
              <th>
                 Scholarship
              </th>
              <th>
                 Details
              </th>
              <th>
                 Country
              </th> 
              <th>
                 Dead-Line
              </th>
              <th>
                 Status
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                 1
              </td>
              <td>
                Gambia Government UTG
              </td>
              <td>
                 Raw denim you probably haven't heard of them jean shorts Austin. 
              </td>
              <td>
                The Gambia
              </td>
              <td>
                 28th November 2013
              </td>
              <td>
                Open
              </td>
            </tr>
          </tbody>
        </table>
              </div>
            </div>
            <div>
              <p class="alert alert-warning">SCHOLARSHIPS AWARDS</p>
              <div>
        <table class="table table-bordered"> 
          <thead>
            <tr>
              <th>
                 #
              </th>
              <th>
                 Scholarship
              </th>
              <th>
                 Details
              </th>
              <th>
                 Country
              </th> 
              <th>
                 Dead-Line
              </th>
              <th>
                 Status
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                 1
              </td>
              <td>
                Gambia Government UTG
              </td>
              <td>
                 Raw denim you probably haven't heard of them jean shorts Austin. 
              </td>
              <td>
                The Gambia
              </td>
              <td>
                 28th November 2013
              </td>
              <td>
                Open
              </td>
            </tr>
          </tbody>
        </table>
              </div>
            </div>
</div>

<div class="cft">
	
</div>

@stop