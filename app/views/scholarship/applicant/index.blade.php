@section('scope')
	@include('_partials/scope')	  
@stop
@section('container')
<div class="chd">
	
</div>
<div class="cc">
<?php 
 $image = '';
	$documents = array();
	$Academics = array();
	$bio = array();
	$work = array();
	$scholarship = array();

	foreach ($data as $key => $value) {

		if ($value) {
			foreach ($value as $key1 => $value1) {
				if ($key == 'details') {
					if ($key1 == 'documents') {
						 $documents[] = $value1['documents'];
						foreach ($value1['documents'] as $key2 => $value2) {
							if ($value2['type'] == 'photo') {
								$image .= $value2['foldername'].'/'.$value2['rename'];
							}
						}
					}
					$bio = $value1;
				}


				if ($key == 'academics') {
					$Academics[] = $value1;
				}


				if ($key == 'works') {
					$work = $value1;				
				}


				if ($key == 'scholarships') {
					$scholarship = $value1;		
				}							


			}
		}
}

?>
 <!-- Content sectio begins here
    ================================================== -->
    <div class="container">
    <div class="row">
        <div class="col-md-3">
          <div class="personal-details">
            <p>
	            	@if($image)
		           	{{
		           		HTML::image('/media/scholarship/documents/'.$image)
		           	}}
		           	@endif

            
            </p>
            <strong>{{$bio['fullname']}}</strong>
            <p>Student</p>
          </div>
        </div>
        <div class="col-md-9">
             <ul class="nav nav-tabs" id="myTab">
              <li class="active"><a href="#home" data-toggle="tab">Details</a></li>
              <li class=""><a href="#aca" data-toggle="tab">Academics</a></li>
              <li class=""><a href="#sch" data-toggle="tab">Scholarship</a></li>
              <li class=""><a href="#doc" data-toggle="tab">Documents</a></li>
              <!-- <li class=""><a href="#doc" data-toggle="tab">Extras</a></li> -->
            </ul>

            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade active in" id="home">
                <h4>Personal Details</h4>
                <!-- <form class="form-horizontal" role="form"> -->
                {{ Form::model($bio, array('route' => array('scholarship.applicant.edit', $bio['id']),'method'=>'post')) }}
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Full Name: </label>
                      <div class="col-lg-10">
                         <!-- <input type="text" class="form-control" value="Mamadou Jallow"> -->
                         {{Form::text('fullname')}}
                      </div>
                    </div> 

                   <div class="form-group">
                      <label class="col-lg-2 control-label">Date of Birth: </label>
                      <div class="col-lg-10">
                         <!-- <input type="date" class="form-control" value=""> -->
                         {{Form::text('dob')}}
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Gender: </label>
                      <div class="col-lg-10">
                         <!-- <input type="text" class="form-control" value="Male"> -->
                         {{Form::text('gender')}}

                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Nationality: </label>
                      <div class="col-lg-10">
                         <!-- <input type="text" class="form-control" value="Gambian"> -->
                         {{Form::text('Nationality')}}

                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Phone: </label>
                      <div class="col-lg-10">
                         <!-- <input type="number" class="form-control" value="6677578">phone -->
                         {{Form::text('phone')}}
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Email: </label>
                      <div class="col-lg-10">
                         <!-- <input type="email" class="form-control" value="yiramang@gmail.com"> -->
                         {{Form::text('email')}}
                      </div>
                    </div> 

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Address: </label>
                      <div class="col-lg-10">
                         <!-- <textarea class="form-control" rows="5"></textarea> -->
                         {{Form::textarea('address')}}

                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-10">
                         <!-- <button type="submit" class="btn btn-primary">Save Edit</button> -->
                         <!-- {{Form::submit('Save Edit',array('class'=>'btn btn-primary'))}} -->
                         <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                      </div>
                    </div> 
                 
                {{Form::close()}}
              </div>

              <div class="tab-pane fade" id="aca">
                <h4>Academics Details</h4>
               
                <table class="table">
                	<thead>
                		<th>id</th>
                		<th>school name</th>
                		<th>certification</th>
                	</thead>
                	<tbody>
		                <?php foreach ($Academics as $key => $value): ?>
		             		<tr>
		             			<td>{{$key + 1}}</td>
		             			<td>{{$value->schoolname}}</td>
		             			<td>{{$value->schoolcertification}}</td>
		             		</tr>
		             	<?php endforeach ?>
                	</tbody>
                </table>
              </div>
              <div class="tab-pane fade" id="sch">
                <h4>Scholarship Details</h4>


                 <!-- <form class="form-horizontal" role="form"> -->
                {{ Form::model($scholarship, array('route' => array('scholarship.applicant.edit', ),'method'=>'post')) }}
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Award name: </label>
                      <div class="col-lg-10">
                         <!-- <input type="text" class="form-control" value="Gambia Government Scholarship"> -->
	                          {{Form::text('award')}}

                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Field of Study: </label>
                      <div class="col-lg-10">
                         <!-- <input type="text" class="form-control" value="Computer Science"> -->
                         {{Form::text('filedofstudy')}}
                      </div>
                      <label class="col-lg-2 control-label">Level: </label>
                      <div class="col-lg-10">
                         <!-- <input type="text" class="form-control" value="Bachelors"> -->
                         {{Form::text('leveofstudy')}}

                      </div>
                    </div> 


                     <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-10">
                       <!-- {{Form::submit('Save Edit',array('class'=>'btn btn-primary'))}} -->
                         <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                         <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                      </div>
                    </div> 
                 
                <!-- </form> -->
                        

                {{Form::close()}}

             
              </div>
              
               <div class="tab-pane fade" id="doc">
                <h4>Documents Details</h4>
                @foreach($documents as $key => $value)
                	<?php foreach ($value as $key1 => $value2): ?>
                  	<strong>{{$value2['type']}}</strong>
                  	@if($value2['rename'])
                   <div class="form-group">
                      <label class="col-lg-2 control-label">{{$value2['type']}} : </label>
                      {{HTML::link('/media/scholarship/documents/'.$value2['foldername'].'/'.$value2['rename'],$value2['type'].' download')}} |
                    </div> 
                    <hr>                    	
                  	@endif               		
                	<?php endforeach ?>
                @endforeach
              </div>
          </div>
        </div>

    </div><!-- end of grid fluid -->

     <hr class="featurette-divider">  

    </div>


</div> <!-- end of cc -->

<div class="cft">
	
</div>

@stop