@section('container')
<div class="chd">
	<h3>Register as a researcher</h3>
	<hr>
	<div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
	</div>
	<hr>
</div>
<div class="cc">
    <!-- Content sectio begins here
    ================================================== -->
    <div class="container content">
    <ul class="breadcrumb" style="margin-bottom: 5px;">
      <li>
      <a href="#">Home</a>
      <li class="active">Register as a Researcher</li>
    </ul>
    <section class="page-header">
      <h1 class="title stressborder">Register as a Researcher</h1>
    </section>

    <div class="row">
        <div class="col-md-7">
         @include('_partials.errors')  
         @if(Session::has('success')) 
          <h3 class="text-success">
            You have successfully uploaded: {{Session::get('success')}}
          </h3>
         @endif
         {{Form::open(['route'=>'research.register.store','files' => true]);}}
        <p>Please Register below, if you are already registered please <a href="login.php">Login</a></p>
        <fieldset>
        <div class="form-group">
          <label for="fullname">Full Name</label>
          <input type="text" class="form-control" id="fullname" placeholder="Enter Full name" name="fullname">
          <p class="help-block">Please the name that is on your documents</p>
        </div> 

        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Choose a Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
        </div> 
<!-- 
        <div class="form-group">
          <label for="exampleInputPassword2">Confirm Password</label>
          <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" name="pwd">
        </div> -->
        <div class="form-group">
          <label for="exampleInputFile">Passport Photo</label>
          <input type="file" id="exampleInputFile" name="photo">
          <p class="help-block">Please upload a recent passport photograph</p>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox"> I have agreed to the <a href="#">terms and conditions</a>
          </label>
        </div>
        </fieldset>
        <fieldset>
           <input type="hidden" name="formType" value="register"/>
          <button type="submit" class="btn btn-primary">Register</button>
          <button type="submit" class="btn btn-default">Cancel</button>
        </fieldset>
      {{Form::close()}}
        </div>

        
    </div><!-- end of grid fluid -->

     <hr class="featurette-divider">  

    </div>  
</div>
<div class="cft">
</div><!-- content footer -->
@stop

