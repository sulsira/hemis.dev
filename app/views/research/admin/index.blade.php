@extends('research.master')

@section('container')
<div class="chd">
  <h3>Researchers</h3>
  <hr>
  <div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
  </div>
</div>
<div class="cc">
    <div class="row">
        <div class="col-md-7">     
      <hr>
        <p>Please Upload document below</p>
        <fieldset>
        <div class="form-group">
          <label for="fullname">Your Name is : </label>
        </div> 
        <hr>
        <div class="form-group">
          <label for="exampleInputEmail1">Telephone/Mobile Number</label>
          <input type="email" class="form-control" name="email" placeholder="Enter email">
          <p class="help-block">Numbers only allowed <a href="#">help me</a></p>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Address</label>
          <textarea class="form-control" rows="3" id="exampleInputPassword1" placeholder="Topic of focus" name="focus">
            
          </textarea> 
          <p class="help-block">Maximum character is 200 <a href="#">help me</a></p>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Qualification</label>
          <input type="text" class="form-control"  placeholder="Your Publication Title" name="title">
          <a href="#">add more</a>
          <p class="help-block">Maximum character is 128 <a href="#">help me</a></p>
        </div> 
        <hr>
        <div class="form-group">
          <label for="exampleInputFile">Upload C.V(curriculum vitae)/Resume</label>
           <input type="file" name="userfile" size="20" />
          <p class="help-block">512MB only , PDF only allow (required field) <a href="#">help me</a></p>
        </div>
        </fieldset>
        <fieldset>
          <button type="submit" class="btn btn-primary">SEND</button>
          <button type="reset" class="btn btn-default">Cancel</button>
        </fieldset>
      </form>
        </div>

    </div><!-- end of grid fluid -->

</div>
<div class="cft">

</div><!-- content footer -->
@stop
