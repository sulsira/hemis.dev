@section('container')
<div class="chd">
  <h3>Research publications</h3>
  <hr>
  <div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
  </div>
</div>
<div class="cc">
    <!-- Content sectio begins here
    ================================================== -->
      <div class="row">
        <?php foreach ($data as $key => $value): ?>
          <?php $document = $value['document']->toArray(); ?>

                 <div class="col-md-3 left0">
                  <div class="thumbnail">
                    <div class="caption">
                      <h4><?php echo (!empty($value['publication']['title'])) ? e($value['publication']['title']) : 'No Title'; ?></h4>

                      <span class="primarycol">
                        - 
                          <?php echo (!empty($value['publication']['researcher']['fullname'])) ? e(ucwords($value['publication']['researcher']['fullname'])) : 'No Name'; ?>
                        -
                     </span>
                      <p>
                        <?php echo (!empty($value['publication']['abstract'])) ? e($value['publication']['abstract']) : 'No Abstract'; ?>
                      </p>
                      <?php if (!empty($document)): ?>
                        <?php if ($value['publication']['pub_view'] == 'public'): ?>
                          <p>
                            <a href="#">Download</a> ∣
                            <a href="#">View</a>
                          </p> 
                          <?php else: ?>
                               <p>
                                 <span>This is private publication </span> ∣
                                 <a href="">view author</a>
                               </p>                 
                        <?php endif ?>   
                        <?php else: ?>
                          <p>
                            <strong>Sorry there was no publication document uploaded for this research</strong>
                          </p>                     
                      <?php endif ?>


                    </div>
                  </div>
                </div>
        <?php endforeach ?>

                <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

            </div>
        
    </div><!-- end of grid fluid -->

     <hr class="featurette-divider"> 


        <p>
          Publications in alphabetical order. please click on to expand
        </p>
        <!-- Accordion 
      ================================================== -->
        <div class="accordion">

          <h3 class=""><span>A All Publications that start with A</span></h3>
          <div style="display: none;" class="accord_cont">
            

            <div class="row">
             
                 <div class="col-md-3 left0">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>
                
            </div>
          </div>


          <h3 class=""><span>B All Publications that start with B</span></h3>
          <div style="display: none;" class="accord_cont">
            <div class="row">
             
                 <div class="col-md-3 left0">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>
                
            </div>
          </div>


          <h3 class=""><span>C All Publications that start with C</span></h3>
          <div style="display: none;" class="accord_cont">
            <div class="row">
             
                 <div class="col-md-3 left0">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>
                
            </div>
          </div>


          <h3 class=""><span>D All Publications that start with D</span></h3>
          <div style="display: none;" class="accord_cont">
            <div class="row">
             
                 <div class="col-md-3 left0">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>
                
            </div>
          </div>


          <h3 class=""><span>E All Publications that start with E</span></h3>
          <div style="display: none;" class="accord_cont">
            <div class="row">
             
                 <div class="col-md-3 left0">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>

                 <div class="col-md-3">
                  <div class="thumbnail">
                    <img src="__public/img/index.png"  alt="Publications name here" />
                    <div class="caption">
                      <h4>Publications name will come here like so</h4>
                      <span class="primarycol">- Name of the Author -</span>
                      <p>
                        <a href="#">Abstract</a>
                      </p>
                    </div>
                  </div>
                </div>
                
            </div>
          </div>
        </div>
        <!-- END Accordion --> 
</div>
<div class="cft">

</div><!-- content footer -->
@stop

