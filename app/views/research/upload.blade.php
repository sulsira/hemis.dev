@section('container')
<div class="chd">
  <h3>Upload a research finding</h3>
  <hr>
  <div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
  </div>
</div>
<div class="cc">
    <div class="row">
     
        <div class="col-md-7">  
         @include('_partials.errors')  
         @if(Session::has('success')) 
          <h3 class="text-success">
            You have successfully uploaded: {{Session::get('success')}}
          </h3>
         @endif
        <p>Please fill the form below and upload a pdf file of your research</p>
        {{Form::open(['route'=>'research.upload.store','files' => true]);}}
        <fieldset>
        <div class="form-group">
          <label for="fullname">Name</label>
          <input type="text" class="form-control" name="fullname" placeholder="Enter Full name">
          <p class="help-block">Please enter your full name <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="write your fullname">Help tips</a></p>
        </div> 

        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" name="email" placeholder="Enter email">
          <p class="help-block">Enter email address  <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="enter a correct email address. example: sulsira@hotmail.com">Help tips</a></p>

        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Publication Title</label>
          <input type="text" class="form-control"  placeholder="Your Publication Title" name="publtitle">
          <p class="help-block">Enter email address  <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="enter the title of your research finding or in the way that you want other people to see it">Help tips</a></p>
        </div> 

        <div class="form-group">
          <label for="exampleInputPassword1">Abstract</label>
          <textarea class="form-control" rows="3" id="exampleInputPassword1" placeholder="Topic of focus" name="abstract">
            
          </textarea> 
<p class="help-block">Abstract of your publication <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="Abstract of your publication">Help tips</a></p>
        </div> 

        <div class="form-group">
          <label for="exampleInputPassword1">Type of publication</label>
            <select name="publtype" data-input="" id="" class="form-control">
              <option value="book">A book</option>
              <option value="scholarly">Scholarly journal articles</option>
              <option value="peer">peer reviewed</option>
              <option value="not-peer">not-peer reviewed</option>
              <option>others</option>
            </select>
<p class="help-block">
  Select a publication 
  <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="Select a publication ">Help tips</a>
</p>


        </div> 

        <div class="form-group">
          <label for="exampleInputPassword1">Discipline</label>
            <select name="discipline" data-input="" id="" class="form-control">
              <optgroup label="UNESCO">
                  <option>Agriculture & Forestry</option>
                  <option>Aquatic Resources</option>
                  <option>Bio-based Industries</option>
                  <option>Biotechnology</option>
                  <option>Energy</option>
                  <option>Environment & Climate Action</option>
                  <option>Food & Healthy Diet</option>
                  <option>Funding Researchers</option>
                  <option>Health</option>
                  <option>ICT Research & Innovation</option>
                  <option>Innovation</option>
                  <option>International Cooperation</option>
                  <option>Key Enabling Technologies</option>
                  <option>Partnerships with Industry and Member States</option>
                  <option>Raw Materials</option>
                  <option>Research Infrastructures</option>
                  <option>Security</option>
                  <option>SMEs</option>
                  <option>Social Sciences & Humanities</option>
                  <option>Society</option>
                  <option>Space</option>
                  <option>Transport</option>
              </optgroup>           
            </select>

<p class="help-block">
  Select a discipline 
  <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="Select a publication discipline ">help tip </a>
</p>
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">View</label>
            <select name="view" data-input="" id="" class="form-control">
              <option>public</option>
              <option>private</option>
            </select>
            <p class="help-block">
              Please upload a document to MOHERST 
              <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="who would you like to see your research ">help tip </a>
            </p>
            <hr>
        </div> 

        <div class="form-group">
          <label for="exampleInputFile">Publication Document</label>
           <input type="file" name="userfile"/>
            <p class="help-block">
              Please upload a document to MOHERST (required field)
              <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="the file/document you upload should be pdf and less that 1mb. this is field is required for you to complete.">help tip </a>
            </p>
        </div>

        </fieldset>
        <fieldset>
          <button type="submit" class="btn btn-primary">SEND</button>
          <button type="reset" class="btn btn-default">Cancel</button>
        </fieldset>
      {{Form::close()}}
        </div>

    </div><!-- end of grid fluid -->

</div>
<div class="cft">

</div><!-- content footer -->
@stop

