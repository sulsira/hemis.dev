@section('container')
<div class="chd">
  <h3>Research publications</h3>
  <hr>
  <div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
  </div>
</div>
<div class="cc">
    <!-- Content sectio begins here
    ================================================== -->
       <!-- <hr class="featurette-divider">  -->
       <?php if (!empty($data)): ?>
            <p>
              Publications in alphabetical order. please click on to expand
            </p>
            <!-- Accordion 
          ================================================== -->
            <div class="accordion">
            <?php foreach ($data as $key => $value): ?>
              <?php if ($key == 'registered'): ?>
                <h3 class=""><span>All Publications with register authors</span></h3>                       
                    <div style="display: none;" class="accord_cont">
                    <div class="row">
                      <?php foreach ($value as $key1 => $value1): ?>
                        <?php $document = $value1['document']->toArray(); ?>
                               <div class="col-md-3 left0">
                                <div class="thumbnail">
                                  <div class="caption">
                                    <h4><?php echo (!empty($value1['publication']['title'])) ? e($value1['publication']['title']) : 'No Title'; ?></h4>

                                    <span class="primarycol">
                                      - 
                                        <?php echo (!empty($value1['publication']['researcher']['fullname'])) ? e(ucwords($value1['publication']['researcher']['fullname'])) : 'No Name'; ?>
                                      -
                                   </span>
                                    <p class="abstract">
                                    {{$value1['publication']['pub_abstract']}}
                                      <?php echo (!empty($value1['publication']['pub_abstract'])) ? e($value1['publication']['pub_abstract']) : 'No Abstract'; ?>
                                    </p>
                                    <?php if (!empty($document)): ?>
                                      <?php if ($value1['publication']['pub_view'] == 'public'): ?>
                                        <p>
                                          <?php if ($document[0]['extension'] != 'pdf'): ?>                                          
                                            {{HTML::link('research/download?file=', 'Download')}}
                                          <?php else: ?> 

                                            {{HTML::link('research/download?file='.urlencode($document[0]['fullpath']),'Download',['target'=>'_blank'])}}
                                            ∣
                                            {{HTML::link('research/view?file='.urlencode($document[0]['foldername'].$document[0]['filename']),'View',['target'=>'_blank'])}}
                                          <?php endif ?>                                                                                    
                                        </p> 
                                        <?php else: ?>
                                             <p>
                                               <span>This is private publication </span> ∣
                                               <a href="">view author</a>
                                             </p>                 
                                      <?php endif ?>   
                                      <?php else: ?>
                                        <p>
                                          <strong>no publication document uploaded</strong>
                                        </p>                     
                                    <?php endif ?>


                                  </div>
                                </div>
                              </div>
                      <?php endforeach ?>
                       <!-- dynamic processing -->

                    </div>
                          
                      </div>
                    

                <?php else: ?>
              <h3 class=""><span>All Publications for un registered authors</span></h3>
              <div style="display: none;" class="accord_cont">
                <div class="row">
                 
                      <?php foreach ($value as $key1 => $value1): ?>
                        <?php $document = $value1['document']->toArray(); ?>

                               <div class="col-md-3 left0">
                                <div class="thumbnail">
                                  <div class="caption">
                                    <h4><?php echo (!empty($value1['publication']['title'])) ? e($value1['publication']['title']) : 'No Title'; ?></h4>

                                    <span class="primarycol">
                                      - 
                                        <?php echo (!empty($value1['publication']['researcher']['fullname'])) ? e(ucwords($value1['publication']['researcher']['fullname'])) : 'No Name'; ?>
                                      -
                                   </span>
                                    <p class="abstract">
                                      <?php echo (!empty($value1['publication']['abstract'])) ? e($value1['publication']['abstract']) : 'No Abstract'; ?>
                                    </p>

                                    <?php if (!empty($document)): ?>
                                      <?php if ($value1['publication']['pub_view'] == 'public'): ?>
                                        <p>
                                          <?php if ($document[0]['extension'] != 'pdf'): ?>                                          
                                            {{HTML::link('research/download?file=', 'Download')}}
                                          <?php else: ?> 

                                            {{HTML::link('research/download?file='.urlencode($document[0]['fullpath']),'Download',['target'=>'_blank'])}}
                                            ∣
                                            {{HTML::link('research/view?file='.urlencode($document[0]['foldername'].$document[0]['filename']),'View',['target'=>'_blank'])}}
                                          <?php endif ?>                                                                                    
                                        </p> 
                                        <?php else: ?>
                                             <p>
                                               <span>This is private publication </span> ∣
                                               <a href="">view author</a>
                                             </p>                 
                                      <?php endif ?>   
                                      <?php else: ?>
                                        <p>
                                          <strong>no publication document uploaded</strong>
                                        </p>                     
                                    <?php endif ?>


                                  </div>
                                </div>
                              </div>
                      <?php endforeach ?>
                    
                </div>
              </div>

              <?php endif ?>
            <?php endforeach ?>
            <!-- END Accordion --> 
            <?php else: ?>
            <h1>THERE ARE NO RESEARCH FINDIG SUBMIT YET!</h1>         
       <?php endif ?>

        
</div><!-- end of grid fluid -->

</div>
<div class="cft">

</div><!-- content footer -->
@stop