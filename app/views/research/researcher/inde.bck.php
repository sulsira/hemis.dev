@extends('research.master')

@section('container')
<div class="chd">
  <h3>-Name of Reasearcher-</h3>
  <hr>
  <div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
  </div>
</div>
<div class="cc">
    <div class="container content">
    <ul class="breadcrumb" style="margin-bottom: 5px;">
      <li>
      <a href="#">Home</a>
      </li>
      <li class="active">Profile</li>
    </ul>



     <hr class="featurette-divider"> 

    <div class="row">

        <div class="col-md-3">
          <div class="personal-details">
            <p>
            <img class="img-circle" src="__public/img/abddev.jpg"  alt="Profile name here" />
            </p>
            <strong>Abd Al-Ala Camara</strong>
            <p>Student</p>
          </div>
        </div>

        <div class="col-md-9">
             <ul class="nav nav-tabs" id="myTab">
              <li class="active"><a href="#home" data-toggle="tab">Details</a></li>
            </ul>

            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade active in" id="home">
                <h4>Personal Details</h4>
                <form class="form-horizontal" role="form">
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Full Name: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Mamadou Jallow">
                      </div>
                    </div> 

                   <div class="form-group">
                      <label class="col-lg-2 control-label">Date of Birth: </label>
                      <div class="col-lg-10">
                         <input type="date" class="form-control" value="">
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Gender: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Male">
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Nationality: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Gambian">
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Phone: </label>
                      <div class="col-lg-10">
                         <input type="number" class="form-control" value="6677578">
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Email: </label>
                      <div class="col-lg-10">
                         <input type="email" class="form-control" value="yiramang@gmail.com">
                      </div>
                    </div> 

                    <div class="form-group">
                      <label class="col-lg-2 control-label">Address: </label>
                      <div class="col-lg-10">
                         <textarea class="form-control" rows="5"></textarea>
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-10">
                         <button type="submit" class="btn btn-primary">Save</button>
                         <button type="submit" class="btn btn-default">Cancel</button>
                      </div>
                    </div> 
                 
                </form>
              </div>

              <div class="tab-pane fade" id="aca">
                <h4>Academics Details</h4>
                 <form class="form-horizontal" role="form">
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Institution: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="University of the Gambia">
                      </div>
                      <label class="col-lg-2 control-label">Qualification: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Bachelors">
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Institution: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="University of the Gambia">
                      </div>
                      <label class="col-lg-2 control-label">Qualification: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Bachelors">
                      </div>
                    </div> 


                     <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-10">
                         <button type="submit" class="btn btn-primary">Save</button>
                         <button type="submit" class="btn btn-default">Cancel</button>
                      </div>
                    </div> 
                 
                </form>
             
              </div>
              <div class="tab-pane fade" id="sch">
                <h4>Scholarship Details</h4>
                 <form class="form-horizontal" role="form">
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Award name: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Gambia Government Scholarship">
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Field of Study: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Computer Science">
                      </div>
                      <label class="col-lg-2 control-label">Level: </label>
                      <div class="col-lg-10">
                         <input type="text" class="form-control" value="Bachelors">
                      </div>
                    </div> 


                     <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-10">
                         <button type="submit" class="btn btn-primary">Save</button>
                         <button type="submit" class="btn btn-default">Cancel</button>
                      </div>
                    </div> 
                 
                </form>
             
              </div>
              
               <div class="tab-pane fade" id="doc">
                <h4>Documents Details</h4>
                 <form class="form-horizontal" role="form">
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Birth Certificate: </label>
                      <div class="col-lg-10">
                        <p>Birth Certificate</p>
                        <input type="file" id="birth">
                        <p class="help-block">update birth certificate</p>
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label">Transcript: </label>
                      <div class="col-lg-10">
                        <p>Transcript</p>
                        <input type="file" id="trans">
                        <p class="help-block">update Transcript</p>
                      </div>
                    </div> 

                     <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-10">
                         <button type="submit" class="btn btn-primary">Save</button>
                         <button type="submit" class="btn btn-default">Cancel</button>
                      </div>
                    </div> 
                 
                </form>
              </div>
          </div>
        </div>

    </div><!-- end of grid fluid -->

     <hr class="featurette-divider">  

    </div>

</div>
<div class="cft">

</div><!-- content footer -->
@stop
