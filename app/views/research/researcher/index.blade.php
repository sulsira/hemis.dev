@include('templates/research')
      <?php 
        $userType = Session::get('userType');
        $userDomain = Session::get('userDomain');
        $userGroup = Session::get('userGroup');
        $privileges = Session::get('privileges');
        $document = array();
        $photo = array();
        $researcher = array();
        $cv = array();
        foreach ($data as $key => $value) {
           $researcher = $value['researcher'];
           foreach ($value['document'] as $key1 => $value1) {
            if (  $value1['type']  == 'photo') {
              $photo = $value1;
            }else if($value1['type']  == 'cv'){
              $cv = $value1;
            }else{
                 $document = $value1;
            }
           }
        }
       ?>
    </div>
  </div>
  <div class="cc">
      <div class="container content">

      <ul class="breadcrumb" style="margin-bottom: 5px;">
        <li>
        <a href="#">profile</a>
        </li>
        <li class="active">Researcher</li>
      </ul>



       <hr class="featurette-divider"> 
   

      <div class="row">

          <div class="col-md-3">
            <div class="personal-details">
              <p>
              <a href="#" class="thumbnail disabled">
                    <?php if (!empty($photo)): ?>
                      {{HTML::image($photo['thumnaildir'].$photo['filename'],'researchers photo', ['title'=>$photo['entity_type']])}}
                     <?php else: ?>
                      {{HTML::image('imgs'.DS.'user.png','researchers photo', ['title'=>'no research photo'])}}
                    <?php endif ?>
              </a>
             
              </p>
              <strong> {{ucwords($researcher['fullname'] )}}</strong>
              <p>Registered:: researcher</p>
              <div>
              <?php if (!empty($cv)): ?>

                  <p>
                  <strong>C.V :</strong>
                    <?php if ($cv['extension'] != 'pdf'): ?>                                          
                      {{HTML::link('research/download?file='.urlencode($cv['fullpath']),'Download',['target'=>'_blank'])}}
                    <?php else: ?> 

                      {{HTML::link('research/download?file='.urlencode($cv['fullpath']),'Download',['target'=>'_blank'])}}
                      ∣
                      {{HTML::link('research/view?file='.urlencode($cv['foldername'].$cv['filename']),'View',['target'=>'_blank'])}}
                    <?php endif ?>
                  </p> 

                  <?php else: ?>
                       <p>
                         <span>No C.V</span> ∣
                       </p>                 
                <?php endif ?> 
              </div>
            </div>
          </div>

          <div class="col-md-9">
              <ul class="nav nav-tabs nav-justified" id="myTab">
                <li class="active"><a href="#profile" data-toggle="tab">Details</a></li>
                <li><a href="#pub" data-toggle="tab">publications</a></li>
              </ul>
              <hr class="featurette-divider"> 
              <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="profile">
                  <h4>Personal Details</h4>
                     <div class="form-group">
                        <label class="col-lg-2 control-label">Full Name: </label>
                        <div class="col-lg-10">
                           <p class="form-control" > {{$researcher['fullname'] }}</p>
                        </div>
                      </div> 

                       <div class="form-group">
                        <label class="col-lg-2 control-label">Phone: </label>
                        <div class="col-lg-10">
                           <p  class="form-control" >{{$researcher['phone'] }} </p>
                        </div>
                      </div> 

                       <div class="form-group">
                        <label class="col-lg-2 control-label">Email: </label>
                        <div class="col-lg-10">
                           <p  class="form-control" > {{$researcher['email'] }} </p>
                        </div>
                      </div> 

                      <div class="form-group">
                        <label class="col-lg-2 control-label">Address: </label>
                        <div class="col-lg-10">
                           <p class="form-control" rows="5">{{e($researcher['address']) }}</p>
                        </div>
                      </div> 

                       <div class="form-group">
                        <label class="col-lg-2 control-label">Qualification: </label>
                        <div class="col-lg-10">
                           <p class="form-control" rows="5">{{e($researcher['qualification']) }}</p>
                        </div>
                      </div> 


                </div>

                <div class="tab-pane fade" id="pub">
                  <h4>Publications</h4>
                    <hr>
                    @foreach($data as $key => $value)
                      @if(!empty($value['publication']))

                        @foreach($value['publication'] as $k=>$v)
                                 <div class="col-md-3 left0">
                                  <div class="thumbnail">
                                    <div class="caption">

                                      <h4><?php echo (!empty($v['detail']['title'])) ? e($v['detail']['title']) : 'No Title'; ?></h4>

                                      <span class="primarycol">
                                        - 
                                          Abstract
                                        -
                                     </span>
                                      <p class="abstract">
                                        <?php echo (!empty($v['detail']['pub_abstract'])) ? e($v['detail']['pub_abstract']) : 'No Abstract'; ?>
                                      </p>
                                      <?php if (!empty($v['detail']['document'])): ?>
                                          <?php foreach ($v['detail']['document'] as $ky => $val): ?>
                                            <p>
                                              <?php if ($val['extension'] != 'pdf'): ?>                                          
                                                {{HTML::link('research/download?file=', 'Download')}}
                                              <?php else: ?> 

                                                {{HTML::link('research/download?file='.urlencode($val['fullpath']),'Download',['target'=>'_blank'])}}
                                                ∣
                                                {{HTML::link('research/view?file='.urlencode($val['foldername'].$val['filename']),'View',['target'=>'_blank'])}}
                                              <?php endif ?>                                                                                    
                                            </p>                                           
                                          <?php endforeach ?>
   
                                        <?php else: ?>
                                          <p>
                                            <strong>no publication document uploaded</strong>
                                          </p>                     
                                      <?php endif ?>


                                    </div>
                                  </div>
                                </div>
                        @endforeach
                       @else
                        <h3>there are no publications yet!</h3>
                      @endif

                    @endforeach
                </div>
   
                
    

            </div>
          </div>

      </div><!-- end of grid fluid -->

       <hr class="featurette-divider">  

      </div>
  </div>
  <div class="cft">

  </div><!-- content footer -->
@stop
