@extends('research.master')

@section('container')
<div class="chd">
  <h3>Researchers</h3>
  <hr>
  <div class="cht">
    <?php 
      $user = Auth::user();
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
      
     ?>
  </div>
</div>
<div class="cc">
    <div class="row">
        <div class="col-md-7">     
      <hr>
        <p>Please Upload document below</p>
        <fieldset>
        <div class="form-group">
          <label for="fullname">Your Name is : {{e($user->fullname)}}</label>
        </div> 
        <hr>
        <div class="col-md-7">
         @include('_partials.errors')  
         @if(Session::has('success')) 
          <h3 class="text-success">
            You have successfully uploaded: {{Session::get('success')}}
          </h3>
         @endif
         {{Form::open(['route'=>'research.researcher.store','files' => true]);}}
        <div class="form-group">
          <label for="exampleInputEmail1">Telephone/Mobile Number</label>
          <input type="text" class="form-control" name="phone" placeholder="Enter telephone number">
          <p class="help-block">
              numbers only allowed
             <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="EXAMPLE : 7052217">Help tips</a>
           </p>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Address</label>
          <textarea class="form-control" rows="3" id="exampleInputPassword1" placeholder="Topic of focus" name="address">
            
          </textarea> 
          <p class="help-block">
              Maximum character is 200 
             <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="EXAMPLE : 7052217">Help tips</a>
           </p>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Qualification</label>
          <input type="text" class="form-control"  placeholder="Your Publication Title" name="quali">
          <!-- <a href="#">add more</a> -->
          <p class="help-block">
              Maximum character is 128
             <a href="#" class="tipify" data-toggle="tooltip" data-placement="right"  title="EXAMPLE : 7052217">Help tips</a>
           </p>
        </div> 
        <hr>
        <div class="form-group">
          <label for="exampleInputFile">Upload C.V(curriculum vitae)/Resume</label>
           <input type="file" name="resume" size="20" />
          <p class="help-block">512MB only , PDF only allow (required field) <a href="#">help me</a></p>
        </div>
        </fieldset>
        <fieldset>
          <input type="hidden" name="formType" value="start"/>
          <button type="submit" class="btn btn-primary">SEND</button>
          <button type="reset" class="btn btn-default">Cancel</button>
        </fieldset>
      {{Form::close()}}
        </div>

    </div><!-- end of grid fluid -->

</div>
<div class="cft">

</div><!-- content footer -->
@stop
