@include('templates/research')
      <?php 
        $userType = Session::get('userType');
        $userDomain = Session::get('userDomain');
        $userGroup = Session::get('userGroup');
        $privileges = Session::get('privileges');
        $document = array();
        $photo = array();
        $researcher = array();
        $cv = array();
        foreach ($data as $key => $value) {
           $researcher = $value['researcher'];
           foreach ($value['document'] as $key1 => $value1) {
            if (  $value1['type']  == 'photo') {
              $photo = $value1;
            }else if($value1['type']  == 'cv'){
              $cv = $value1;
            }else{
                 $document = $value1;
            }
           }
        }

       ?>
    </div>
  </div>
  <div class="cc">
                <div>
                  <h4>Publications</h4>
                    <hr>
                    @foreach($data as $key => $value)
                      @if(!empty($value['publication']))

                        @foreach($value['publication'] as $k=>$v)
                                 <div class="col-md-3 left0">
                                  <div class="thumbnail">
                                    <div class="caption">

                                      <h4><?php echo (!empty($v['detail']['title'])) ? e($v['detail']['title']) : 'No Title'; ?></h4>

                                      <span class="primarycol">
                                        - 
                                          Abstract
                                        -
                                     </span>
                                      <p class="abstract">
                                        <?php echo (!empty($v['detail']['pub_abstract'])) ? e($v['detail']['pub_abstract']) : 'No Abstract'; ?>
                                      </p>
                                      <?php if (!empty($v['detail']['document'])): ?>
                                          <?php foreach ($v['detail']['document'] as $ky => $val): ?>
                                            <p>
                                              <?php if ($val['extension'] != 'pdf'): ?>                                          
                                                {{HTML::link('research/download?file=', 'Download')}}
                                              <?php else: ?> 

                                                {{HTML::link('research/download?file='.urlencode($val['fullpath']),'Download',['target'=>'_blank'])}}
                                                ∣
                                                {{HTML::link('research/view?file='.urlencode($val['foldername'].$val['filename']),'View',['target'=>'_blank'])}}
                                              <?php endif ?>                                                                                    
                                            </p>                                           
                                          <?php endforeach ?>
   
                                        <?php else: ?>
                                          <p>
                                            <strong>no publication document uploaded</strong>
                                          </p>                     
                                      <?php endif ?>


                                    </div>
                                  </div>
                                </div>
                        @endforeach
                       @else
                        <h3>there are no publications yet!</h3>
                      @endif

                    @endforeach
                </div>
  </div>
  <div class="cft">

  </div><!-- content footer -->
@stop
