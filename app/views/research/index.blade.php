@section('container')
<div class="chd">
	<h3>Research publications</h3>
	<hr>
	<div class="cht">
    <?php 
      $userType = Session::get('userType');
      $userDomain = Session::get('userDomain');
      $userGroup = Session::get('userGroup');
      $privileges = Session::get('privileges');
     ?>
	</div>
</div>
<div class="cc">
    <!-- Content sectio begins here
    ================================================== -->
       <!-- <hr class="featurette-divider">  -->
       <?php if (!empty($data)): ?>
            <p>
              Publications in alphabetical order. please click on to expand
            </p>
            <!-- Accordion 
          ================================================== -->

            <div class="accordion">
            <?php foreach ($data as $key => $value): ?>
              <?php if ($key == 'registered'): ?>
                <h3 class="active"><span>Registered Researchers</span></h3>                       
                <div style="display: none;" class="accord_cont">
                    <div class="row">
                      <?php foreach ($value as $key1 => $value1): ?>
                        <?php
                          $photo = array();
                          $cv = array();
                         $document = $value1['document']->toArray();
                          foreach ($document as $key => $value) {
                              if ($value['type'] == 'photo') {
                                $photo = $value;
                              }else if($value['type'] == 'cv'){
                                $cv = $value;
                              }
                          }
                          ?>
                        <div class="col-md-3 left0">
                            <div class="thumbnail content">
                              <?php if (!empty($photo)): ?>
                                {{HTML::image($photo['thumnaildir'].$photo['filename'],'researchers photo', ['title'=>$photo['entity_type']])}}
                               <?php else: ?>
                                {{HTML::image('imgs'.DS.'user.png','researchers photo', ['title'=>'no research photo'])}}
                              <?php endif ?>
                             
                                  <div class="caption">
                                    <h4><?php echo (!empty($value1['researcher']['fullname'])) ? e($value1['researcher']['fullname']) : 'No Name'; ?></h4>
                                      <span class="primarycol">
                                        - 
                                          <?php echo (!empty($value1['researcher']['qualification'])) ? e(ucwords($value1['researcher']['qualification'])) : 'No Qualification'; ?>
                                        -
                                      </span>
                                      <p>
                                        {{HTML::linkAsset('research/researchers/'.$value1['researcher']['id'], 'More Details',[])}}
                                      </p>
                                    <?php if (!empty($cv)): ?>

                                        <p>
                                        <strong>C.V :</strong>
                                          <?php if ($cv['extension'] != 'pdf'): ?>                                          
                                            {{HTML::link('research/download?file='.urlencode($cv['fullpath']),'Download',['target'=>'_blank'])}}
                                          <?php else: ?> 

                                            {{HTML::link('research/download?file='.urlencode($cv['fullpath']),'Download',['target'=>'_blank'])}}
                                            ∣
                                            {{HTML::link('research/view?file='.urlencode($cv['foldername'].$cv['filename']),'View',['target'=>'_blank'])}}
                                          <?php endif ?>
                                        </p> 

                                        <?php else: ?>
                                             <p>
                                               <span>No C.V</span> ∣
                                             </p>                 
                                      <?php endif ?> 
                                      <hr>
                                      <?php if (!empty($value1['researcher']['publications'])): ?>
                                        <p>
                                          <strong>publications </strong>&#91; {{count($value1['researcher']['publications'])}} &#93;
                                        </p>                          
                                      <?php endif ?>
                                      <!-- #here                                       -->
                                  </div>
                            </div>
                        </div>
                      <?php endforeach ?>
                  </div>                          
                </div>
                    

                  </div>   
              </div>

              <?php endif ?>
            <?php endforeach ?>
            <!-- END Accordion --> 
            <?php else: ?>
            <h1>THERE ARE NO RESEARCH FINDIG SUBMIT YET!</h1>         
       <?php endif ?>

        
</div><!-- end of grid fluid -->
</div>
<div class="cft">

</div><!-- content footer -->
@stop

