<?php $session = Session::get('userDomain'); ?>
@include('_partials/doc')
	@section('style')
		<!-- {{ HTML::style('assets/dist/css/bootstrap.min.css') }}		 -->
	{{ HTML::style('css/main.css') }}
	<!-- {{ HTML::style('css/.css') }}		  -->
	{{ HTML::style('css/style.css') }}	 
	@show
       
    </head>
    <body>
	    <div class="top">
	    	@include('_partials/menu')
	  		<div class="acctheda  fixheda_style">
				<div class="navbar">
				 	<div class="navbar-inner">
						<div class="hedacont">								 
						    <a class="active brand" href="{{ route('admin') }}">{{ strtoupper($session . ' account ')}}</a>
						    <ul class="nav">
						      <li><a href="{{ route('institution') }} ">Add School</a></li>
						      <li><a href="{{ route('programme') }} ">add programme</a></li>
						    </ul>
							<form class="" action="search.php" method="GET">
								<div class="input-append">
								  <input class="span5" id="appendedInputButtons" type="text" name="q"  placeholder="seach here">
								  <button class="btn" type="submit" name="natural">Search</button>
								  <button class="btn" type="submit" name="index">Extreme Search</button>
								</div>
							</form>
						</div>
					</div>
				</div>						  			
	  		</div>	  		
	  		@yield('scope')		
	    </div>
	    <div class="wrapper">
		    <div class="row content">
		    	@yield('container')
		    </div>	    	
	    </div>

	    <footer>
	    	<div class="hedacont">MOHERST &copy; Copyright 2013</div>
	    	@yield('footer')
	    </footer>
	    	@yield('script')
@include('_partials/footer')
