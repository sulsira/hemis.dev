@section('top')

@stop
@section('container')
<div class="chd">
    <h3>MOHERST SCHOLARSHIP APPLICATION FORM</h3>
  <hr>
  <div class="cht ">

  </div>
  <hr>
</div>
<div class="cc">
<?php $step = strtolower(Request::segment(2)); ?>
  <div class="main col-md-8">
    <div class="content">

      <div class="tabbable tabs-below">
        <ul class="nav nav-tabs nav-justified">
           <li <?php echo ($step == 'step1')? 'class="active"' : 'class="disabled"' ?>><a href="apply/step1" data-toggle="tab">Personal Information</a></li>
           <li <?php echo ($step == 'step2')? 'class="active disabled"' : 'class="disabled"' ?>><a href="apply/step2" data-toggle="tab">Educational Background</a></li>
           <li <?php echo ($step == 'step3')? 'class="active disabled"' : 'class="disabled"' ?>><a href="apply/step3" data-toggle="tab">Work Experience</a></li>
           <li <?php echo ($step == 'step4')? 'class="active disabled"' : 'class="disabled"' ?>><a href="apply/step4" data-toggle="tab">Documents</a></li>
           <li <?php echo ($step == 'step5')? 'class="active disabled"' : 'class="disabled"' ?>><a href="apply/step5" data-toggle="tab">Scholarship Details</a></li>
        </ul>
          <div class="tab-content">
              @if(Request::segment(2))

                @if($step == 'step1')
                  <?php $id = Session::get('id');?>
                  @include('_partials/step/1')
                @elseif($step == 'step2')
                   <?php $id = Session::get('id');?>
                  @include('_partials/step/2')

                @elseif($step == 'step3')
                  <?php $id = Session::get('id');?>
                  @include('_partials/step/3')
  
                @elseif($step == 'step4')
                  <?php $id = Session::get('id');?>
                  @include('_partials/step/4')

                @elseif($step == 'step5')
                <?php $id = Session::get('id');?>
                  @include('_partials/step/5')

                @elseif($step == 'step6')
                  <?php $id = Session::get('id');?>
                  @include('_partials/step/6')

                @endif
              @else

              @endif

          </div>
      </div>
    </div>

  </div>

</div><!-- content conent -->
<div class="cft">
</div><!-- content footer -->

@stop