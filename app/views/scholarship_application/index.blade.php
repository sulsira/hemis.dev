@section('top')

@stop
@section('container')


<div class="chd">
    <h3>Applying for scholarship</h3>
  <hr>
  <div class="cht ">
  <ul class="nav nav-pills">
        <li><a href="#">How to get a code</a></li>
        <li><a href="#">Code sellers</a></li>
        <li><a href="#">Scholarships</a></li>
</ul>
  </div>
  <hr>
    <div> 
       <em>INSTRUCTIONS : </em>please enter your scholarship code exactly as its even to you. example: A1232b should be writen a123b. it should be exactly A1232b.
    </div>
    <hr>
    @if($errors->any())
      <hr>
      <ul class="list-group">
        {{implode('',$errors->all('<li class="list-group-item list-group-item-danger">:message</li>'))}}
      </ul>
    @endif
</div>
<div class="cc">
  <div class="main col-md-8">
      <div class="jumbotron">
      <div class="page-header">
        <h1>Scholarship Code <small>Enter your scholarship code</small></h1>
      </div>

          <div class="row">
            <div class="col-lg-6">
            {{Form::open(['url'=>'apply'])}}
              <div class="input-group">
                <input type="text" class="form-control input-lg" name="code">
                <span class="input-group-btn">
                {{Form::submit('Go!',['class'=>'btn btn-info btn-lg'])}}
                  <!-- <button class="btn btn-info btn-lg" type="button">Go!</button> -->
                </span>
              </div><!-- /input-group -->
              {{Form::close()}}
            </div><!-- /.col-lg-6 -->

          </div><!-- /.row -->

        <!-- <p><a class="btn btn-primary btn-lg" role="button">Learn more</a></p> -->
      </div>

  </div>

</div><!-- content conent -->
<div class="cft">
</div><!-- content footer -->

@stop