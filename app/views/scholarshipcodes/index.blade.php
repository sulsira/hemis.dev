@section('scope')
	@include('_partials/scope')	  
@stop
@section('container')
<div class="chd">
			<nav class="navbar navbar-default" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#">Scholarship Codes</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li class="active"><a href="#">Codes</a></li>
			        <li><a href="{{route('code.create')}}">Generate codes</a></li>
			      </ul>
			      <form class="navbar-form navbar-left" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default">Search</button>
			      </form>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>		
</div>
<div class="cc">
	<div class="content">	
			<table class="table table-hover">
			<caption><strong>Code table</strong></caption>
	 				<thead>	
	 						<th>id</th>
	 						<th>code</th>
	 						<th>status</th>
	 						<th>date created</th>
	 						<th>assigned to</th>
	 						<th>seller</th>
	 						<th>applicant status</th>
	 						<th>actions</th>
	 				</thead>
	 				<tbody>	
	 					<tr>	
	 						<td>1</td>
	 						<td>1</td>
	 						<td>1</td>
	 						<td>1</td>
	 						<td>1</td>
	 						<td>1</td>
	 						<td>1</td>
	 					</tr>
	 				</tbody>
			</table>
	</div>		
</div>

<div class="cft">
		
</div>

@stop