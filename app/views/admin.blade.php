<?php $session = Session::get('userDomain'); ?>
@include('_partials/doc')
	@section('style')
		<!-- {{ HTML::style('assets/dist/css/bootstrap.min.css') }}		 -->
	{{ HTML::style('css/main.css') }}
	<!-- {{ HTML::style('css/.css') }}		  -->
	{{ HTML::style('css/style.css') }}	 
	@show
       
    </head>
    <body>
	    <div class="top">
	    	@include('_partials/menu')
	  		<div class="acctheda  fixheda_style">
				<div class="navbar">
				 	<div class="navbar-inner">
						<div class="hedacont">								 
						    <a class="active brand" href="{{ route('admin') }}">{{ strtoupper($session . ' account ')}}</a>
						    <ul class="nav">
						      <li><a href="{{ route('institution') }} ">Add School</a></li>
						      <!-- <li><a href="{{ route('programme') }} ">add programme</a></li> -->
						    </ul>
						    {{Form::open(array('route'=>'search','method'=>'get'))}}
								<div class="input-append">
									{{ Form::text('q',null,['class'=>'span5','placeholder'=>"seach here",'id'=>"appendedInputButtons",'required'=>1]) }}
									{{ Form::hidden('x',1) }}	
								  	<button class="btn" type="submit" name="type" value="natural">Search</button>
								  	<!-- <button class="btn" type="submit" name="type" value="index">Extreme Search</button> -->
								</div>
							{{Form::close()}}
						</div>
					</div>
				</div>						  			
	  		</div>	  		
	  		@yield('scope')		
	    </div>
	    <div class="wrapper">
		    <div class="row content">
		    	@yield('container')
		    </div>	    	
	    </div>

	    <footer>
	    	<div class="hedacont">MOHERST &copy; Copyright 2013</div>
	    	@yield('footer')
	    </footer>
	    	@yield('script')
@include('_partials/footer')

