<?php
	#View specific processing
		$school = $data['school'];
		$courses = $data['school']['courses'];
		$students = $data['students'];
		$gender = array();
		$stats = array();
	foreach ($students as $key => $value) {
		$gender[$value['Pers_Gender']][] = $value;
	}
// end of specific processing
?>
@include('templates/school')
		<div class="statistics-area">
			<div class="table-age-range">
				<h3>student</h3>
				<table class="table table-bordered student-gender-general">
				  <caption>students by gender</caption>
				  <thead>
				    <tr>
				      <th>gender</th>
				      <th>total</th>
				      <th>percentage</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td>Female</td>
				      <td>
				      	<?php if (!empty($gender['Female'])): ?>
				      		{{count($gender['Female'])}}
				      	<?php else: ?>
				      		-
				      	<?php endif ?>
				      	
				      	</td>
				      <td>-</td>
				    </tr>
				    <tr>
				      <td>Male</td>
				      <td>
				      	<?php if (!empty($gender['Male'])): ?>
				      	{{count($gender['Male'])}}	
				      <?php else: ?>
				      	-
				      	<?php endif ?>
				      	
				      </td>
				      <td>-</td>
				    </tr>
				  </tbody>
				</table>
			</div>

		</div>
		</div><!-- content conent -->
		<div class="cft">
			
		</div><!-- content footer -->
@stop