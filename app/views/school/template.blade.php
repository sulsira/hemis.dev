<?php
	#View specific processing
		$school = $data['school'];
// end of specific processing
$download = Document::whereRaw('entity_type = ? ORDER BY created_at DESC',['MOHERST_template'])->first()->toArray();
?>	
@include('templates/school')
@include('_partials/Mupload')
<div class="upload">
{{
  HTML::link('school/download?file='.$download['fullpath'],'Download',['target'=>'_blank', 'role'=>"button", 'class'=>"btn",'title'=>$download['filename']])}}
  <a href="#myModal" role="button" class="btn" data-toggle="modal">Upload Your File To MoHERST</a>
</div>
 @include('_partials.errors')  
 @if(Session::has('success')) 
  <h3 class="text-success">
    You have successfully uploaded: {{Session::get('success')}}
  </h3>
 @endif
<hr>
<table class="table table-bordered">
  <caption>
	<h3>List of files</h3>
  <thead>
    <tr>
      <th>#</th>
      <th>file name</th>
      <th>file added date</th>
      <th>actions</th>
    </tr>
  </thead>
  <tbody class="entered-data-school">
  	<?php foreach ($data['docs'] as $key => $value): ?>
  	    <tr>
	      <td  class="maked">{{$key + 1}}</td>
	      <td>{{$value['title']}}</td>
	      <td>{{$value['created_at']}}</td>
	      <td class="action-td">
	      {{HTML::link('school/download?file='.urlencode($value['fullpath']),'Download',['target'=>'_blank'])}}
	      </td>
	    </tr>  		
  	<?php endforeach ?>

  </tbody>
</table>
</div><!-- cc content conent -->
<div class="cft">
						
</div><!-- content footer -->
@stop
@section('script')
   {{HTML::script('__public/ASSET_0.2/js/course_modal.js')}}
@stop