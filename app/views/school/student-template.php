<script type="text/x-handlebars-template"  id="templates">
	<table class="table table-bordered pupinfo">
    	<thead>
		    <tr>
		      <th>Full name</th>
		      <th>birthday</th>
		      <th>nationality</th>
		      <th>Course</th>
		      <th>gender</th>
		      <th>last updated</th>
		      <th>quick peek</th>
		    </tr>
    	</thead>
    	<tbody>
            {{#each this}}
	        <tr data-rnum="{{Pers_PersonID}}" class="onmodal" role="button" data-toggle="modal" data-mtype="student">
				<td class="nm maked">{{ Pers_FirstName }} {{ Pers_MiddleName }} {{ Pers_LastName }}</td>
    			<td> {{ Pers_DOB }} </td>
    			<td> {{ Pers_Nationality }} </td>
    			<td>
                    {{#if classes}}
                        {{#each classes}}
                        {{Clas_LocalClassName}}
                        {{/each}}
                    {{else}}
                        N/A
                    {{/if}}      
    			</td>
    			<td>{{Pers_Gender}}</td>
    			<td>{{Pers_UpdateDate}}</td>
    			<td><a href="#"></a></td>
	    	</tr>
            {{/each}}
    	</tbody>
    </table>			  	
</script>