<?php
	#View specific processing
		$school = $data['school'];
// end of specific processing
?>
@include('templates/school')
	<div class="form-snippet">
		{{Form::open(['route'=>'school.students.store'],[],['class'=>'form-snippet'])}}
			<div class="form-header">
				<div class="title">
					<h2>Create A New Student </h2>
				</div>
				<div class="messages">
					 @include('_partials.errors')  
					 @if(Session::has('success')) 
					  <h3 class="text-success">
					    You have successfully uploaded: {{Session::get('success')}}
					  </h3>
					 @endif
				</div>
			</div>
			<div class="level name">
				<div class="first fname">
					{{Form::label('person[Pers_FirstName]','First Name')}}
					{{Form::text('person[Pers_FirstName]',null,['class'=>'input-xlarge','placeholder'=>'Enter your first name'])}}
	    			<p class="help-block">Example</p>
				</div>
				<div class="midle mname">
					{{Form::label('person[Pers_MiddleName]','Middle Name')}}
					{{Form::text('person[Pers_MiddleName]',null,['class'=>'input-xlarge'])}}
					<p class="help-block">Example: Ebirma <em>S.</em> Jallow</p>
				</div>
				<div class="last lname">
					{{Form::label('person[Pers_LastName]','Lamin Name')}}
					{{Form::text('person[Pers_LastName]',null,['class'=>'input-xlarge','placeholder'=>'Enter your last name'])}}
					<p class="help-block">Example: Ebirma S <em>Jallow</em></p>

				</div>
			</div>
			<div class="level details">
			<span>Details</span>
			<hr>
				<div class="first ">
					{{Form::label('person[Pers_DOB]','Birthday')}}
					{{Form::text('person[Pers_DOB]',null,['class'=>'input-xlarge','placeholder'=>'Enter birthday'])}}
					<p class="help-block">Example: 28/1/1988</p>
				</div>
				<div class="midle ">
					{{Form::label('person[Pers_Ethnicity]','Ethniticity')}}
					<select name="person[Pers_Ethnicity]" id="enit" class="input-xlarge">
						<?php $countries = Variable::domain('Pers_Ethnicity')->toArray();  ?>
						@foreach ($countries as $key => $country)
						<option>{{$country['Vari_VariableName']}}</option>
						@endforeach
					</select>
					<p class="help-block">Example: jola</p>
				</div>
				<div class="last ">
					{{Form::label('person[Pers_Nationality]','Nationality')}}
					<select name="person[Pers_Nationality]" id="nation" class="input-xlarge">
						<?php $countries = Variable::domain('Country')->toArray();  ?>
						@foreach ($countries as $key => $country)
						<option>{{$country['Vari_VariableName']}}</option>
						@endforeach
					</select>
					<p class="help-block">Example: The Gambia</p>
				</div>
				<div class="last ">
					{{Form::label('person[Pers_Gender]','Gender')}}
					{{Form::select('person[Pers_Gender]', array('male' => 'Male','female' => 'Female'));}}
					<p class="help-block">Example: male or female</p>
				</div>
			</div>
			<div class="level contact">
			<span>Address</span>
			<hr>
				<div class="first">
					{{Form::label('address[Addr_Town]','Town')}}
					{{Form::text('address[Addr_Town]',null,['class'=>'input-xlarge','placeholder'=>'Enter town'])}}
					<p class="help-block">Example: Kololi</p>
				</div>
				<div class="midle">
					{{Form::label('address[Addr_District]','District')}}
					<select name="address[Addr_District]" id="region">
						<?php $countries = Variable::domain('Addr_District')->toArray();  ?>
						@foreach ($countries as $key => $country)
						<option>{{$country['Vari_VariableName']}}</option>
						@endforeach
					</select> 
					<p class="help-block">Example: Banjul South</p>
				</div>
				<div class="last">
					{{Form::label('address[Addr_AddressStreet]','street')}}
					{{Form::text('address[Addr_AddressStreet]',null,['class'=>'input-xlarge','placeholder'=>'Enter street'])}}
					<p class="help-block">Example: Dr. ceesay Street</p>
				</div>
			</div>

			<div class="level address">
			 <span>class</span>
			<hr>
				<div class="first ">
					{{Form::label('class[Clas_LocalClassName]','Class Name')}}
					{{Form::text('class[Clas_LocalClassName]',null,['class'=>'input-xlarge','placeholder'=>'Enter local class name'])}}
					<p class="help-block">Example: Introduction to ICT</p>

				</div>
				<div class="midle ">
					{{Form::label('class[Clas_AdmissionDate]','Admission Date')}}
					{{Form::date('class[Clas_AdmissionDate]',['class'=>'input-xlarge','placeholder'=>'Enter admission date'])}}
					<p class="help-block">Example: 30/1/2014</p>

				</div>
			</div>
			<div class="level actions">
				<!-- <div class="left"> -->
					<!-- <button type="button" class="btn btn-primary">Primary</button> -->
					<!-- <button type="button" class="btn btn-info">Info</button> -->
				<!-- </div> -->
				<div class="right">
					<!-- <button type="button" class="btn btn-default">Reset Default</button> -->
					<!-- <button type="button" class="btn btn-success span6">Create Student</button> -->
	<!-- 				<button type="button" class="btn btn-danger">Cancel Operation</button> -->

					  <button type="submit" class="btn btn-primary  span6" name="save" value="save">Save</button>
					  <button type="submit" class="btn btn-success btn-success span6" name="save" value="done">Done</button>
			</div>

			</div>
		{{Form::close()}}
	</div>
		</div><!-- content conent -->
	<div class="cft">
		
	</div><!-- content footer -->
@stop