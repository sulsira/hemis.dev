@include('_partials/doc')
	@section('style')
	{{ HTML::style('css/main.css') }}
	{{ HTML::style('css/start-main.css') }}		 
	{{ HTML::style('css/start-style.css') }}	 
	@show
       
    </head>
    <body>
    	<div class="top">
			<div class="top_bar_container">
				<div class="top_bar_inner top_bar_style">
					<div class="top_bar_up">
						<h2>WELCOME TO HIGHER EDUCATION MANAGEMENT INFORMATION SYSTEM (HEMIS)</h2>
					</div>
					<div class="navbar">
						<ul class="nav nav-pills">
							  <li><a href="{{route('schools')}}">learning centers</a></li>
		            <li>{{HTML::link('research','Research')}}</li>
		            <li>{{HTML::link('apply','Scholarship')}}</li>
					<li><a href="{{route('login')}}">login</a></li>
		            <li>{{HTML::link('help','Help')}}</li>

		            <li><a href="http://moherst.info/" target="_blank">MOHERST INFO</a></li>
							  <li>
							    <a href="http://moherst.gov.gm/" target="_blank">MoHERST Website</a>
							  </li>
						</ul>
					</div>
				</div>
			</div>
    	</div>
	    <div class="content">

	<div class="content">
	<h1>SORRY THERE WAS AN ERROR</h1>
	<hr>
	<strong><a href="login">Login</a></strong> |
	<strong><a href="help">help</a></strong> 
	</div>
	    </div>
	    <footer>
	    	<div class="hedacont">MOHERST &copy; Copyright 2013</div>
	    	@yield('footer')
	    </footer>
	    	@yield('script')
@include('_partials/footer')