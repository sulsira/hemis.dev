@section('scope')
	@include('_partials/scope')	  
@stop
@section('container')
<div class="chd">
	<h2>Learning Centers</h2>
	<hr>
</div>
<div class="cc">
	<table class="table table-bordered">
	<caption><strong>Learning Centers</strong></caption>
		<thead>
			<tr>
				<th>classification</th>
				<th>ownership</th>
				<th>number of learning centers</th>			
			</tr>
		</thead>
		<tbody>
		<?php  ?>
		<?php foreach ($data['stats'] as $key => $value): ?>
			<tr>
				<td rowspan="{{count($value) + 1}}">{{$key}}</td>
			</tr>	
				<?php foreach ($value as $key1 => $value1): ?>
					<tr>
						<td>{{$key1}}</td>	
						<td>{{$value1}}</td>		
					</tr>					
				<?php endforeach ?>
		<?php endforeach ?>
		</tbody>
	</table>
<hr>
<h2>List Of Learning Centers</h2>
<hr>
<?php foreach ($data['lc'] as $key => $value): ?>
	<h3 class="hed">{{$key}}</h3>
		<?php foreach ($value as $key1 => $value1): ?>
			<h4 class="subhd">{{$key1}}</h4>
			<ul class="hdul">
			<?php foreach ($value1 as $key2 => $value2): ?>				
					<li>{{$value2['LeCe_Name']}}</li>				
			<?php endforeach ?>
			</ul>
		<?php endforeach ?>
	<hr>
<?php endforeach ?>


</div>

<div class="cft">


</div>

@stop	