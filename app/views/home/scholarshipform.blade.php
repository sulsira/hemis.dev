@section('style')
	@parent
@stop
@section('top')
	<div class="top_bar_container">
		<div class="top_bar_inner top_bar_style">
			<div class="top_bar_up">
				<h2>WELCOME TO HIGHER EDUCATION MANAGEMENT INFORMATION SYSTEM (HEMIS)</h2>
			</div>
			<div class="navbar">
				<ul class="nav nav-pills">
					  <li>
					    <a href="http://moherst.gov.gm/">MoHERST</a>
					  </li>
					  <li><a href="http://moherst.info/">MOHERST INFO</a></li>
					  <li><a href="http://dev.moherst.info/">DEV</a></li>
					  <li><a href="http://apps.moherst.info/">APPS</a></li>
					  <li><a href="http://hemis.moherst.info/">HEMIS</a></li>
					  <li><a href="http://app.moherst.info/">APP</a></li>
				</ul>
			</div>
		</div>
	</div>
@stop
@section('container')
	<div class="content">
<div class="tabbable tabs-below">
<ul class="nav nav-tabs nav-justified">
   <li class="active"><a href="#tab1" data-toggle="tab">Personal Information</a></li>
   <li><a href="#tab2" data-toggle="tab">Educational Background</a></li>
   <li><a href="#tab3" data-toggle="tab">Work Experience</a></li>
   <li><a href="#tab4" data-toggle="tab">Documents</a></li>
   <li><a href="#tab5" data-toggle="tab">Scholarship Details</a></li>
</ul>
  <div class="tab-content">
      <div class="panel-info" id="tab1">
        <!-- Default panel contents -->
        <hr>
        <div class="panel-heading">Personal Information</div>
        <hr>
        <div class="panel-body">
             <form role="form" method="post">

              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="">
                <p class="help-block">Please the name that is on your documents</p>
              </div> 

              <div class="form-group">
                <label for="othername">Other name</label>
                <input type="text" class="form-control" id="othername" placeholder="Enter Other name" name="othername" value="">
              </div> 

               <div class="form-group">
                <label for="dob">Date of Birth</label>
                <input type="date" class="form-control" id="dob" placeholder="Enter Other name" name="dob" value="">
              </div> 

              <div class="form-group">
                <label for="dob">Gender</label>
                <div class="radio">
                  <label>
                    <input type="radio"  id="male" value="male" checked="" name="gender">
                    Male
                  </label>
                </div> 

                <div class="radio">
                  <label>
                    <input type="radio"  id="female" value="f" checked="" name="gender">
                    Female
                  </label>
                </div>
              </div> 

              <div class="form-group">
                <label for="nationality">Nationality</label>
                <input type="text" class="form-control" name="nationality" placeholder="Enter Nationality" value="">
              </div>

              <div class="form-group">
                <label for="tel">Telephone/Mobile</label>
                <input type="telephone" class="form-control" id="tel" placeholder="Enter Telephoe" name="telephone" value="">
              </div>

              <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="">
              </div>
              
              <div class="form-group">
                <label for="address">address</label>
                <textarea class="form-control" rows="3" name="address" value=""></textarea>
                <p class="help-block">For Correspondence</p>
              </div>
            <fieldset>
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="reset" class="btn btn-default">Cancel</button>
            </fieldset>
          </form>
        </div>

        <!-- Table -->
        <table class="table">
          ...
        </table>
      </div>

      <div class="panel-info" id="tab2">
        <!-- Default panel contents -->
        <hr>
        <div class="panel-heading">Information</div>
        <hr>

        <div class="panel-body">

        </div>

        <!-- Table -->
        <table class="table">
          ...
        </table>
      </div>
      <div class="panel-info" id="tab3">
        <!-- Default panel contents -->
        <hr>
        <div class="panel-heading">Information3</div>
        <hr>

        <div class="panel-body">

        </div>

        <!-- Table -->
        <table class="table">
          ...
        </table>
      </div>

      <div class="panel-info" id="tab4">
        <!-- Default panel contents -->
        <hr>
        <div class="panel-heading">Information4</div>
        <hr>

        <div class="panel-body">

        </div>

        <!-- Table -->
        <table class="table">
          ...
        </table>
      </div>

      <div class="panel-info" id="tab5">
        <!-- Default panel contents -->
        <hr>
        <div class="panel-heading">Information5</div>
        <hr>

        <div class="panel-body">

        </div>

        <!-- Table -->
        <table class="table">
          ...
        </table>
      </div>


  </div>
</div>

	</div>

@stop




