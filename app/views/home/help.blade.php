@section('style')
	@parent
@stop
@section('container')

<div class="content ">
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">start up</li>
              <li><a href="#signup">signing up</a></li>
              <li><a href="#login">loging in</a></li>
              <li><a href="#access">access</a></li>
              <li class="nav-header">data navigation</li>
              <li><a href="#">navigation bar</a></li>
              <li><a href="#">access rights</a></li>
              <li class="nav-header">exit</li>
              <li><a href="#logout">log out</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          <div class="hero-unit">
            <h1>MoHERST information System Guide</h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
            <hr>
            <h2>General templates</h2>
            <p>
            	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
			<p><a href="#" class="btn btn-primary btn-large">Download Template »</a></p>
            <a href="#" class="btn btn-primary btn-large">Upload File</a>
          </div>
          <div class="row-fluid" id="login">
            <div class="row">
              <h2>how to Login</h2>
              	<h3>Step 1 :</h3>
              <p>To login you would go to go to information system login page. which you can acess. Which you can access at <a href="login">LOGIN</a>. </p>
              	<h3>Step 2 :</h3>
              <p>You are required to have a register email address and a password. example: something@mail.com and your registered password.
              </p>
              <hr>
            </div><!--/span-->
          </div><!--/row-->

          <div class="row-fluid" id="signup">
            <div class="row">
              <h2>how to sign up</h2>
              	<h3>Step 1 :</h3>
              <p>Generally there are no signups. have access to the MoHERST information system you would have be given access token. which would let you log in by an email address with a password of your choosing. </p>
              	<h3>Step 2 :</h3>
              <p>Contact system admistrator to create a user account for you. the following individuals are responsible:
              <em>Name of The Person</em>
              </p>
              <hr>
            </div><!--/span-->
          </div><!--/row-->
          <div class="row-fluid" id="access">
          <h2>Access</h2>
          	<p>Each user is ristricted to a certain data domain. that is user depending on his/her privelleges and access level would only be allowed to access certain data assigned.</p>
          </div><!--/row-->
          <hr>
          <div class="row-fluid" id="logout">
            <div class="row">
              <h2>how to Log out</h2>
              	<h3>Step 1 :</h3>
              <p>To log out from account. first you would click your name or email address at the top blue bar. </p>
              	<h3>Step 2 :</h3>
              <p>after the drop down click on log out
              </p>
              <hr>
            </div><!--/span-->
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

    </div><!--/.fluid-container-->	
</div>


@stop




