<?php

use Illuminate\Database\Migrations\Migration;

class AddFieldToTaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tasks', function($table) {
			$table->date('emailed')->integer()->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tasks', function($table) {
			$table->dropColumn('emailed');
		});
	}

}