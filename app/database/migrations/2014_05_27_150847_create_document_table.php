<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable();
			$table->string('entity_type')->nullable();
			$table->string('entity_ID')->default(0);
			$table->string('type')->nullable();
			$table->string('fullpath')->nullable();
			$table->string('filename')->nullable();
			$table->string('foldername')->nullable();
			$table->string('extension')->nullable();
			$table->string('filetype')->nullable();
			$table->string('userID')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document');
	}

}
