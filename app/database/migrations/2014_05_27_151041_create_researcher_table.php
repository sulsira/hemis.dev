<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResearcherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('researcher', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('userID')->default(0);
			$table->string('email')->nullable();
			$table->string('type')->nullable();
			$table->string('fullname')->nullable();
			$table->integer('deleted')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('researcher');
	}

}
