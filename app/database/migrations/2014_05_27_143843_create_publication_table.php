<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePublicationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('publication', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable();
			$table->integer('pub_researcherID')->default(0);
			$table->string('pub_type')->nullable();
			$table->string('pub_abstract')->nullable();
			$table->string('pub_discipline')->nullable();
			$table->string('pub_view')->nullable();
			$table->string('pub_docID')->nullable();
			$table->string('visible')->nullable();
			$table->string('deleted')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('publication');
	}

}
