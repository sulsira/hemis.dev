<?php

use Illuminate\Database\Migrations\Migration;

class AddTaskUseridTaskusersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('task_users', function($table) {
			$table->integer('id')->autoincrement()->primaryKey();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tasks', function($table) {
			$table->dropColumn('id');
		});
	}

}