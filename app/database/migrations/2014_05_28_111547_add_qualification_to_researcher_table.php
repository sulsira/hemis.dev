<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddQualificationToResearcherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('researcher', function(Blueprint $table)
		{
					$table->text('qualification')->nullable();	
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('researcher', function(Blueprint $table)
		{
					$table->dropColumn('qualification');
		});
	}

}
