<?php

use Illuminate\Database\Migrations\Migration;

class CreateTaskUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_users', function($t)
		{
			$t->integer('toID')->nullable();
			$t->integer('fromID')->nullable();
			$t->integer('taskID')->nullable();
			$t->integer('statusID')->nullable();
			$t->integer('opened')->nullable();
			$t->integer('emailed')->nullable();
			$t->integer('deleted')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('task_users');
	}

}