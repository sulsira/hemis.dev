<?php namespace Services\Validators;

class CreateUser extends Validator
{
	
	public static $rules = [
				'email'=>'required|email|unique:users,email',
				'phone'=> 'required|numeric',
				'userType'=>'required',
				'password'=>'required'
	];

}